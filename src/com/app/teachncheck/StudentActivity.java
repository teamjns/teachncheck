package com.app.teachncheck;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.fragments.ClassListFragment;
import com.app.teachncheck.fragments.ManagerFragment;
import com.app.teachncheck.fragments.StudentAbsenceFragment;
import com.app.teachncheck.fragments.StudentDetailsFragment;
import com.app.teachncheck.fragments.StudentMarksFragment;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>StudentActivity is designed to allow the user to see all details of a student and modify it</b> 
 * <p>This activity contains three actionbar tabs : {@link StudentDetailsFragment}, {@link StudentMarksFragment} and {@link StudentAbsenceFragment}. 
 * StudentDetails gives access to all the student's details (name,parents ...), StudentMarks to his marks (or grades), and StudentAbsence to his absences.
 * </p>
 * 
 * @see MainActivity
 * @see SettingsActivity
 * @see StudentDetailsFragment
 * @see StudentMarksFragment
 * @see StudentAbsenceFragment
 */
public class StudentActivity extends SherlockFragmentActivity {
	
	/**
	 * The student whose data are displayed
	 */
	private Student student;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_student);
    
        Bundle bundle = this.getIntent().getExtras();
        student = (Student) bundle.getSerializable("student");
        
        ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);	
		actionBar.setTitle(student.toString());
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		ActionBar.Tab tabA = actionBar.newTab().setText(R.string.tab_student_details);
		ActionBar.Tab tabB = actionBar.newTab().setText(R.string.tab_student_marks);
		ActionBar.Tab tabC = actionBar.newTab().setText(R.string.tab_student_absences);
		
		tabA.setTabListener(new TabListener<StudentDetailsFragment>(this,"title1", StudentDetailsFragment.class));
		tabB.setTabListener(new TabListener<StudentMarksFragment>(this,"title2", StudentMarksFragment.class));
		tabC.setTabListener(new TabListener<StudentAbsenceFragment>(this,"title3", StudentAbsenceFragment.class));
		
		actionBar.addTab(tabA);
		actionBar.addTab(tabB);
		actionBar.addTab(tabC);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
    
    /**	
     * {@inheritDoc}
     */
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case android.R.id.home:
    			CommonMethods.returnHome(this);
    			return true;
			case R.id.menu_settings:
				CommonMethods.gotoSettings(this);
				return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }

    /**
	 * <b>Listener for the actionbar tabs.</b>
	 * <p>Here it is used to change the content of the action bar menu depending on the tab selected.</p>
	 * @param <T> An instance of {@link ManagerFragment} or {@link ClassListFragment}
	 */
    private class TabListener<T extends Fragment> implements
		    ActionBar.TabListener {
		private Fragment mFragment;
		private final SherlockFragmentActivity mActivity;
		private final String mTag;
		private final Class<T> mClass;
		
		/**
		 * Constructor used each time a new tab is created.
		 * 
		 * @param studentActivity
		 *            The host Activity, used to instantiate the fragment
		 * @param tag
		 *            The identifier tag for the fragment
		 * @param clz
		 *            The fragment's Class, used to instantiate the fragment
		 */
		public TabListener(StudentActivity studentActivity, String tag, Class<T> clz) {
		    mActivity = studentActivity;
		    mTag = tag;
		    mClass = clz;
		}
		
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
		    // Check if the fragment is already initialized
		    if (mFragment == null) {
		        
		    	Bundle bundle = new Bundle();
				bundle.putSerializable("student",student);
				
		        mFragment = Fragment.instantiate(mActivity, mClass.getName(),bundle);
		        ft.add(android.R.id.content, mFragment, mTag);
		        
		    } else {
		        // If it exists, simply attach it in order to show it
		        ft.show(mFragment);
		    }
		}
		
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		    if (mFragment != null) {
		        // Detach the fragment, because another one is being attached
		    	ft.hide(mFragment);
		    }
		}
		
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		    // User selected the already selected tab. Usually do nothing.
		}
    }
}
