package com.app.teachncheck.fragments;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.TextView;

import com.app.teachncheck.ClassActivity;
import com.app.teachncheck.ClassAddActivity;
import com.app.teachncheck.ClassExportActivity;
import com.app.teachncheck.ClassImportActivity;
import com.app.teachncheck.ClassModificationActivity;
import com.app.teachncheck.MainActivity;
import com.app.teachncheck.R;
import com.app.teachncheck.StudentsListActivity;
import com.app.teachncheck.bdd.Classroom;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnActionExpandListener;

/**
 * <b>ClassList fragment is one of the two main fragment of this application. Display a list of all classrooms.</b>
 * <p>This fragment is instantiated by the MainActivity class.</p>
 * <p>In this activity, the user can :</p>
 * <ul>
 * 		<li>Browse the classroom list</li>
 * 		<li>Select the classroom that will become the current one</li>
 * 		<li>Access the details of a classroom</li>
 * 		<li>Add a new classroom</li>
 * 		<li>Access to the students list</li>
 * 		<li>Import or export data</li>
 * 		<li>Search in the list of classrooms</li>
 * </ul>
 * <p>Uses the activity_class_list layout.</p>
 * 
 * @see ClassActivity
 * @see ClassAddActivity
 * @see ClassExportActivity
 * @see ClassImportActivity
 * @see ClassModificationActivity
 * @see MainActivity
 * @see StudentsListActivity
 */
public class ClassListFragment extends SherlockFragment {
	
	/**
	 * List of all classrooms
	 */
	private ArrayList<Classroom> listClassroom;
	
	/**
	 * ListView displaying the list of classrooms
	 */
	private ListView listView_ClassList;
	
	/**
	 * Adapter of the classrooms ListView
	 */
	private ArrayAdapter<Classroom> adapter = null;
	
	/**
	 * Action mode currently instantiated
	 */
	private ActionMode mMode;

	/**
	 * Indicates how many classrooms are currently selected in the listview
	 */
	private int qtClassSelected = 0;
	
	/**
	 * Classroom currently selected (if there is only one classroom selected in the list)
	 */
	private Classroom classroom;
	
	/**
	 * Indicates which action mode is currently activated. Can have the value EDIT_OR_DELETE_MODE or DETAILS_OR_CHECK_MODE
	 */
	private int amode;
	
	/**
	 * Indicates that the action mode currently activated is {@link DetailsOrCheckActionMode}
	 */
	private final int EDIT_OR_DELETE_MODE = 0;
	
	/**
	 * Indicates that the action mode currently activated is {@link EditOrDeleteActionMode}
	 */
	private final int DETAILS_OR_CHECK_MODE = 1;
	
	/**
	 * Head of the checkbox column (display 'open')
	 */
	private TextView text_open;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
    	super.onCreateView(inflater, container, savedInstanceState);
    	
    	//Required to manipulate the menu inside a fragment
    	setHasOptionsMenu(true);
    	
    	//Set the actionbar title
		getSherlockActivity().getSupportActionBar().setTitle(getString(R.string.label_class_list).toUpperCase());
		
    	LinearLayout view = (LinearLayout) inflater.inflate(R.layout.activity_class_list, container, false);
   
    	listView_ClassList = (ListView) view.findViewById(R.id.list_class);
		listView_ClassList.setOnItemClickListener(new OnItemClickListener() {		
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(mMode==null){
					amode = DETAILS_OR_CHECK_MODE;
					classroom = listClassroom.get(position);
					mMode = getSherlockActivity().startActionMode(new DetailsOrCheckActionMode());	
				}else {

					if(amode == EDIT_OR_DELETE_MODE){
						
						//Update the number of selected classroom
						if(listView_ClassList.isItemChecked(position))
							qtClassSelected++;
						else
							qtClassSelected--;
						
						//If no classroom selected, the action mode is finished
						if(qtClassSelected == 0){
							mMode.finish();
						}
						//If only one classroom is selected, then it is possible to modify or delete the classrooms
						else if (qtClassSelected == 1){
							Menu menu = mMode.getMenu();
							menu.clear();
							MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
							inflater.inflate(R.menu.menu_delete_plus_modif, menu);
						}
						//If more than one classroom is selected, then only the removal is possible
						else if (qtClassSelected == 2){
							Menu menu = mMode.getMenu();
							menu.clear();
							MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
							inflater.inflate(R.menu.menu_delete, menu);
						}
					}else{
						classroom = listClassroom.get(position);
					}
				}
			}
		});
		
		listView_ClassList.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {

				//Activates the EditOrDelete action mode if it is not yet activated
				if(mMode == null || !(amode==EDIT_OR_DELETE_MODE)){
					listView_ClassList.setItemChecked(position, true);
					amode = EDIT_OR_DELETE_MODE;
					mMode = getSherlockActivity().startActionMode(new EditOrDeleteActionMode());
					listView_ClassList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
				}
				return true;
			}
		});
		
		listView_ClassList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    	
		text_open = (TextView) view.findViewById(R.id.text_open);
		
    	return view;
    }
    
	@Override
	public void onStop(){
		super.onStop();
		if(mMode!=null)
			mMode.finish();
	}

	@Override
	public void onResume(){
		super.onResume();
		initList();
	}

    @Override
    public void onCreateOptionsMenu (Menu menu, MenuInflater inflater) {
    	
    	super.onCreateOptionsMenu(menu, inflater);
    	
    	menu.clear();
		inflater.inflate(R.menu.menu_activity_class_list, menu);	
		
		View v = (View) menu.findItem(R.id.menu_search).getActionView();
        final EditText edit_search = ( EditText ) v.findViewById(R.id.txt_search);
        
        //For the search bar
        edit_search.addTextChangedListener(searchOnChangeListener);	
        menu.findItem(R.id.menu_search).setOnActionExpandListener(new OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
            	edit_search.post(new Runnable() {
                    @Override
                    public void run() {
                    	edit_search.requestFocus();
                		InputMethodManager imm = (InputMethodManager)getSherlockActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                		imm.showSoftInput(edit_search, InputMethodManager.SHOW_FORCED);
                    }
                });
                return true;
            }
            
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
            	edit_search.post(new Runnable() {
                    @Override
                    public void run() {
                    	edit_search.clearFocus();
                    }
                });
                edit_search.setText("");
                return true;
            }
        });
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		//To create a new classroom
		case R.id.menu_add_class:
			Intent myIntent = new Intent(getSherlockActivity(), ClassAddActivity.class);
	    	myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
	        startActivity(myIntent);
			return true;
		//To browse the list of students
		case R.id.menu_list_students:
			Intent myIntent1 = new Intent(getSherlockActivity(), StudentsListActivity.class);
	    	myIntent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
	        startActivity(myIntent1);
	        return true;
	    //To import data
		case R.id.menu_import:
			Intent myIntent2 = new Intent(getSherlockActivity(), ClassImportActivity.class);
	    	myIntent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
	        startActivity(myIntent2);
	        return true;
	    //To export data
		case R.id.menu_export:
			Intent myIntent3 = new Intent(getSherlockActivity(), ClassExportActivity.class);
	    	myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
	        startActivity(myIntent3);
	        return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	/**
	 * Listener of the search bar
	 */
	private TextWatcher searchOnChangeListener = new TextWatcher() {

		public void afterTextChanged(Editable s) {
		}

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(adapter!=null)
				adapter.getFilter().filter(s);
		}

	};

	/**
	 * <b>Action mode that allows the user to edit or delete a classroom</b>
	 *  Activated when the user perform a long click on the list of classrooms.
	 */
	private final class EditOrDeleteActionMode implements ActionMode.Callback {
		
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
			inflater.inflate(R.menu.menu_delete_plus_modif, menu);
			qtClassSelected = 1;
			text_open.setVisibility(TextView.GONE);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

			switch (item.getItemId()) {
				case R.id.delete:
					
					//Retrieve the list of classroom to delete
					SparseBooleanArray listModified = listView_ClassList.getCheckedItemPositions();
					final ArrayList<Classroom> lClass_to_delete = new ArrayList<Classroom>();
					for(int pos=0;pos<listModified.size();pos++){
						if(listModified.get(listModified.keyAt(pos))==true){
							Classroom classroom1 = listClassroom.get(listModified.keyAt(pos));
							lClass_to_delete.add(classroom1);
								
						}
					}
					
					//Ask a confirmation to the user, then delete the classrooms
					AlertDialog confirmDialog = new AlertDialog.Builder(getSherlockActivity()).create();
					confirmDialog.setTitle(getString(R.string.confirmation));
					confirmDialog.setMessage(getString(R.string.delete_confirmation_class));
					confirmDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
					      public void onClick(DialogInterface dialog, int which) {
					    	  for(Classroom c:lClass_to_delete){
					    		  c.deleteClassroom();	
					    	  }
					    	  initList();
					}});
					confirmDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}});
					confirmDialog.show();
						
					mMode.finish();
					return true;
					
				case R.id.modify:
					
					//Retrieve the selected classroom
					Classroom class1 = null;
					SparseBooleanArray listModified2 = listView_ClassList.getCheckedItemPositions();
					for(int pos=0;pos<listModified2.size();pos++){
						if(listModified2.get(listModified2.keyAt(pos))==true){
							class1 = listClassroom.get(listModified2.keyAt(pos));
							break;	
						}
					}
					
					Bundle bundle = new Bundle();
					bundle.putSerializable("class",class1);
					
					Intent myIntent = new Intent(getSherlockActivity(), ClassModificationActivity.class);
			    	myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			    	myIntent.putExtras(bundle);
			    	
			    	mMode.finish();
			        startActivity(myIntent);
			        
					return true;
					
				default:
					return true;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			for(int pos=0;pos<listView_ClassList.getAdapter().getCount();pos++){
				listView_ClassList.setItemChecked(pos, false);
			}
			listView_ClassList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			text_open.setVisibility(TextView.VISIBLE);
			mMode = null;
		}
	}

	/**
	 * <b>Action mode that allows the user to see the details of a classroom or to set it as the current classroom</b>
	 *  Activated when the user perform a short click on the list of classrooms.
	 */
	private final class DetailsOrCheckActionMode implements ActionMode.Callback {
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
			inflater.inflate(R.menu.menu_classe, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			
			Bundle bundle;
			Intent newIntent;
			
			switch (item.getItemId()) {
			
				case R.id.details_class:
					
					mMode.finish();
					bundle = new Bundle();
					bundle.putSerializable("class",classroom);
					newIntent = new Intent(getSherlockActivity(), ClassActivity.class);
					newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
					newIntent.putExtras(bundle);
					startActivity(newIntent);
					return true;
					
				case R.id.open_class:
					
					writeFileToInternalStorage(classroom);
					newIntent = new Intent(getSherlockActivity(), MainActivity.class);
					newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(newIntent);
					mMode.finish();
					return true;
					
				default:
					return true;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			if(amode!=EDIT_OR_DELETE_MODE)
				listView_ClassList.setItemChecked(listView_ClassList.getCheckedItemPosition(), false);
			mMode = null;
		}
	}
	
	/**
	 * Initialize the list of classroom using the database
	 */
	public void initList(){
		listClassroom = MainActivity.getDb().getAllClassroom();
		adapter = new ArrayAdapter<Classroom>(getSherlockActivity(),android.R.layout.simple_list_item_multiple_choice, listClassroom);
		if(listClassroom.isEmpty())
			((TextView) getView().findViewById(R.id.empty_list)).setVisibility(TextView.VISIBLE);
		else
			listView_ClassList.setAdapter(adapter);
	}
	
	/**
	 * Save the id of the current classroom into the application's internal file
	 * @param classroom The current classroom
	 */
	public void writeFileToInternalStorage(Classroom classroom) {
		String eol = System.getProperty("line.separator");
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(getSherlockActivity().openFileOutput("myfile", Context.MODE_PRIVATE)));
			writer.write(classroom.getId() + eol);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	} 
}
