package com.app.teachncheck.fragments;


import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.app.teachncheck.R;
import com.app.teachncheck.StudentActivity;
import com.app.teachncheck.bdd.Absence;
import com.app.teachncheck.bdd.Student;

/**
 * <b>StudentAbsenceFragment allows to check and justify the student's absences.</b>
 * <p>This fragment is instantiated by {@link StudentActivity}.</p>
 * <p>In this activity, the user can :</p>
 * <ul>
 * 		<li>See the student absences</li>
 * 		<li>Justify an absence by giving a valid reason</li>
 * </ul>
 * <p>Uses the fragment_student_absences layout.</p>
 * 
 * @see StudentActivity
 */
public class StudentAbsenceFragment extends SherlockFragment {
	
	/**
	 * The student whose absences are displayed
	 */
	private Student student;
	/**
	 * Table of absences
	 */
	private TableLayout table;
	/**
	 * Absence currently selected
	 */
	private Absence absence;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
    	//Required to manipulate the menu inside a fragment
    	setHasOptionsMenu(true);
    	
    	LinearLayout view = (LinearLayout) inflater.inflate(R.layout.fragment_student_absences, container, false);
    	
    	//Retrieves the student
    	Bundle bundle = this.getArguments();
    	student = (Student) bundle.getSerializable("student");

		table = (TableLayout) view.findViewById(R.id.tab_absence);
		
    	return view;
    }
    
	@Override
	public void onCreateOptionsMenu (Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_simple, menu);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		initTab();
	};
	
	/**
	 * Fills the absences table
	 */
	private void initTab(){
		
		//Purges the table
		table.removeAllViews();
		
		ArrayList<Absence> listAbsences = student.getAllAbsenceOfStudent();
    	ArrayList<CheckBox> list_checks = new ArrayList<CheckBox>();
		
    	if(listAbsences.isEmpty()){
    		
    		((TextView) getView().findViewById(R.id.empty_list)).setVisibility(TextView.VISIBLE);
    		
    	}else{
	    	
    		//Creates a new line 
	    	TableRow r = new TableRow(getSherlockActivity());
	    	
	    	//Add the columns heads
			TextView t = new TextView(getSherlockActivity());
			t.setText(R.string.date);
			r.addView(t);
			TextView t1 = new TextView(getSherlockActivity());
			t1.setText(R.string.reason);
			r.addView(t1);
			t1.setGravity(Gravity.CENTER_HORIZONTAL);
			TextView t2 = new TextView(getSherlockActivity());
			t2.setText(R.string.justified);
			r.addView(t2);
			table.addView(r);
			
			//Add an horizontal line
			table.addView(createLine());
	    	
			boolean reason_exist = false;
			
			//Fills the table with the date of the absence and the reason of each absence(if justified)
			for(Absence absence:listAbsences){
				TableRow row = (TableRow) getSherlockActivity().getLayoutInflater().inflate(R.layout.item_student_absence, null);
	
				((TextView)row.findViewById(R.id.txt_date)).setText(absence.toString());
				((TextView)row.findViewById(R.id.txt_reason)).setText(absence.getReason());
				final CheckBox check_justified = (CheckBox)row.findViewById(R.id.check_justified);
				check_justified.setTag(absence);
				list_checks.add(check_justified);
	        	if(absence.getJustified()){
	        		check_justified.setChecked(true);
	        		reason_exist = true;
	        	}
	        	check_justified.setOnClickListener(checkOnClickListener);
	        	
	            table.addView(row,new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			}
		
			//If there is not even one justification, then an empty space is added
			if(!reason_exist)
				t1.setPadding(0, 0, 40, 0);
    	}
	}
	
    /**
     * Called when an absence's checkbox is clicked. Displays a dialog box to choose the justification if the absence is not yet justified.
     */
    private OnClickListener checkOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			CheckBox checkbox = (CheckBox) v;
			absence = (Absence) checkbox.getTag();
			if(checkbox.isChecked()){	
				DialogFragment justifiedDialog = new JustifiedDialogFragment();
				justifiedDialog.show(getSherlockActivity().getSupportFragmentManager(), "newSlot");
			}else{
				absence.setJustified(false);
				absence.setReason("");
				absence.saveAbsence();
				initTab();
			}
		}
	};
	
	/**
	 * Dialog box used to choose a justification to an absence.
	 */
	private class JustifiedDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflater = getActivity().getLayoutInflater();

			LinearLayout linear = (LinearLayout)inflater.inflate(R.layout.dialog_absence_choice, null);
			final RadioGroup groupReasons = (RadioGroup) linear.findViewById(R.id.radio_reason);
			
			builder.setTitle(R.string.motif)
			.setView(linear)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {

					RadioButton radio_reason = (RadioButton) groupReasons.findViewById(groupReasons.getCheckedRadioButtonId());
					absence.setReason(radio_reason.getText().toString());
					absence.setJustified(true);
					absence.saveAbsence();
					initTab();
					JustifiedDialogFragment.this.getDialog().dismiss();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					RadioButton radio_reason = (RadioButton) groupReasons.findViewById(groupReasons.getCheckedRadioButtonId());
					radio_reason.setChecked(false);
					initTab();
					JustifiedDialogFragment.this.getDialog().cancel();
				}
			});   

			Dialog d = builder.create();

			return d;
		}
	}
	
	/**
	 * Create a horizontal line
	 * @return The new horizontal line
	 */
	@SuppressWarnings("deprecation")
	private View createLine(){
		View v = new View(getSherlockActivity());
		v.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 3));
		v.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_solid_green));
		return v;
	}
}
