package com.app.teachncheck.fragments;


import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.ClassActivity;
import com.app.teachncheck.CreateMarkActivity;
import com.app.teachncheck.MarkActivity;
import com.app.teachncheck.R;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.bdd.Mark;

/**
 * <b>This fragment is designed to display a list of all marks in a classroom.</b>
 * <p>This fragment is instantiated by {@link ClassActivity}.</p>
 * <p>In this fragment, the user can :</p>
 * <ul>
 * 		<li>See the mark's list</li>
 * 		<li>Create a new mark</li>
 * 		<li>Delete a mark</li>
 * 		<li>Access details of a mark for each student</li>
 * </ul>
 * <p>Uses the fragment_class_marks layout.</p>
 * 
 * @see CreateMarkActivity
 */
public class ClassMarksFragment extends SherlockFragment {
    
	/**
	 * List of all marks
	 */
	private ArrayList<Mark> listMarks;

	/**
	 * Classroom whose marks are listed
	 */
	private Classroom classroom;
	/**
	 * ListView listing marks
	 */
	private ListView listViewMarks;
	/**
	 * Indicates how many marks are currently selected
	 */
	private int nbMarksSelected;
	/**
	 * ActionMode currently activated
	 */
	private ActionMode mMode;
	/**
	 * Head of CheckBoxes column
	 */
	private TextView textDetails;
	/**
	 * Icon delete marks (trash)
	 */
	private ImageView iconDelete;	

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
		setHasOptionsMenu(true);
		
		LinearLayout view = (LinearLayout) inflater.inflate(R.layout.fragment_class_marks, container, false);
		
		//Retrieve the classroom
		Bundle bundle = this.getArguments();
		classroom = (Classroom) bundle.getSerializable("class");
		
		listViewMarks = (ListView) view.findViewById(R.id.list_marks);
		listViewMarks.setOnItemClickListener(onMarkClicked);
		listViewMarks.setOnItemLongClickListener(onMarkLongClicked);
		listViewMarks.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		textDetails = (TextView) view.findViewById(R.id.text_open);
		iconDelete = (ImageView) view.findViewById(R.id.icon_delete);
		
		listMarks = classroom.getAllMarksOfClassroom();
		if(listMarks.isEmpty())
			((TextView) view.findViewById(R.id.empty_list)).setVisibility(TextView.VISIBLE);
		else
			listViewMarks.setAdapter(new ArrayAdapter<Mark>(getSherlockActivity(),android.R.layout.simple_list_item_multiple_choice, listMarks));
	
    	return view;
    }
	
    /**
	 * <b>Action mode used to delete one or more marks.</b>
	 *  Activated when a user perform a long click on a mark in the ListView.
	 */
	private final class DeleteActionMode implements ActionMode.Callback {
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
			inflater.inflate(R.menu.menu_delete, menu);
			nbMarksSelected = 0;
			textDetails.setVisibility(TextView.GONE);
			iconDelete.setVisibility(ImageView.VISIBLE);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

			switch (item.getItemId()) {
				case R.id.delete:	
					SparseBooleanArray listModified = listViewMarks.getCheckedItemPositions();
					for(int pos=0;pos<listModified.size();pos++){
						if(listModified.get(listModified.keyAt(pos))==true){
							Mark mark = listMarks.get(listModified.keyAt(pos));
							mark.deleteMark();
						}
					}
					initList();
					mMode.finish();
					return true;
				default:
					return true;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			for(int pos=0;pos<listViewMarks.getAdapter().getCount();pos++){
				listViewMarks.setItemChecked(pos, false);
			}
			listViewMarks.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			textDetails.setVisibility(TextView.VISIBLE);
			iconDelete.setVisibility(ImageView.GONE);
			mMode = null;
		}
	}

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    	menu.clear();
        inflater.inflate(R.menu.menu_add_mark, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    

    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case R.id.menu_add_mark:
    			DialogFragment newMark = new NewMarkDialogFragment();
    			newMark.show(getSherlockActivity().getSupportFragmentManager(), "newSlot");
    			return true;
    		default:    			
    			return super.onOptionsItemSelected(item);
    	}
    }
    
    /**
     * Open a mark's details when a mark is clicked in the ListView (if no action mode is activated)
     */
    private OnItemClickListener onMarkClicked = new OnItemClickListener() {

    	@Override
		public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
    		
			if(listViewMarks.isItemChecked(position))
				nbMarksSelected++;
			else
				nbMarksSelected--;

			if(mMode==null){		
				Bundle bundle = new Bundle();
				bundle.putSerializable("mark",listMarks.get(position));
				bundle.putSerializable("class",classroom);
				Intent newIntent = new Intent(getSherlockActivity(), MarkActivity.class);
				newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				newIntent.putExtras(bundle);
				getSherlockActivity().startActivity(newIntent);	
			}else if(nbMarksSelected == 0){
				mMode.finish();
			}
		}
	};
	
	/**
	 * Start the delete ActionMode when a long click is performed on a mark  
	 */
	private OnItemLongClickListener onMarkLongClicked = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view,
				int position, long id) {
			
			if(mMode==null){
				mMode = getSherlockActivity().startActionMode(new DeleteActionMode());
				listViewMarks.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
				
				Menu menu = mMode.getMenu();
				menu.clear();
				MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
				inflater.inflate(R.menu.menu_delete, menu);
			}
			return false;
		}
	};
	
	/**
	 * Update the ListView content or displays the "No Marks" message
	 */
	private void initList(){
		listMarks = classroom.getAllMarksOfClassroom();
		if(listMarks.isEmpty()){
			((TextView) getView().findViewById(R.id.empty_list)).setVisibility(TextView.VISIBLE);
			listViewMarks.setAdapter(new ArrayAdapter<Mark>(getSherlockActivity(), 0));
		}
		else
			listViewMarks.setAdapter(new ArrayAdapter<Mark>(getSherlockActivity(),android.R.layout.simple_list_item_multiple_choice, listMarks));
	}
	
    @Override
    public void onResume(){
    	super.onResume();
    	initList();
    }
    
	@Override
	public void onStop(){
		super.onStop();
		if(mMode!=null)
			mMode.finish();
	}
    
    /**
     * Dialog fragment used to create a new Mark
     */
    private class NewMarkDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflater = getActivity().getLayoutInflater();

			LinearLayout linear = (LinearLayout)inflater.inflate(R.layout.dialog_mark, null);
			final EditText edit_name = (EditText) linear.findViewById(R.id.edit_mark_name);
			final EditText edit_max = (EditText) linear.findViewById(R.id.edit_mark_max);

			builder.setTitle(R.string.new_mark)
			.setView(linear)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {

					Mark mark = new Mark(edit_name.getText().toString(), edit_max.getText().toString(), classroom);
					
					Bundle bundle = new Bundle();
					bundle.putSerializable("class", classroom);
					bundle.putSerializable("mark", mark);
					Intent intent = new Intent(getSherlockActivity(),CreateMarkActivity.class);
					intent.putExtras(bundle);
					startActivity(intent);
					NewMarkDialogFragment.this.getDialog().dismiss();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					NewMarkDialogFragment.this.getDialog().cancel();
				}
			});   

			Dialog d = builder.create();

			return d;
		}
	}
}
