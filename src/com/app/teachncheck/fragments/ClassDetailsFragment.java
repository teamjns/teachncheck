package com.app.teachncheck.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.app.teachncheck.ClassActivity;
import com.app.teachncheck.R;
import com.app.teachncheck.bdd.Classroom;

/**
 * <b>This fragment is designed to displayed the details data about a classroom.</b>
 * <p>This fragment is instantiated by {@link ClassActivity}.</p>
 * <p>In this fragment, the user can :</p>
 * <ul>
 * 		<li>See the detailed data of a classroom</li>
 * 		<li>Modify the details of a classroom</li>
 * </ul>
 * <p>Uses the fragment_class_details layout.</p>
 * 
 */
public class ClassDetailsFragment extends SherlockFragment {
	
	/**
	 * The classroom whose details are displayed
	 */
	private Classroom classroom;
	
	/**
	 * TextView displaying classroom's name
	 */
	private TextView txt_name;
	/**
	 * TextView displaying classroom's level
	 */
	private TextView txt_level;
	/**
	 * TextView displaying classroom's year
	 */
	private TextView txt_year;
	/**
	 * TextView displaying classroom's short name
	 */
	private TextView txt_short_name;

	/**
	 * EditText used to modify the classroom's name
	 */
	private EditText edit_name;
	/**
	 * EditText used to modify the classroom's level
	 */
	private EditText edit_level;
	/**
	 * EditText used to modify the classroom's year
	 */
	private EditText edit_year;
	/**
	 * EditText used to modify the classroom's short name
	 */
	private EditText edit_short_name;
	
	/**
	 * Switcher used to switch between TextView and EditView for classroom's name
	 */
	private ViewSwitcher switcher_name;
	/**
	 * Switcher used to switch between TextView and EditView for classroom's year
	 */
	private ViewSwitcher switcher_year;
	/**
	 * Switcher used to switch between TextView and EditView for classroom's short name
	 */
	private ViewSwitcher switcher_short_name;
	/**
	 * Switcher used to switch between TextView and EditView for classroom's level
	 */
	private ViewSwitcher switcher_level;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
    	ScrollView view = (ScrollView) inflater.inflate(R.layout.fragment_class_details, container, false);
    	
    	Bundle bundle = this.getArguments();
    	classroom = (Classroom) bundle.getSerializable("class");
    	
    	//Objects related to the name
    	txt_name = (TextView) view.findViewById(R.id.txt_name_class);
    	edit_name = (EditText) view.findViewById(R.id.edit_name_class);
    	switcher_name = (ViewSwitcher) view.findViewById(R.id.switch_name_class);
    	ImageButton bt_edit_name = (ImageButton) view.findViewById(R.id.bt_edit_name_class);
		ImageButton bt_save_name = (ImageButton) view.findViewById(R.id.bt_save_name_class);
		txt_name.setOnClickListener(onEdit_name_Clicked);
		bt_edit_name.setOnClickListener(onEdit_name_Clicked);
		bt_save_name.setOnClickListener(onSave_name_Clicked);

    	//Objects related to the short name
		txt_short_name = (TextView) view.findViewById(R.id.txt_short_name_class);
    	edit_short_name = (EditText) view.findViewById(R.id.edit_short_name_class);
    	switcher_short_name = (ViewSwitcher) view.findViewById(R.id.switch_short_name_class);
    	ImageButton bt_edit_short_name = (ImageButton) view.findViewById(R.id.bt_edit_short_name_class);
		ImageButton bt_save_short_name = (ImageButton) view.findViewById(R.id.bt_save_short_name_class);
		txt_short_name.setOnClickListener(onEdit_short_name_Clicked);
		bt_edit_short_name.setOnClickListener(onEdit_short_name_Clicked);
		bt_save_short_name.setOnClickListener(onSave_short_name_Clicked);
		

    	//Objects related to the year
		txt_year = (TextView) view.findViewById(R.id.txt_year_class);
    	edit_year = (EditText) view.findViewById(R.id.edit_year_class);
    	switcher_year = (ViewSwitcher) view.findViewById(R.id.switch_year_class);
    	ImageButton bt_edit_year = (ImageButton) view.findViewById(R.id.bt_edit_year_class);
		ImageButton bt_save_year = (ImageButton) view.findViewById(R.id.bt_save_year_class);
		txt_year.setOnClickListener(onEdit_year_Clicked);
		bt_edit_year.setOnClickListener(onEdit_year_Clicked);
		bt_save_year.setOnClickListener(onSave_year_Clicked);

    	//Objects related to the level
		txt_level = (TextView) view.findViewById(R.id.txt_level_class);
    	edit_level = (EditText) view.findViewById(R.id.edit_level_class);
    	switcher_level = (ViewSwitcher) view.findViewById(R.id.switch_level_class);
    	ImageButton bt_edit_level = (ImageButton) view.findViewById(R.id.bt_edit_level_class);
		ImageButton bt_save_level = (ImageButton) view.findViewById(R.id.bt_save_level_class);
		txt_level.setOnClickListener(onEdit_level_Clicked);
		bt_edit_level.setOnClickListener(onEdit_level_Clicked);
		bt_save_level.setOnClickListener(onSave_level_Clicked);
		
		//Filling fields
    	initView();

    	return view;
    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    	menu.clear();
        inflater.inflate(R.menu.menu_simple, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    
    /**
     * Take a String in parameter and format it with the first letter as an upper letter
     * @param String to format
     * @return
     * 		String formatted
     */
	private String formatFirstUp(String attribute){
    	if(attribute.equals(""))
    		return attribute;
    	return attribute.replaceFirst(".",(attribute.charAt(0)+"").toUpperCase());
    }
    
    /**
     * Fills the fields
     */
    private void initView(){
		txt_name.setText(isAvailable(formatFirstUp(classroom.getName())));
		edit_name.setText(classroom.getName());
		
		txt_short_name.setText(isAvailable(classroom.getEntitled()));
		edit_short_name.setText(classroom.getEntitled());
		
		txt_year.setText(isAvailable(classroom.getYear()));
		edit_year.setText(classroom.getYear());
		
		txt_level.setText(isAvailable(formatFirstUp(classroom.getLevel())));
		edit_level.setText(classroom.getLevel());
    }
    
    /**
     * Show the EditView for the classroom's name when the name or the corresponding edit button is clicked
     */
    private OnClickListener onEdit_name_Clicked = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			switcher_name.showNext();
			edit_name.requestFocus();
			showKeyboard(edit_name);
		}
	};
	
    /**
     * Save the classroom's name and hide the EditText when the corresponding save button is clicked
     */
    private OnClickListener onSave_name_Clicked = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			classroom.setName(edit_name.getText().toString());
			if(classroom.saveClass())
				txt_name.setText(classroom.getName());
			switcher_name.showNext();
			hideKeyboard(edit_name);
		}
	};
	
    /**
     * Show the EditView for the classroom's short name when the short name or the corresponding edit button is clicked
     */
    private OnClickListener onEdit_short_name_Clicked = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			switcher_short_name.showNext();
			edit_short_name.requestFocus();
			showKeyboard(edit_short_name);
		}
	};
	
    /**
     * Save the classroom's short name and hide the EditText when the corresponding save button is clicked
     */
    private OnClickListener onSave_short_name_Clicked = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			classroom.setEntitled(edit_short_name.getText().toString());
			if(classroom.saveClass())
				txt_short_name.setText(classroom.getEntitled());
			switcher_short_name.showNext();
			hideKeyboard(edit_short_name);
		}
	};
	
    /**
     * Show the EditView for the classroom's year when the year or the corresponding edit button is clicked
     */
    private OnClickListener onEdit_year_Clicked = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			switcher_year.showNext();
			edit_year.requestFocus();
			showKeyboard(edit_year);
		}
	};
	
    /**
     * Save the classroom's year and hide the EditText when the corresponding save button is clicked
     */
    private OnClickListener onSave_year_Clicked = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			classroom.setYear(edit_year.getText().toString());
			if(classroom.saveClass())
				txt_year.setText(classroom.getYear());
			switcher_year.showNext();
			hideKeyboard(edit_year);
		}
	};
	
    /**
     * Show the EditView for the classroom's level when the level or the corresponding edit button is clicked
     */
    private OnClickListener onEdit_level_Clicked = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			switcher_level.showNext();
			edit_level.requestFocus();
			showKeyboard(edit_level);
		}
	};
	
    /**
     * Save the classroom's level and hide the EditText when the corresponding save button is clicked
     */
    private OnClickListener onSave_level_Clicked = new OnClickListener() {		
		@Override
		public void onClick(View v) {
			classroom.setLevel(edit_level.getText().toString());
			if(classroom.saveClass())
				txt_level.setText(classroom.getLevel());
			switcher_level.showNext();
			hideKeyboard(edit_level);
		}
	};
	
	/**
	 * Force the keyboard to show for an EditText
	 * @param edit
	 * 		EditText calling the keyboard
	 */
	private void showKeyboard(EditText edit){
		InputMethodManager imm = (InputMethodManager)getSherlockActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(edit, InputMethodManager.SHOW_FORCED);
	}
	
	/**
	 * Force the keyboard to hide after a forced appearance
	 * @param edit
	 * 		EditText that force the keyboard to show
	 */
	private void hideKeyboard(EditText edit){
		InputMethodManager imm = (InputMethodManager)getSherlockActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}
	
	/**
	 * Show a "Not Available" String if the attribute is not stored in the database
	 * @param value : Attribute to check
	 * @return attribute value or the "Not Available" String
	 */
	private String isAvailable(String value){
		if(value.equals(""))
			return getString(R.string.not_available);
		return value;
	}
}
