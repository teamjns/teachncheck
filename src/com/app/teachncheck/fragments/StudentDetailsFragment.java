package com.app.teachncheck.fragments;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.ParentAddActivity;
import com.app.teachncheck.R;
import com.app.teachncheck.StudentActivity;
import com.app.teachncheck.bdd.Parent;
import com.app.teachncheck.bdd.Student;

/**
 * <b>StudentDetailsFragment allows the user to check and modify a student's personal informations.</b>
 * <p>This fragment is instantiated by {@link StudentActivity}.</p>
 * <p>In this activity, the user can :</p>
 * <ul>
 * 		<li>See the student personal data</li>
 * 		<li>Modify any of the data displayed</li>
 * 		<li>Add a parent</li>
 * </ul>
 * <p>Uses the fragment_student_details layout.</p>
 * 
 * @see ParentAddActivity
 * @see StudentActivity
 */
public class StudentDetailsFragment extends SherlockFragment {

	/**
	 * The student whose data are displayed
	 */
	private Student student;
	/**
	 * The textview displaying the student's surname
	 */
	private TextView txt_surname;
	/**
	 * The textview displaying the student's first name
	 */
	private TextView txt_first_name;
	/**
	 * The textview displaying the student's identity number
	 */
	private TextView txt_identity;
	/**
	 * The textview displaying the student's birth date
	 */
	private TextView txt_birthday;
	/**
	 * The textview displaying the student's address
	 */
	private TextView txt_address;
	/**
	 * The textview displaying the student's phone number
	 */
	private TextView txt_phone;
	/**
	 * The textview displaying the student's email address
	 */
	private TextView txt_mail;
	/**
	 * The textview displaying the student's picture path (temp)
	 */
	private TextView txt_picture_path;
	/**
	 * The edittext allowing to modify the student's surname
	 */
	private EditText edit_surname;
	/**
	 * The edittext allowing to modify the student's first name
	 */
	private EditText edit_first_name;
	/**
	 * The edittext allowing to modify the student's identity number
	 */
	private EditText edit_identity;
	/**
	 * The edittext allowing to modify the student's birth date
	 */
	private EditText edit_birthday;
	/**
	 * The edittext allowing to modify the student's address
	 */
	private EditText edit_address;
	/**
	 * The edittext allowing to modify the student's phone number
	 */
	private EditText edit_phone;
	/**
	 * The edittext allowing to modify the student's email address
	 */
	private EditText edit_mail;
	/**
	 * The edittext allowing to modify the student's picture path (temp)
	 */
	private EditText edit_picture_path;
	/**
	 * The textview displaying the first parent's surname
	 */
	private TextView txt_surname_p1;
	/**
	 * The textview displaying the first parent's name
	 */
	private TextView txt_first_name_p1;
	/**
	 * The textview displaying the first parent's birth date
	 */
	private TextView txt_birthday_p1;
	/**
	 * The textview displaying the first parent's address
	 */
	private TextView txt_address_p1;
	/**
	 * The textview displaying the first parent's phone number
	 */
	private TextView txt_phone_p1;
	/**
	 * The textview displaying the first parent's email address
	 */
	private TextView txt_mail_p1;
	/**
	 * The edittext allowing to modify the first parent's surname
	 */
	private EditText edit_surname_p1;
	/**
	 * The edittext allowing to modify the first parent's first name
	 */
	private EditText edit_first_name_p1;
	/**
	 * The edittext allowing to modify the first parent's birth date
	 */
	private EditText edit_birthday_p1;
	/**
	 * The edittext allowing to modify the first parent's addresss
	 */
	private EditText edit_address_p1;
	/**
	 * The edittext allowing to modify the first parent's phone number
	 */
	private EditText edit_phone_p1;
	/**
	 * The edittext allowing to modify the first parent's email address
	 */
	private EditText edit_mail_p1;
	/**
	 * The textview displaying the second parent's surname
	 */
	private TextView txt_surname_p2;
	/**
	 * The textview displaying the second parent's first name
	 */
	private TextView txt_first_name_p2;
	/**
	 * The textview displaying the second parent's birth date
	 */
	private TextView txt_birthday_p2;
	/**
	 * The textview displaying the second parent's address
	 */
	private TextView txt_address_p2;
	/**
	 * The textview displaying the second parent's phone number
	 */
	private TextView txt_phone_p2;
	/**
	 * The textview displaying the second parent's email address
	 */
	private TextView txt_mail_p2;
	/**
	 * The edittext allowing to modify the second parent's surname
	 */
	private EditText edit_surname_p2;
	/**
	 * The edittext allowing to modify the second parent's first name
	 */
	private EditText edit_first_name_p2;
	/**
	 * The edittext allowing to modify the second parent's birth date
	 */
	private EditText edit_birthday_p2;
	/**
	 * The edittext allowing to modify the second parent's address
	 */
	private EditText edit_address_p2;
	/**
	 * The edittext allowing to modify the second parent's phone number
	 */
	private EditText edit_phone_p2;
	/**
	 * The edittext allowing to modify the second parent's email address
	 */
	private EditText edit_mail_p2;
	/**
	 * The Switcher for the edittext and the textview of the student's surname
	 */
	private ViewSwitcher switcher_surname;
	/**
	 * The Switcher for the edittext and the textview of the student's first name
	 */
	private ViewSwitcher switcher_first_name;
	/**
	 * The Switcher for the edittext and the textview of the student's identity
	 */
	private ViewSwitcher switcher_identity;
	/**
	 * The Switcher for the edittext and the textview of the student's birth date
	 */
	private ViewSwitcher switcher_birthday;
	/**
	 * The Switcher for the edittext and the textview of the student's address
	 */
	private ViewSwitcher switcher_address;
	/**
	 * The Switcher for the edittext and the textview of the student's phone number
	 */
	private ViewSwitcher switcher_phone;
	/**
	 * The Switcher for the edittext and the textview of the student's email_address
	 */
	private ViewSwitcher switcher_mail;
	/**
	 * The Switcher for the edittext and the textview of the student's picture path (temp)
	 */
	private ViewSwitcher switcher_picture_path;
	/**
	 * The Switcher for the edittext and the textview of the first parent's surname
	 */
	private ViewSwitcher switcher_surname_p1;
	/**
	 * The Switcher for the edittext and the textview of the first parent's first name
	 */
	private ViewSwitcher switcher_first_name_p1;
	/**
	 * The Switcher for the edittext and the textview of the first parent's birth date
	 */
	private ViewSwitcher switcher_birthday_p1;
	/**
	 * The Switcher for the edittext and the textview of the first parent's address
	 */
	private ViewSwitcher switcher_address_p1;
	/**
	 * The Switcher for the edittext and the textview of the first parent's phone number
	 */
	private ViewSwitcher switcher_phone_p1;
	/**
	 * The Switcher for the edittext and the textview of the first parent's email address
	 */
	private ViewSwitcher switcher_mail_p1;
	/**
	 * The Switcher for the edittext and the textview of the second parent's surname
	 */
	private ViewSwitcher switcher_surname_p2;
	/**
	 * The Switcher for the edittext and the textview of the second parent's first name
	 */
	private ViewSwitcher switcher_first_name_p2;
	/**
	 * The Switcher for the edittext and the textview of the second parent's birth date
	 */
	private ViewSwitcher switcher_birthday_p2;
	/**
	 * The Switcher for the edittext and the textview of the second parent's address
	 */
	private ViewSwitcher switcher_address_p2;
	/**
	 * The Switcher for the edittext and the textview of the second parent's phone number
	 */
	private ViewSwitcher switcher_phone_p2;
	/**
	 * The Switcher for the edittext and the textview of the second parent's email address
	 */
	private ViewSwitcher switcher_mail_p2;
	/**
	 * The layout containing the student's detailed informations, hidden by default
	 */
	private LinearLayout layout_details;
	/**
	 * The layout containing the first parent's detailed informations, hidden by default
	 */
	private LinearLayout layout_parent1;
	/**
	 * The layout containing the second parent's detailed informations, hidden by default
	 */
	private LinearLayout layout_parent2;
	/**
	 * The layout containing the elements to add a new parent, visible only if there is less than 2 parents
	 */
	private LinearLayout layout_add_parent;
	/**
	 * Layout used as a fake layout to force the keyboard to hide
	 */
	private LinearLayout layout_lost_focus;
	/**
	 * The student's first parent
	 */
	private Parent parent1;
	/**
	 * The student's second parent
	 */
	private Parent parent2;
	/**
	 * Checkbox to hide/show first parent's details
	 */
	private CheckBox check_parent1;
	/**
	 * Checkbox to hide/show second parent's details
	 */
	private CheckBox check_parent2;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		//Required to manipulate the menu inside a fragment
		setHasOptionsMenu(true);

		ScrollView view = (ScrollView) inflater.inflate(R.layout.fragment_student_details, container, false);

		//Retrieve the student from the parent activity
		Bundle bundle = this.getArguments();
		student = (Student) bundle.getSerializable("student");

		//Get the student's parents (if any)
		ArrayList<Parent> parents = student.getAllParentOfStudent();
		parent1 = null;
		parent2 = null;
		if (parents.size() > 0) {
			parent1 = parents.get(0);
			if (parents.size() > 1)
				parent2 = parents.get(1);
		}

		//Find the three main layouts
		layout_details = (LinearLayout) view.findViewById(R.id.layout_student_details);
		layout_parent1 = (LinearLayout) view.findViewById(R.id.layout_parent_1);
		layout_parent2 = (LinearLayout) view.findViewById(R.id.layout_parent_2);

		//Find the fake layout
		layout_lost_focus = (LinearLayout) view.findViewById(R.id.layout_lost_focus);

		//Checkbox that hide or show the student's details
		CheckBox check_details = (CheckBox) view.findViewById(R.id.check_student_details);
		check_details.setOnClickListener(onDetailsClicked);

		check_parent1 = (CheckBox) view.findViewById(R.id.check_parent_1);
		check_parent1.setOnClickListener(onParent1Clicked);

		check_parent2 = (CheckBox) view.findViewById(R.id.check_parent_2);
		check_parent2.setOnClickListener(onParent2Clicked);

		//Button to add a parent
		ImageButton bt_plus = (ImageButton) view.findViewById(R.id.bt_add_student);
		bt_plus.setOnClickListener(onAddParentClick);
		
		//Textview to add a parent
		TextView txt_plus = (TextView) view.findViewById(R.id.txt_add_student);
		txt_plus.setOnClickListener(onAddParentClick);

		layout_add_parent = (LinearLayout) view.findViewById(R.id.layout_add_parent);

		/////Student
		initStudent(view);

		/////Parent 1
		if (parent1 != null) {
			initParent1(view);
		}

		/////Parent 2
		if (parent2 != null) {
			initParent2(view);
		}

		initView();

		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();

		//Update the parent's data
		ArrayList<Parent> parents = student.getAllParentOfStudent();
		parent1 = null;
		parent2 = null;
		if (parents.size() > 0) {
			parent1 = parents.get(0);
			if (parents.size() > 1)
				parent2 = parents.get(1);
		}

		//Update the fields
		initView();
	};

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_simple, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Fills the fields with the student's data 
	 */
	private void initView() {

		//Filling the students details
		
		txt_surname.setText(isAvailable(formatFirstUp(student.getSurname())));
		edit_surname.setText(student.getSurname());

		txt_first_name.setText(isAvailable(formatFirstUp(student.getName())));
		edit_first_name.setText(student.getName());

		txt_identity.setText(isAvailable(formatFirstUp(student.getIdentity())));
		edit_identity.setText(student.getIdentity());

		txt_birthday.setText(isAvailable(formatFirstUp(student.getBirthday())));
		edit_birthday.setText(student.getBirthday());

		txt_address.setText(isAvailable(formatFirstUp(student.getAddress())));
		edit_address.setText(student.getAddress());

		txt_phone.setText(isAvailable(formatFirstUp(student.getPhone())));
		edit_phone.setText(student.getPhone());

		txt_mail.setText(isAvailable(formatFirstUp(student.getMail())));
		edit_mail.setText(student.getMail());

		txt_picture_path.setText(isAvailable(formatFirstUp(student
				.getPicture_path())));
		edit_picture_path.setText(student.getPicture_path());

		// Filling parent 1 data

		if (parent1 != null) {

			if (txt_surname_p1 == null)
				initParent1(getView());

			check_parent1.setVisibility(CheckBox.VISIBLE);

			txt_surname_p1.setText(isAvailable(formatFirstUp(parent1
					.getSurname())));
			edit_surname_p1.setText(parent1.getSurname());

			txt_first_name_p1.setText(isAvailable(formatFirstUp(parent1
					.getFirstName())));
			edit_first_name_p1.setText(parent1.getFirstName());

			txt_birthday_p1.setText(isAvailable(formatFirstUp(parent1
					.getBirthday())));
			edit_birthday_p1.setText(parent1.getBirthday());

			txt_address_p1.setText(isAvailable(formatFirstUp(parent1
					.getAddress())));
			edit_address_p1.setText(parent1.getAddress());

			txt_phone_p1
					.setText(isAvailable(formatFirstUp(parent1.getPhone())));
			edit_phone_p1.setText(parent1.getPhone());

			txt_mail_p1.setText(isAvailable(formatFirstUp(parent1.getMail())));
			edit_mail_p1.setText(parent1.getMail());

			// Filling parent 2 data

			if (parent2 != null) {

				if (txt_surname_p2 == null)
					initParent2(getView());

				check_parent2.setVisibility(CheckBox.VISIBLE);

				txt_surname_p2.setText(isAvailable(formatFirstUp(parent2
						.getSurname())));
				edit_surname_p2.setText(parent2.getSurname());

				txt_first_name_p2.setText(isAvailable(formatFirstUp(parent2
						.getFirstName())));
				edit_first_name_p2.setText(parent2.getFirstName());

				txt_birthday_p2.setText(isAvailable(formatFirstUp(parent2
						.getBirthday())));
				edit_birthday_p2.setText(parent2.getBirthday());

				txt_address_p2.setText(isAvailable(formatFirstUp(parent2
						.getAddress())));
				edit_address_p2.setText(parent2.getAddress());

				txt_phone_p2.setText(isAvailable(formatFirstUp(parent2
						.getPhone())));
				edit_phone_p2.setText(parent2.getPhone());

				txt_mail_p2
						.setText(isAvailable(formatFirstUp(parent2.getMail())));
				edit_mail_p2.setText(parent2.getMail());

				layout_add_parent.setVisibility(LinearLayout.GONE);
			} else {
				layout_add_parent.setVisibility(LinearLayout.VISIBLE);
			}
		} else {
			layout_add_parent.setVisibility(LinearLayout.VISIBLE);
		}
	}

	/**
	 * Called when details checkbox is clicked. Hide or show the student's details.
	 */
	private OnClickListener onDetailsClicked = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (((CheckBox) v).isChecked())
				layout_details.setVisibility(LinearLayout.VISIBLE);
			else
				layout_details.setVisibility(LinearLayout.GONE);
		}
	};

	/**
	 * Called when parent 1 checkbox is clicked. Hide or show the parent's details.
	 */
	private OnClickListener onParent1Clicked = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (((CheckBox) v).isChecked())
				layout_parent1.setVisibility(LinearLayout.VISIBLE);
			else
				layout_parent1.setVisibility(LinearLayout.GONE);
		}
	};

	/**
	 * Called when parent 2 checkbox is clicked. Hide or show the parent's details.
	 */
	private OnClickListener onParent2Clicked = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (((CheckBox) v).isChecked())
				layout_parent2.setVisibility(LinearLayout.VISIBLE);
			else
				layout_parent2.setVisibility(LinearLayout.GONE);
		}
	};

	/**
	 * Called when the TextView or the edit button of student's surname is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_surname_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_surname.showNext();
			edit_surname.requestFocus();
			showKeyboard(edit_surname);
		}
	};

	/**
	 * Called when the save button for the student's surname is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_surname_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			student.setSurname(edit_surname.getText().toString());
			if (student.saveStudent())
				txt_surname.setText(isAvailable(student.getSurname()));
			switcher_surname.showNext();
			hideKeyboard(edit_surname);
		}
	};

	/**
	 * Called when the TextView or the edit button of student's first name is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_first_name_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_first_name.showNext();
			edit_first_name.requestFocus();
			showKeyboard(edit_first_name);
		}
	};

	/**
	 * Called when the save button for the student's first name is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_first_name_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			student.setName(edit_first_name.getText().toString());
			if (student.saveStudent())
				txt_first_name.setText(isAvailable(student.getName()));
			switcher_first_name.showNext();
			hideKeyboard(edit_first_name);
		}
	};

	/**
	 * Called when the TextView or the edit button of student's identity number is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_identity_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_identity.showNext();
			edit_identity.requestFocus();
			showKeyboard(edit_identity);
		}
	};

	/**
	 * Called when the save button for the student's identity number is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_identity_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			student.setIdentity(edit_identity.getText().toString());
			if (student.saveStudent())
				txt_identity.setText(isAvailable(student.getIdentity()));
			switcher_identity.showNext();
			hideKeyboard(edit_identity);
		}
	};

	/**
	 * Called when the TextView or the edit button of student's birth date is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_birthday_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_birthday.showNext();
			edit_birthday.requestFocus();
			showKeyboard(edit_birthday);
		}
	};

	/**
	 * Called when the save button for the student's birth date is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_birthday_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			student.setBirthday(edit_birthday.getText().toString());
			if (student.saveStudent())
				txt_birthday.setText(isAvailable(student.getBirthday()));
			switcher_birthday.showNext();
			hideKeyboard(edit_birthday);
		}
	};

	/**
	 * Called when the TextView or the edit button of student's address is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_address_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_address.showNext();
			edit_address.requestFocus();
			showKeyboard(edit_address);
		}
	};

	/**
	 * Called when the save button for the student's address is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_address_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			student.setAddress(edit_address.getText().toString());
			if (student.saveStudent())
				txt_address.setText(isAvailable(student.getAddress()));
			switcher_address.showNext();
			hideKeyboard(edit_address);
		}
	};

	/**
	 * Called when the TextView or the edit button of student's phone number is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_phone_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_phone.showNext();
			edit_phone.requestFocus();
			showKeyboard(edit_phone);
		}
	};

	/**
	 * Called when the save button for the student's phone number is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_phone_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			student.setPhone(edit_phone.getText().toString());
			if (student.saveStudent())
				txt_phone.setText(isAvailable(student.getPhone()));
			switcher_phone.showNext();
			hideKeyboard(edit_phone);
		}
	};

	/**
	 * Called when the TextView or the edit button of student's email address is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_mail_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_mail.showNext();
			edit_mail.requestFocus();
			showKeyboard(edit_mail);
		}
	};

	/**
	 * Called when the save button for the student's email address is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_mail_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			student.setMail(edit_mail.getText().toString());
			if (student.saveStudent())
				txt_mail.setText(isAvailable(student.getMail()));
			switcher_mail.showNext();
			hideKeyboard(edit_mail);
		}
	};

	/**
	 * Called when the TextView or the edit button of student's picture path is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_picture_path_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_picture_path.showNext();
			edit_picture_path.requestFocus();
			showKeyboard(edit_picture_path);
		}
	};

	/**
	 * Called when the save button for the student's picture path is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_picture_path_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			student.setPicture_path(edit_picture_path.getText().toString());
			if (student.saveStudent())
				txt_picture_path
						.setText(isAvailable(student.getPicture_path()));
			switcher_picture_path.showNext();
			hideKeyboard(edit_picture_path);
		}
	};

	/**
	 * Take a String and put an uppercase character
	 * @param The String to be changed
	 * @return The String modified
	 */
	private String formatFirstUp(String attribute) {
		if (attribute.equals(""))
			return attribute;
		return attribute.replaceFirst(".",
				(attribute.charAt(0) + "").toUpperCase());
	}

	//Parent 1 listeners

	/**
	 * Called when the TextView or the edit button of first parent's surname is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_surname_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_surname_p1.showNext();
			edit_surname_p1.requestFocus();
			showKeyboard(edit_surname_p1);
		}
	};

	/**
	 * Called when the save button for the first parent's surname is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_surname_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent1.setSurname(edit_surname_p1.getText().toString());
			if (parent1.saveParent())
				txt_surname_p1.setText(isAvailable(parent1.getSurname()));
			switcher_surname_p1.showNext();
			hideKeyboard(edit_surname_p1);
		}
	};

	/**
	 * Called when the TextView or the edit button of first parent's first name is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_first_name_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_first_name_p1.showNext();
			edit_first_name_p1.requestFocus();
			showKeyboard(edit_first_name_p1);
		}
	};

	/**
	 * Called when the save button for the first parent's first name is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_first_name_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent1.setFirstName(edit_first_name_p1.getText().toString());
			if (parent1.saveParent())
				txt_first_name_p1.setText(isAvailable(parent1.getFirstName()));
			switcher_first_name_p1.showNext();
			hideKeyboard(edit_first_name_p1);
		}
	};

	/**
	 * Called when the TextView or the edit button of first parent's birth date is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_birthday_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_birthday_p1.showNext();
			edit_birthday_p1.requestFocus();
			showKeyboard(edit_birthday_p1);
		}
	};

	/**
	 * Called when the save button for the first parent's birth date is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_birthday_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent1.setBirthday(edit_birthday_p1.getText().toString());
			if (parent1.saveParent())
				txt_birthday_p1.setText(isAvailable(parent1.getBirthday()));
			switcher_birthday_p1.showNext();
			hideKeyboard(edit_birthday_p1);
		}
	};

	/**
	 * Called when the TextView or the edit button of first parent's address is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_address_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_address_p1.showNext();
			edit_address_p1.requestFocus();
			showKeyboard(edit_address_p1);
		}
	};

	/**
	 * Called when the save button for the first parent's address is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_address_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent1.setAddress(edit_address_p1.getText().toString());
			if (parent1.saveParent())
				txt_address_p1.setText(isAvailable(parent1.getAddress()));
			switcher_address_p1.showNext();
			hideKeyboard(edit_address_p1);
		}
	};

	/**
	 * Called when the TextView or the edit button of first parent's phone number is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_phone_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_phone_p1.showNext();
			edit_phone_p1.requestFocus();
			showKeyboard(edit_phone_p1);
		}
	};

	/**
	 * Called when the save button for the first parent's phone number is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_phone_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent1.setPhone(edit_phone_p1.getText().toString());
			if (parent1.saveParent())
				txt_phone_p1.setText(isAvailable(parent1.getPhone()));
			switcher_phone_p1.showNext();
			hideKeyboard(edit_phone_p1);
		}
	};

	/**
	 * Called when the TextView or the edit button of first parent's email address is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_mail_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_mail_p1.showNext();
			edit_mail_p1.requestFocus();
			showKeyboard(edit_mail_p1);
		}
	};

	/**
	 * Called when the save button for the first parent's email address is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_mail_p1_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent1.setMail(edit_mail_p1.getText().toString());
			if (parent1.saveParent())
				txt_mail_p1.setText(isAvailable(parent1.getMail()));
			switcher_mail_p1.showNext();
			hideKeyboard(edit_mail_p1);
		}
	};

	// ////Parent 2

	/**
	 * Called when the TextView or the edit button of first parent's surname is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_surname_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_surname_p2.showNext();
			edit_surname_p2.requestFocus();
			showKeyboard(edit_surname_p2);
		}
	};

	/**
	 * Called when the save button for the second parent's surname is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_surname_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent2.setSurname(edit_surname_p2.getText().toString());
			if (parent2.saveParent())
				txt_surname_p2.setText(isAvailable(parent2.getSurname()));
			switcher_surname_p2.showNext();
			hideKeyboard(edit_surname_p2);
		}
	};

	/**
	 * Called when the TextView or the edit button of first parent's first name is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_first_name_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_first_name_p2.showNext();
			edit_first_name_p2.requestFocus();
			showKeyboard(edit_first_name_p2);
		}
	};

	/**
	 * Called when the save button for the second parent's first name is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_first_name_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent2.setFirstName(edit_first_name_p2.getText().toString());
			if (parent2.saveParent())
				txt_first_name_p2.setText(isAvailable(parent2.getFirstName()));
			switcher_first_name_p2.showNext();
			hideKeyboard(edit_first_name_p2);
		}
	};

	/**
	 * Called when the TextView or the edit button of first parent's birth date is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_birthday_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_birthday_p2.showNext();
			edit_birthday_p2.requestFocus();
			showKeyboard(edit_birthday_p2);
		}
	};

	/**
	 * Called when the save button for the second parent's birth date is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_birthday_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent2.setBirthday(edit_birthday_p2.getText().toString());
			if (parent2.saveParent())
				txt_birthday_p2.setText(isAvailable(parent2.getBirthday()));
			switcher_birthday_p2.showNext();
			hideKeyboard(edit_birthday_p2);
		}
	};

	/**
	 * Called when the TextView or the edit button of first parent's address is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_address_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_address_p2.showNext();
			edit_address_p2.requestFocus();
			showKeyboard(edit_address_p2);
		}
	};

	/**
	 * Called when the save button for the second parent's address is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_address_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent2.setAddress(edit_address_p2.getText().toString());
			if (parent2.saveParent())
				txt_address_p2.setText(isAvailable(parent2.getAddress()));
			switcher_address_p2.showNext();
			hideKeyboard(edit_address_p2);
		}
	};

	/**
	 * Called when the TextView or the edit button of first parent's phone number is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_phone_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_phone_p2.showNext();
			edit_phone_p2.requestFocus();
			showKeyboard(edit_phone_p2);
		}
	};

	/**
	 * Called when the save button for the second parent's phone number is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_phone_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent2.setPhone(edit_phone_p2.getText().toString());
			if (parent2.saveParent())
				txt_phone_p2.setText(isAvailable(parent2.getPhone()));
			switcher_phone_p2.showNext();
			hideKeyboard(edit_phone_p2);
		}
	};

	/**
	 * Called when the TextView or the edit button of first parent's email address is clicked. Show the EditText.
	 */
	private OnClickListener onEdit_mail_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switcher_mail_p2.showNext();
			edit_mail_p2.requestFocus();
			showKeyboard(edit_mail_p2);
		}
	};

	/**
	 * Called when the save button for the second parent's email address is clicked. Saves the changes into the database.
	 */
	private OnClickListener onSave_mail_p2_Clicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			parent2.setMail(edit_mail_p2.getText().toString());
			if (parent2.saveParent())
				txt_mail_p2.setText(isAvailable(parent2.getMail()));
			switcher_mail_p2.showNext();
			hideKeyboard(edit_mail_p2);
		}
	};

	/**
	 * Forces the keyboard to deploy for an EditText after that a switch has been performed
	 * @param edit The EditText that needs the keyboard
	 */
	private void showKeyboard(EditText edit) {
		InputMethodManager imm = (InputMethodManager) StudentDetailsFragment.this
				.getSherlockActivity().getSystemService(
						Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(edit, InputMethodManager.SHOW_FORCED);
	}

	/**
	 * Forces the keyboard to hide for an EditText after that a switch has been performed. Use a fake layout to change the focus.
	 * @param edit The EditText that doesn't need the keyboard anymore
	 */
	private void hideKeyboard(EditText edit) {
		InputMethodManager imm = (InputMethodManager) StudentDetailsFragment.this
				.getSherlockActivity().getSystemService(
						Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
		layout_lost_focus.requestFocus();
	}

	/**
	 * Return a default String if the String given in parameter is empty
	 * @param value The String to verify
	 * @return The String in parameter or a default String
	 */
	private String isAvailable(String value) {
		if (value.equals(""))
			return getString(R.string.not_available);
		return value;
	}

	/**
	 * Called when the button to add a parent is clicked. Launches an activity that allow the user to add a new Parent to the Student.
	 * @see ParentAddActivity
	 */
	private OnClickListener onAddParentClick = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent myIntent = new Intent(getSherlockActivity(), ParentAddActivity.class);
			myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			Bundle extra = new Bundle();
			extra.putSerializable("student", student);
			myIntent.putExtras(extra);
			startActivity(myIntent);
		}
	};

	/**
	 * Initialize the student's fields
	 * @param view The main view of this activity
	 */
	private void initStudent(View view) {
		
		//Views for the student's surname
		txt_surname = (TextView) view.findViewById(R.id.txt_surname_student);
		edit_surname = (EditText) view.findViewById(R.id.edit_surname_student);
		switcher_surname = (ViewSwitcher) view.findViewById(R.id.switch_surname_student);
		ImageButton bt_edit_surname = (ImageButton) view.findViewById(R.id.bt_edit_surname_student);
		ImageButton bt_save_surname = (ImageButton) view.findViewById(R.id.bt_save_surname_student);
		txt_surname.setOnClickListener(onEdit_surname_Clicked);
		bt_edit_surname.setOnClickListener(onEdit_surname_Clicked);
		bt_save_surname.setOnClickListener(onSave_surname_Clicked);

		//Views for the student's first name
		txt_first_name = (TextView) view.findViewById(R.id.txt_first_name_student);
		edit_first_name = (EditText) view.findViewById(R.id.edit_first_name_student);
		switcher_first_name = (ViewSwitcher) view.findViewById(R.id.switch_first_name_student);
		ImageButton bt_edit_first_name = (ImageButton) view.findViewById(R.id.bt_edit_first_name_student);
		ImageButton bt_save_first_name = (ImageButton) view.findViewById(R.id.bt_save_first_name_student);
		txt_first_name.setOnClickListener(onEdit_first_name_Clicked);
		bt_edit_first_name.setOnClickListener(onEdit_first_name_Clicked);
		bt_save_first_name.setOnClickListener(onSave_first_name_Clicked);

		//Views for the student's identity number
		txt_identity = (TextView) view.findViewById(R.id.txt_identity_student);
		edit_identity = (EditText) view.findViewById(R.id.edit_identity_student);
		switcher_identity = (ViewSwitcher) view.findViewById(R.id.switch_identity_student);
		ImageButton bt_edit_identity = (ImageButton) view.findViewById(R.id.bt_edit_identity_student);
		ImageButton bt_save_identity = (ImageButton) view.findViewById(R.id.bt_save_identity_student);
		txt_identity.setOnClickListener(onEdit_identity_Clicked);
		bt_edit_identity.setOnClickListener(onEdit_identity_Clicked);
		bt_save_identity.setOnClickListener(onSave_identity_Clicked);

		//Views for the student's birth date
		txt_birthday = (TextView) view.findViewById(R.id.txt_birthday_student);
		edit_birthday = (EditText) view.findViewById(R.id.edit_birthday_student);
		switcher_birthday = (ViewSwitcher) view.findViewById(R.id.switch_birthday_student);
		ImageButton bt_edit_birthday = (ImageButton) view.findViewById(R.id.bt_edit_birthday_student);
		ImageButton bt_save_birthday = (ImageButton) view.findViewById(R.id.bt_save_birthday_student);
		txt_birthday.setOnClickListener(onEdit_birthday_Clicked);
		bt_edit_birthday.setOnClickListener(onEdit_birthday_Clicked);
		bt_save_birthday.setOnClickListener(onSave_birthday_Clicked);

		//Views for the student's address
		txt_address = (TextView) view.findViewById(R.id.txt_address_student);
		edit_address = (EditText) view.findViewById(R.id.edit_address_student);
		switcher_address = (ViewSwitcher) view.findViewById(R.id.switch_address_student);
		ImageButton bt_edit_address = (ImageButton) view.findViewById(R.id.bt_edit_address_student);
		ImageButton bt_save_address = (ImageButton) view.findViewById(R.id.bt_save_address_student);
		txt_address.setOnClickListener(onEdit_address_Clicked);
		bt_edit_address.setOnClickListener(onEdit_address_Clicked);
		bt_save_address.setOnClickListener(onSave_address_Clicked);

		//Views for the student's phone number
		txt_phone = (TextView) view.findViewById(R.id.txt_phone_student);
		edit_phone = (EditText) view.findViewById(R.id.edit_phone_student);
		switcher_phone = (ViewSwitcher) view.findViewById(R.id.switch_phone_student);
		ImageButton bt_edit_phone = (ImageButton) view.findViewById(R.id.bt_edit_phone_student);
		ImageButton bt_save_phone = (ImageButton) view.findViewById(R.id.bt_save_phone_student);
		txt_phone.setOnClickListener(onEdit_phone_Clicked);
		bt_edit_phone.setOnClickListener(onEdit_phone_Clicked);
		bt_save_phone.setOnClickListener(onSave_phone_Clicked);

		//Views for the student's email address
		txt_mail = (TextView) view.findViewById(R.id.txt_mail_student);
		edit_mail = (EditText) view.findViewById(R.id.edit_mail_student);
		switcher_mail = (ViewSwitcher) view.findViewById(R.id.switch_mail_student);
		ImageButton bt_edit_mail = (ImageButton) view.findViewById(R.id.bt_edit_mail_student);
		ImageButton bt_save_mail = (ImageButton) view.findViewById(R.id.bt_save_mail_student);
		txt_mail.setOnClickListener(onEdit_mail_Clicked);
		bt_edit_mail.setOnClickListener(onEdit_mail_Clicked);
		bt_save_mail.setOnClickListener(onSave_mail_Clicked);

		//Views for the student's picture path
		txt_picture_path = (TextView) view.findViewById(R.id.txt_picture_path_student);
		edit_picture_path = (EditText) view.findViewById(R.id.edit_picture_path_student);
		switcher_picture_path = (ViewSwitcher) view.findViewById(R.id.switch_picture_path_student);
		ImageButton bt_edit_picture_path = (ImageButton) view.findViewById(R.id.bt_edit_picture_path_student);
		ImageButton bt_save_picture_path = (ImageButton) view.findViewById(R.id.bt_save_picture_path_student);
		txt_picture_path.setOnClickListener(onEdit_picture_path_Clicked);
		bt_edit_picture_path.setOnClickListener(onEdit_picture_path_Clicked);
		bt_save_picture_path.setOnClickListener(onSave_picture_path_Clicked);	
	}
	
	/**
	 * Initialize the first parent's fields
	 * @param view The main view of this activity
	 */
	private void initParent1(View view) {
		
		//Views for the surname
		txt_surname_p1 = (TextView) view.findViewById(R.id.txt_surname_parent_1);
		edit_surname_p1 = (EditText) view.findViewById(R.id.edit_surname_parent_1);
		switcher_surname_p1 = (ViewSwitcher) view.findViewById(R.id.switch_surname_parent_1);
		ImageButton bt_edit_surname_p1 = (ImageButton) view.findViewById(R.id.bt_edit_surname_parent_1);
		ImageButton bt_save_surname_p1 = (ImageButton) view.findViewById(R.id.bt_save_surname_parent_1);
		txt_surname_p1.setOnClickListener(onEdit_surname_p1_Clicked);
		bt_edit_surname_p1.setOnClickListener(onEdit_surname_p1_Clicked);
		bt_save_surname_p1.setOnClickListener(onSave_surname_p1_Clicked);

		//Views for the first name
		txt_first_name_p1 = (TextView) view.findViewById(R.id.txt_first_name_parent_1);
		edit_first_name_p1 = (EditText) view.findViewById(R.id.edit_first_name_parent_1);
		switcher_first_name_p1 = (ViewSwitcher) view.findViewById(R.id.switch_first_name_parent_1);
		ImageButton bt_edit_firstname_p1 = (ImageButton) view.findViewById(R.id.bt_edit_first_name_parent_1);
		ImageButton bt_save_firstname_p1 = (ImageButton) view.findViewById(R.id.bt_save_first_name_parent_1);
		txt_first_name_p1.setOnClickListener(onEdit_first_name_p1_Clicked);
		bt_edit_firstname_p1.setOnClickListener(onEdit_first_name_p1_Clicked);
		bt_save_firstname_p1.setOnClickListener(onSave_first_name_p1_Clicked);

		//Views for the birth date
		txt_birthday_p1 = (TextView) view.findViewById(R.id.txt_birthday_parent_1);
		edit_birthday_p1 = (EditText) view.findViewById(R.id.edit_birthday_parent_1);
		switcher_birthday_p1 = (ViewSwitcher) view.findViewById(R.id.switch_birthday_parent_1);
		ImageButton bt_edit_birthday_p1 = (ImageButton) view.findViewById(R.id.bt_edit_birthday_parent_1);
		ImageButton bt_save_birthday_p1 = (ImageButton) view.findViewById(R.id.bt_save_birthday_parent_1);
		txt_birthday_p1.setOnClickListener(onEdit_birthday_p1_Clicked);
		bt_edit_birthday_p1.setOnClickListener(onEdit_birthday_p1_Clicked);
		bt_save_birthday_p1.setOnClickListener(onSave_birthday_p1_Clicked);

		//Views for the address
		txt_address_p1 = (TextView) view.findViewById(R.id.txt_address_parent_1);
		edit_address_p1 = (EditText) view.findViewById(R.id.edit_address_parent_1);
		switcher_address_p1 = (ViewSwitcher) view.findViewById(R.id.switch_address_parent_1);
		ImageButton bt_edit_address_p1 = (ImageButton) view.findViewById(R.id.bt_edit_address_parent_1);
		ImageButton bt_save_address_p1 = (ImageButton) view.findViewById(R.id.bt_save_address_parent_1);
		txt_address_p1.setOnClickListener(onEdit_address_p1_Clicked);
		bt_edit_address_p1.setOnClickListener(onEdit_address_p1_Clicked);
		bt_save_address_p1.setOnClickListener(onSave_address_p1_Clicked);

		//Views for the phone number
		txt_phone_p1 = (TextView) view.findViewById(R.id.txt_phone_parent_1);
		edit_phone_p1 = (EditText) view.findViewById(R.id.edit_phone_parent_1);
		switcher_phone_p1 = (ViewSwitcher) view.findViewById(R.id.switch_phone_parent_1);
		ImageButton bt_edit_phone_p1 = (ImageButton) view.findViewById(R.id.bt_edit_phone_parent_1);
		ImageButton bt_save_phone_p1 = (ImageButton) view.findViewById(R.id.bt_save_phone_parent_1);
		txt_phone_p1.setOnClickListener(onEdit_phone_p1_Clicked);
		bt_edit_phone_p1.setOnClickListener(onEdit_phone_p1_Clicked);
		bt_save_phone_p1.setOnClickListener(onSave_phone_p1_Clicked);

		//Views for the email address
		txt_mail_p1 = (TextView) view.findViewById(R.id.txt_mail_parent_1);
		edit_mail_p1 = (EditText) view.findViewById(R.id.edit_mail_parent_1);
		switcher_mail_p1 = (ViewSwitcher) view.findViewById(R.id.switch_mail_parent_1);
		ImageButton bt_edit_mail_p1 = (ImageButton) view.findViewById(R.id.bt_edit_mail_parent_1);
		ImageButton bt_save_mail_p1 = (ImageButton) view.findViewById(R.id.bt_save_mail_parent_1);
		txt_mail_p1.setOnClickListener(onEdit_mail_p1_Clicked);
		bt_edit_mail_p1.setOnClickListener(onEdit_mail_p1_Clicked);
		bt_save_mail_p1.setOnClickListener(onSave_mail_p1_Clicked);
	}

	/**
	 * @param view
	 */
	private void initParent2(View view) {
		
		//Views for the surname
		txt_surname_p2 = (TextView) view.findViewById(R.id.txt_surname_parent_2);
		edit_surname_p2 = (EditText) view.findViewById(R.id.edit_surname_parent_2);
		switcher_surname_p2 = (ViewSwitcher) view.findViewById(R.id.switch_surname_parent_2);
		ImageButton bt_edit_surname_p2 = (ImageButton) view.findViewById(R.id.bt_edit_surname_parent_2);
		ImageButton bt_save_surname_p2 = (ImageButton) view.findViewById(R.id.bt_save_surname_parent_2);
		txt_surname_p2.setOnClickListener(onEdit_surname_p2_Clicked);
		bt_edit_surname_p2.setOnClickListener(onEdit_surname_p2_Clicked);
		bt_save_surname_p2.setOnClickListener(onSave_surname_p2_Clicked);

		//Views for the first name
		txt_first_name_p2 = (TextView) view.findViewById(R.id.txt_first_name_parent_2);
		edit_first_name_p2 = (EditText) view.findViewById(R.id.edit_first_name_parent_2);
		switcher_first_name_p2 = (ViewSwitcher) view.findViewById(R.id.switch_first_name_parent_2);
		ImageButton bt_edit_firstname_p2 = (ImageButton) view.findViewById(R.id.bt_edit_first_name_parent_2);
		ImageButton bt_save_firstname_p2 = (ImageButton) view.findViewById(R.id.bt_save_first_name_parent_2);
		txt_first_name_p2.setOnClickListener(onEdit_first_name_p2_Clicked);
		bt_edit_firstname_p2.setOnClickListener(onEdit_first_name_p2_Clicked);
		bt_save_firstname_p2.setOnClickListener(onSave_first_name_p2_Clicked);

		//Views for the birth date
		txt_birthday_p2 = (TextView) view.findViewById(R.id.txt_birthday_parent_2);
		edit_birthday_p2 = (EditText) view.findViewById(R.id.edit_birthday_parent_2);
		switcher_birthday_p2 = (ViewSwitcher) view.findViewById(R.id.switch_birthday_parent_2);
		ImageButton bt_edit_birthday_p2 = (ImageButton) view.findViewById(R.id.bt_edit_birthday_parent_2);
		ImageButton bt_save_birthday_p2 = (ImageButton) view.findViewById(R.id.bt_save_birthday_parent_2);
		txt_birthday_p2.setOnClickListener(onEdit_birthday_p2_Clicked);
		bt_edit_birthday_p2.setOnClickListener(onEdit_birthday_p2_Clicked);
		bt_save_birthday_p2.setOnClickListener(onSave_birthday_p2_Clicked);

		//Views for the address
		txt_address_p2 = (TextView) view.findViewById(R.id.txt_address_parent_2);
		edit_address_p2 = (EditText) view.findViewById(R.id.edit_address_parent_2);
		switcher_address_p2 = (ViewSwitcher) view.findViewById(R.id.switch_address_parent_2);
		ImageButton bt_edit_address_p2 = (ImageButton) view.findViewById(R.id.bt_edit_address_parent_2);
		ImageButton bt_save_address_p2 = (ImageButton) view.findViewById(R.id.bt_save_address_parent_2);
		txt_address_p2.setOnClickListener(onEdit_address_p2_Clicked);
		bt_edit_address_p2.setOnClickListener(onEdit_address_p2_Clicked);
		bt_save_address_p2.setOnClickListener(onSave_address_p2_Clicked);

		//Views for the phone number
		txt_phone_p2 = (TextView) view.findViewById(R.id.txt_phone_parent_2);
		edit_phone_p2 = (EditText) view.findViewById(R.id.edit_phone_parent_2);
		switcher_phone_p2 = (ViewSwitcher) view.findViewById(R.id.switch_phone_parent_2);
		ImageButton bt_edit_phone_p2 = (ImageButton) view.findViewById(R.id.bt_edit_phone_parent_2);
		ImageButton bt_save_phone_p2 = (ImageButton) view.findViewById(R.id.bt_save_phone_parent_2);
		txt_phone_p2.setOnClickListener(onEdit_phone_p2_Clicked);
		bt_edit_phone_p2.setOnClickListener(onEdit_phone_p2_Clicked);
		bt_save_phone_p2.setOnClickListener(onSave_phone_p2_Clicked);

		//Views for the email address
		txt_mail_p2 = (TextView) view.findViewById(R.id.txt_mail_parent_2);
		edit_mail_p2 = (EditText) view.findViewById(R.id.edit_mail_parent_2);
		switcher_mail_p2 = (ViewSwitcher) view.findViewById(R.id.switch_mail_parent_2);
		ImageButton bt_edit_mail_p2 = (ImageButton) view.findViewById(R.id.bt_edit_mail_parent_2);
		ImageButton bt_save_mail_p2 = (ImageButton) view.findViewById(R.id.bt_save_mail_parent_2);
		txt_mail_p2.setOnClickListener(onEdit_mail_p2_Clicked);
		bt_edit_mail_p2.setOnClickListener(onEdit_mail_p2_Clicked);
		bt_save_mail_p2.setOnClickListener(onSave_mail_p2_Clicked);
	}
}
