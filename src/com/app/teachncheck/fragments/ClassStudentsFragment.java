package com.app.teachncheck.fragments;


import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.AddStudentClassActivity;
import com.app.teachncheck.ClassActivity;
import com.app.teachncheck.MainActivity;
import com.app.teachncheck.R;
import com.app.teachncheck.StudentActivity;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.bdd.Student;

/**
 * <b>This fragment is designed to display a list of all students in a classroom.</b>
 * <p>This fragment is instantiated by {@link ClassActivity}.</p>
 * <p>In this fragment, the user can :</p>
 * <ul>
 * 		<li>See the student's list</li>
 * 		<li>Access a student's details</li>
 * 		<li>Add one or more students</li>
 * 		<li>Remove a student from the classroom</li>
 * </ul>
 * <p>Uses the fragment_class_students layout.</p>
 * 
 * @see StudentActivity
 */
public class ClassStudentsFragment extends SherlockFragment {
    
	/**
	 * List of all students in the classroom
	 */
	private ArrayList<Student> listStudents;
	/**
	 * Classroom whose students are listed
	 */
	private Classroom classroom;
	/**
	 * ListView listing students
	 */
	private ListView listView_StudentList;
	/**
	 * Indicates how many students are currently selected
	 */
	private int nbChoice;
	/**
	 * ActionMode currently effective
	 */
	private ActionMode mMode;
	/**
	 * Head of the CheckBoxes column
	 */
	private TextView textDetails;
	/**
	 * Icon trash, indicates that the user can select the student he want to remove from the classroom
	 */
	private ImageView iconDelete;	

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
		setHasOptionsMenu(true);
		
		LinearLayout view = (LinearLayout) inflater.inflate(R.layout.fragment_class_students, container, false);
		
		//Retrieve classroom
		Bundle bundle = this.getArguments();
		classroom = (Classroom) bundle.getSerializable("class");
		
		//Used only in listeners as a context
		final SherlockFragmentActivity activity = getSherlockActivity();
		
		//Manage clicks on each students in the list
		//	if no ActionMode activated, then a click will show the student's details
		//  otherwise the ActionMode is closed if there is no student selected  	
		listView_StudentList = (ListView) view.findViewById(R.id.list_students);
		listView_StudentList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				
				if(listView_StudentList.isItemChecked(position))
					nbChoice++;
				else
					nbChoice--;

				if(mMode==null){		
					Bundle bundle = new Bundle();
					bundle.putSerializable("student",listStudents.get(position));
					Intent newIntent = new Intent(view.getContext(), StudentActivity.class);
					newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
					newIntent.putExtras(bundle);
					startActivity(newIntent);	
				}else if(nbChoice == 0){
					switchVisibility(TextView.VISIBLE, ImageView.GONE);
					mMode.finish();
				}
			}
		});
		
		//Launch the ActionMode after a long click on a student in the list
		listView_StudentList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(mMode==null){
					mMode = activity.startActionMode(new DeleteActionMode());
					listView_StudentList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
					switchVisibility(TextView.GONE, ImageView.VISIBLE);
				}
				return false;
			}
		});
		
		listView_StudentList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		textDetails = (TextView) view.findViewById(R.id.text_open);
		iconDelete = (ImageView) view.findViewById(R.id.icon_delete);
    	
    	return view;
    }
	
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    	menu.clear();
        inflater.inflate(R.menu.menu_fragment_class_students_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_add_student:
			gotoAddStudent();
	        return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
    /**
	 * <b>Action mode used to remove one or more student from the classroom.</b>
	 *  Activated when a user perform a long click on a student in the ListView.
	 */
	private final class DeleteActionMode implements ActionMode.Callback {
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			menu.clear();
			MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
			inflater.inflate(R.menu.menu_delete, menu);
			nbChoice = 0;
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

			switch (item.getItemId()) {
			case R.id.delete:	
				SparseBooleanArray listModified = listView_StudentList.getCheckedItemPositions();
				for(int pos=0;pos<listModified.size();pos++){
					if(listModified.get(listModified.keyAt(pos))==true){
						Student student1 = listStudents.get(listModified.keyAt(pos));
						MainActivity.getDb().deleteClass_Student(classroom.getId(), student1.getId());
					}
				}
				initList();
				mMode.finish();
				return true;
				
			default:
				return true;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			for(int pos=0;pos<listView_StudentList.getAdapter().getCount();pos++){
				listView_StudentList.setItemChecked(pos, false);
			}
			listView_StudentList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			
			switchVisibility(TextView.VISIBLE, ImageView.GONE);
			
			mMode = null;
		}	
	}

    @Override
    public void onResume(){
    	super.onResume();
    	initList();
    }
    
    /**
     * Set the visibility state of textDetails and iconDelete
     * @param textState : visibility state to apply (GONE or VISIBLE) to textDetails
     * @param iconState : visibility state to apply (GONE or VISIBLE) to iconDelete
     */
    private void switchVisibility(int textState, int iconState){
		textDetails.setVisibility(textState);
		iconDelete.setVisibility(iconState);
    }

	public void onStop(){
		super.onStop();
		if(mMode!=null)
			mMode.finish();
	}
	
	/**
	 * Fills the ListView with students or display the "No Student" TextView
	 */
	private void initList(){
    	switchVisibility(TextView.VISIBLE, ImageView.GONE);
		listStudents = classroom.getAllStudentsOfClassroom();
		if(listStudents.isEmpty())
			((TextView) getView().findViewById(R.id.empty_list)).setVisibility(TextView.VISIBLE);
		else
			listView_StudentList.setAdapter(new ArrayAdapter<Student>(getSherlockActivity(),android.R.layout.simple_list_item_multiple_choice, listStudents));
	}
	
	/**
	 * Launch an activity allowing to add other students to the classroom
	 */
    private void gotoAddStudent(){
    	Bundle bundle = new Bundle();
		bundle.putSerializable("class",classroom);
		Intent newIntent = new Intent(getSherlockActivity(), AddStudentClassActivity.class);
		newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		newIntent.putExtras(bundle);
		
		//Indicates to AddStudentClassActivity that the call is special because it is a call for result, which is not the case if the call come from other activities
		newIntent.putExtra("special","true");
		
		startActivity(newIntent);
    }
}
