package com.app.teachncheck.fragments;


import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.ParentAddActivity;
import com.app.teachncheck.R;
import com.app.teachncheck.StudentActivity;
import com.app.teachncheck.bdd.Mark;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.bdd.StudentMark;

/**
 * <b>StudentMarksFragment allows to check, modify, and delete the student's marks.</b>
 * <p>This fragment is instantiated by {@link StudentActivity}.</p>
 * <p>In this activity, the user can :</p>
 * <ul>
 * 		<li>See the student marks</li>
 * 		<li>Modify any of these marks</li>
 * 		<li>Delete a mark</li>
 * </ul>
 * <p>Uses the fragment_student_marks layout.</p>
 * 
 * @see ParentAddActivity
 * @see StudentActivity
 */
public class StudentMarksFragment extends SherlockFragment {

	/**
	 * The student whose marks are displayed
	 */
	private Student student;
	/**
	 * The current action mode
	 */
	private ActionMode mMode;
	/**
	 * Number of marks selected
	 */
	private int nbMarksSelected;
	/**
	 * List of checkboxes (one for each student's mark)
	 */
	private ArrayList<CheckBox> list_checks; 
	/**
	 * Table containing the student's marks
	 */
	private TableLayout table_marks;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		//Required to manipulate the menu inside a fragment
		setHasOptionsMenu(true);

		LinearLayout view = (LinearLayout) inflater.inflate(R.layout.fragment_student_marks, container, false);

		//Retrieves the student
		Bundle bundle = this.getArguments();
		student = (Student) bundle.getSerializable("student");

		list_checks = new ArrayList<CheckBox>();
		
		table_marks = (TableLayout) view.findViewById(R.id.tab_marks);

		return view;
	}

	@Override
	public void onCreateOptionsMenu (Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_simple, menu);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		initTab();
	}

	/**
	 * Fills the table of marks
	 */
	private void initTab(){
		
		//Retrieve student's marks from database
		ArrayList<StudentMark> listMarks = student.getAllMarkOfStudent();
		
		//Purge the table
		table_marks.removeAllViews();
		
		if(listMarks.isEmpty()){
			
			((TextView) getView().findViewById(R.id.empty_list)).setVisibility(TextView.VISIBLE);
		
		}else{
			
			for(StudentMark studentMark : listMarks){
	
				Mark mark = studentMark.getMark();
	
				//Create a new line
				TableRow row = (TableRow) getSherlockActivity().getLayoutInflater().inflate(R.layout.item_student_mark, null);
	
				//Add the name of the mark and the value obtained by the student
				((TextView)row.findViewById(R.id.txt_mark_name)).setText(mark.getName());
				((TextView)row.findViewById(R.id.txt_mark_value)).setText(studentMark.getvalue() + "/" + mark.getMax_value());
				
				//Add a checkbox at the end of the line
				final CheckBox check_mark = (CheckBox)row.findViewById(R.id.check_mark);
				check_mark.setTag(studentMark);
				list_checks.add(check_mark);
				
				//Set the listener that will launch or destroy an action mode
				row.setOnClickListener(new OnClickListener() {			
					@Override
					public void onClick(View v) {
						
						//Count the number of marks selected, and force the check state
						if(check_mark.isChecked()){
							check_mark.setChecked(false);
							nbMarksSelected--;
						}else{
							check_mark.setChecked(true);
							nbMarksSelected++;
						}
	
						//If no mode is started yet, the default action mode is launched
						if(mMode == null){
							nbMarksSelected = 0;
							mMode = getSherlockActivity().startActionMode(new DeleteAndEditActionMode());
						}
						//If no mark selected then the action mode is stopped
						else if(nbMarksSelected == 0){
							mMode.finish();
						}
						//If only one mark is selected then the delete and edit menu is displayed
						else if (nbMarksSelected == 1){
							Menu menu = mMode.getMenu();
							menu.clear();
							MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
							inflater.inflate(R.menu.menu_delete_plus_modif, menu);
						}
						//If more than one mark is selected then the delete menu is displayed
						else if (nbMarksSelected == 2){
							Menu menu = mMode.getMenu();
							menu.clear();
							MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
							inflater.inflate(R.menu.menu_delete, menu);
						}
					}
				});
	
				//Add the new line to the table
				table_marks.addView(row,new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			}
		}
	}

	@Override
	public void onStop(){
		super.onStop();
		if(mMode!=null)
			mMode.finish();
	}

	/**
	 * <b>Action mode used to delete or edit a student's mark</b>
	 *  Activated when one or more checkbox of mark is clicked.
	 */
	private final class DeleteAndEditActionMode implements ActionMode.Callback {
		
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
			inflater.inflate(R.menu.menu_delete_plus_modif, menu);
			nbMarksSelected = 1;
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

			switch (item.getItemId()) {
			case R.id.delete:

				final ArrayList<StudentMark> l_sm_to_delete = new ArrayList<StudentMark>();
				for(CheckBox check : list_checks){
					if(check.isChecked())
						l_sm_to_delete.add((StudentMark) check.getTag());
				}

				AlertDialog deleteDialog = new AlertDialog.Builder(getSherlockActivity()).create();
				deleteDialog.setTitle(getString(R.string.confirmation));
				deleteDialog.setMessage(getString(R.string.delete_confirmation_class));
				deleteDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						for(StudentMark sm:l_sm_to_delete){
							sm.deleteMark();	
						}
						initTab();
					}});
				deleteDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}});

				deleteDialog.show();

				mMode.finish();
				return true;

			case R.id.modify:

				StudentMark sm = null;
				for(CheckBox check : list_checks){
					if(check.isChecked()){
						sm = (StudentMark) check.getTag();
						break;
					}
				}
				
				//Launch a dialog to modify a mark 
				DialogFragment modifyMarkDialog = new ModifyMarkDialogFragment();
				Bundle bundle = new Bundle();
				bundle.putSerializable("studentMark", sm);
				modifyMarkDialog.setArguments(bundle);
				modifyMarkDialog.show(getSherlockActivity().getSupportFragmentManager(), "modifyMark");
				
				mMode.finish();

				return true;

			default:
				return true;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			
			for(CheckBox check : list_checks){
				check.setChecked(false);
			}

			mMode = null;
		}
	}
	
	/**
	 * <b>Dialog used to modify a mark's value.</b>
	 */
	private class ModifyMarkDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflater = getActivity().getLayoutInflater();

			LinearLayout linear = (LinearLayout)inflater.inflate(R.layout.dialog_modify_student_mark, null);
			final EditText editValue = (EditText) linear.findViewById(R.id.edit_mark_value);
			final TextView textMax = (TextView) linear.findViewById(R.id.text_mark_max);
			
			final StudentMark studentMark = (StudentMark) getArguments().getSerializable("studentMark");
			Mark mark = studentMark.getMark();
			
			editValue.setText(studentMark.getvalue());
			textMax.setText("/"+mark.getMax_value());

			builder.setTitle(R.string.modify)
			.setView(linear)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {

					studentMark.setvalue(editValue.getText().toString());
					studentMark.saveMark();
					initTab();
					
					ModifyMarkDialogFragment.this.getDialog().dismiss();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					ModifyMarkDialogFragment.this.getDialog().cancel();
				}
			});   

			Dialog d = builder.create();

			return d;
		}
	}
}


