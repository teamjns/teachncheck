package com.app.teachncheck.fragments;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.AddStudentClassActivity;
import com.app.teachncheck.BackupActivity;
import com.app.teachncheck.ClassAddActivity;
import com.app.teachncheck.ClassExportActivity;
import com.app.teachncheck.ClassImportActivity;
import com.app.teachncheck.CreateMarkActivity;
import com.app.teachncheck.MainActivity;
import com.app.teachncheck.R;
import com.app.teachncheck.StudentActivity;
import com.app.teachncheck.bdd.Absence;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.bdd.Mark;
import com.app.teachncheck.bdd.Schedule;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.classes.CheckBoxPerso;
import com.app.teachncheck.classes.LayoutPerso;


/**
 * <b>ManagerFragment is the first fragment displayed in the application. It is used to manage the students of a classroom.</b>
 * <p>This fragment is instantiated by {@link MainActivity}.</p>
 * <p>In this activity, the user can :</p>
 * <ul>
 * 		<li>See the student list of the current class</li>
 * 		<li>Add a time slot for the current day</li>
 * 		<li>Take attendance for students</li>
 * 		<li>Select a student to see his details data</li>
 * 		<li>Add a grade</li>
 * 		<li>Import or export data</li>
 * </ul>
 * <p>Uses the fragment_manager layout.</p>
 * 
 * @see AddStudentClassActivity
 * @see ClassAddActivity
 * @see ClassExportActivity
 * @see ClassImportActivity
 * @see CreateMarkActivity
 * @see MainActivity
 * @see StudentActivity
 * @see BackupActivity
 */
public class ManagerFragment extends SherlockFragment {

	/**
	 * Classroom currently managed
	 */
	private Classroom classroom = null;
	/**
	 * Buttons to create the first classroom and the first student of a class
	 */
	private Button bt_first_class,bt_first_student;
	/**
	 * Table containing all students
	 */
	private TableLayout table_students;
	/**
	 * Map of all the attendance checkbox
	 */
	private HashMap<Schedule, ArrayList<CheckBoxPerso>> list_checkbox;
	/**
	 * List of textviews that must be clickable or not depending of the situation
	 */
	private ArrayList<TextView> list_StudentNames;
	
	/**
	 * Time slot for which the verification of attendance is being made
	 */
	private Schedule current_schedule;
	
	/**
	 * Action mode currently instantiated
	 */
	private ActionMode mMode;

	/**
	 * Activity flags
	 */
	private int MARK_CREATED = 0, CLASS_ADDED = 1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		
		//Required to manipulate the menu inside a fragment
		setHasOptionsMenu(true);

		RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.fragment_manager, container, false);

		//Button to create the first classroom if the list is empty 
		bt_first_class = (Button) view.findViewById(R.id.bt_first_classroom);
		bt_first_class.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(v.getContext(),ClassAddActivity.class);
				startActivityForResult(intent, CLASS_ADDED);
			}
		});
		
		//Button to add the first student to an empty classroom
		bt_first_student = (Button) view.findViewById(R.id.bt_first_student);
		bt_first_student.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(v.getContext(),AddStudentClassActivity.class);
				intent.putExtra("class", classroom);
				startActivityForResult(intent, CLASS_ADDED);
			}
		});

		table_students = (TableLayout) view.findViewById(R.id.student_table);

		return view;
	}
	
	/**	
	 * {@inheritDoc}
	 */
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initTable();
	}

	/**	
	 * {@inheritDoc}
	 */
	@Override
	public void onCreateOptionsMenu (Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.menu_activity_manager, menu);
	}

	/**	
	 * {@inheritDoc}
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		//Add time slot item selected
		case R.id.menu_add_slot:
			if(classroom != null){
				DialogFragment newSlot = new NewSlotDialogFragment();
				newSlot.show(getSherlockActivity().getSupportFragmentManager(), "newSlot");
			}
			return true;
		//Add mark item selected
		case R.id.menu_add_mark:
			if(classroom != null){
				DialogFragment newMark = new NewMarkDialogFragment();
				newMark.show(getSherlockActivity().getSupportFragmentManager(), "newMark");
			}
			return true;
		//Import item selected
		case R.id.menu_import:
			Intent myIntent2 = new Intent(getSherlockActivity(), ClassImportActivity.class);
	    	myIntent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
	        startActivity(myIntent2);
	        return true;
	    //Export item selected
		case R.id.menu_export:
			Intent myIntent3 = new Intent(getSherlockActivity(), ClassExportActivity.class);
	    	myIntent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
	        startActivity(myIntent3);
	        return true;

	    //Export bdd item selected
		case R.id.menu_bdd:
			Intent myIntent4 = new Intent(getSherlockActivity(), BackupActivity.class);
	    	myIntent4.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
	        startActivity(myIntent4);
	        return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Method used to fill the main table with the students names and the checkboxes
	 */
	private void initTable(){
		
		classroom = null;
		list_StudentNames = new ArrayList<TextView>();
		
		//Retrieve the current classroom id from the application files
		int id_class = readClassroomIdFromInternalStorage();
		
		//Retrieve the current classroom from the database
		if(id_class >= 0)
			classroom = MainActivity.getDb().getClassroom(id_class);
		
		//If there is no current classroom, the first classroom is picked instead
		if (classroom == null){
			resetFileToInternalStorage();
			ArrayList<Classroom> classrooms = MainActivity.getDb().getAllClassroom();
			if(classrooms.size() > 0){
				classroom = classrooms.get(0);
			}else{
				bt_first_class.setVisibility(View.VISIBLE);
				getSherlockActivity().getSupportActionBar().setTitle(R.string.app_name);
			}
		}

		if(classroom != null){
			
			//Set title of the actionbar
			getSherlockActivity().getSupportActionBar().setTitle(classroom.getName().toUpperCase());

			//Retrieve all students and schedules of the classroom
			ArrayList<Student> listStudents = classroom.getAllStudentsOfClassroom();
			ArrayList<Schedule> list_creneaux = classroom.getAllSchedulesOfClassroom();

			list_checkbox = new HashMap<Schedule, ArrayList<CheckBoxPerso>>();
			
			
			//Filling of the columns titles ////////////////////////
			
			TableRow r = new TableRow(getSherlockActivity());
			TextView studentText = createTitleStyled();
			r.addView(studentText);

			if(list_creneaux.size() == 0)
				r.addView(createSlot(null));
			else
				for(Schedule schedule: list_creneaux){
					list_checkbox.put(schedule, new ArrayList<CheckBoxPerso>());
					r.addView(createSlot(schedule));
				}
			
			table_students.removeAllViews();
			table_students.addView(r,new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			table_students.addView(createLine());

			//Filling of the students list////////////////////
			
			if(listStudents.isEmpty()){
				bt_first_student.setVisibility(View.VISIBLE);
			}else{
				bt_first_student.setVisibility(View.GONE);
				
				//Add a new line for each student with a checkbox for each time slot
				for(final Student student: listStudents){
					table_students.addView(createStudentRow(student,list_creneaux),new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				}
	
				//Set the checkboxes according to the database
				for(Schedule s : list_creneaux){
					initChecks(s);
				}
			}
		}
	}

	/**
	 * Called when an attendance checkbox is clicked 
	 */
	private OnClickListener checkClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			//Check that no action mode is activated = normal mode
			if(mMode == null){
				
				current_schedule  = ((CheckBoxPerso) v).getSchedule();

				//Disable all checkboxes that don't match the selected schedule and the student names to avoid all mistake
				for(Schedule sch : list_checkbox.keySet()){
					if(sch.getId() != current_schedule.getId()){
						ArrayList<CheckBoxPerso> list_check = list_checkbox.get(sch);
						for(CheckBoxPerso check : list_check){
							check.setEnabled(false);
						}
						for(TextView text: list_StudentNames){
							text.setClickable(false);
						}
					}
				}

				//Start the attendance checking action mode
				mMode = getSherlockActivity().startActionMode(new CheckActionMode());
			}
		}
	};


	/**
	 * Called when the add slot button is touched
	 */
	private OnTouchListener showTimePicker = new OnTouchListener() {
		public boolean onTouch (View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN){		
				//Create and display the dialog that allow to create a new time slot
				DialogFragment newFragment = new TimePickerForSchedule((EditText)v);
				newFragment.show(getSherlockActivity().getSupportFragmentManager(), "timePicker");
			}
			return true;
		}
	};

	/**
	 * Called when a long click is performed on a checkbox of a schedule
	 */
	private OnLongClickListener checkLongClickListener = new OnLongClickListener() {
		@Override
		public boolean onLongClick(View v) {
			final Schedule schedule  = ((CheckBoxPerso) v).getSchedule();
			
			//Display a dialog box that allow the user to delete a schedule
			createAndDisplayDeleteScheduleDialog(schedule);
			
			return true;
		}
	};
	
	/**
	 * Called when a long click is performed on head cell of a schedule
	 */
	private OnLongClickListener layoutLongClickListener = new OnLongClickListener() {

		@Override
		public boolean onLongClick(View v) {
			final Schedule schedule  = ((LayoutPerso) v).getSchedule();
			
			//Display a dialog box that allow the user to delete a schedule
			createAndDisplayDeleteScheduleDialog(schedule);
			
			return true;
		}
	};

	/**
	 * Create and display a dialog that allows to delete a schedule for the current classroom
	 * @param schedule The time slot to delete
	 */
	private void createAndDisplayDeleteScheduleDialog(final Schedule schedule){

			AlertDialog deleteScheduleDialog = new AlertDialog.Builder(getSherlockActivity()).create();
			deleteScheduleDialog.setTitle(getString(R.string.delete));
			deleteScheduleDialog.setMessage(getString(R.string.delete_schedule_confirmation));
			deleteScheduleDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.delete_schedule), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					schedule.deleteSchedule();
					initTable();
				}});
	
			deleteScheduleDialog.show();
	}

	/**
	 * <b>Action mode used to make the attendance record by checking the students presence</b>
	 *  Activated when the user check any students attendance checkbox. This mode display save button to save the attendance record.
	 */
	private final class CheckActionMode implements ActionMode.Callback {

		/**
		 * Indicates whether the attendance record must be saved or not
		 */
		private boolean modified = false;
		
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
			inflater.inflate(R.menu.menu_save_checks, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

			switch (item.getItemId()) {
			case R.id.save_checks:
				//Save the attendance record
				saveChecks();
				modified = true;
				mMode.finish();
				return true;
			default:
				return true;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			
			//Reenable all checkboxes and students name
			for(Schedule schedule : list_checkbox.keySet()){
				ArrayList<CheckBoxPerso> list_check = list_checkbox.get(schedule);
				for(CheckBoxPerso check : list_check){
					check.setEnabled(true);
				}
			}
			for(TextView text: list_StudentNames){
				text.setClickable(true);
			}

			//Reinitialize the checkbox according to the database if the changes must be canceled 
			if(!modified)
				initChecks(current_schedule);

			mMode = null;
		}
	}

	/**
	 * Save the attendance record to the database
	 */
	private void saveChecks(){

		//If the attendance for this schedule is not yet done, then an absence is created for each missing student 
		if(!current_schedule.isChecked()){
			
			current_schedule.setChecked(true);
			current_schedule.saveSchedule();

			for(CheckBoxPerso check : list_checkbox.get(current_schedule)){
				if(!check.isChecked()){
					Student student = check.getStudent();
					Absence absence = new Absence(false,"",student,current_schedule);
					absence.initAbsence();
				}
			}
		}
		
		//If the attendance was already done once, then only the differences with the previous attendance record are saved
		else{
			for(CheckBoxPerso check : list_checkbox.get(current_schedule)){
				Student student = check.getStudent();
				Absence absence = MainActivity.getDb().getAbsence(current_schedule, student);
				if(absence == null){
					if(!check.isChecked()){
						absence = new Absence(false,"",student,current_schedule);
						absence.initAbsence();
					}
				}else{
					if(check.isChecked()){
						absence.deleteAbsence();
					}
				}
			}
		}

	}

	/**
	 * Initialize the checkboxes state according to the database. If a student has an absence for a schedule, the checkbox is checked.
	 * @param s The schedule concern by the attendance record performed
	 */
	private void initChecks(Schedule s){
		
		//If the schedule has no previous attendance record, then all checkbox must be unchecked
		if(!s.isChecked()){
			for(CheckBoxPerso check : list_checkbox.get(s)){
				check.setChecked(false);
			}
		}
		//If there is one, then the checkboxes are checked according to the database
		else{
			ArrayList<Student> absent_students = classroom.getAllStudentAbsentOfClassroom(s);

			for(CheckBoxPerso check : list_checkbox.get(s)){
				if(absent_students.contains(check.getStudent())){
					check.setChecked(false);
				}else
					check.setChecked(true);
			}
		}
	}
	
	/**	
	 * Determines what to do depending of the request code of the result activity
	 * {@inheritDoc}
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("test","ok");
		if(data != null){
			if (requestCode == MARK_CREATED) {
				Toast.makeText(getSherlockActivity(), R.string.mark_added, Toast.LENGTH_LONG).show();
			}else if (requestCode == CLASS_ADDED){
				//Nothing to do because the classroom is loaded, so it's obvious that the operation worked
			}
		}
	}

	/**	
	 * Used to close all action modes 
	 */
	@Override
	public void onStop(){
		super.onStop();
		if(mMode!=null)
			mMode.finish();
	}

	/**
	 * Retrieve the last or preferred classroom id from the internal file of the application
	 * @return The classroom id
	 */
	private int readClassroomIdFromInternalStorage() {
		BufferedReader input = null;
		try {
			input = new BufferedReader(new InputStreamReader(getSherlockActivity().openFileInput("myfile")));
			String line = input.readLine();
			return Integer.parseInt(line);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return -1;
	}

	/**
	 * Reset the classroom id in the internal file of the application
	 */
	public void resetFileToInternalStorage() {
		String eol = System.getProperty("line.separator");
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(getSherlockActivity().openFileOutput("myfile", Context.MODE_PRIVATE)));
			writer.write(-1 + eol);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	} 

	/**
	 * <b>Dialog used to pick the start and end times for a new schedule</b>
	 */
	public static class TimePickerForSchedule extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

		/**
		 * The edittext which contains the chosen time
		 */
		private EditText edit;

		public TimePickerForSchedule() {
			super();
		}

		public TimePickerForSchedule(EditText edit) {
			super();
			this.edit = edit;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,
					DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			edit.setText(((hourOfDay>9)?hourOfDay:("0"+hourOfDay)) +":"+((minute>9)?minute:("0"+minute)));
		}
	}
	
	/**
	 * <b>Dialog used to create a new time slot</b>
	 */
	private class NewSlotDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflater = getActivity().getLayoutInflater();

			LinearLayout linear = (LinearLayout)inflater.inflate(R.layout.dialog_slot, null);
			final EditText edit_s_h = (EditText) linear.findViewById(R.id.edit_start_hour);
			edit_s_h.setOnTouchListener(showTimePicker);
			final EditText edit_e_h = (EditText) linear.findViewById(R.id.edit_end_hour);
			edit_e_h.setOnTouchListener(showTimePicker);

			builder.setTitle(R.string.new_slot)
			.setView(linear)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					
					String start = edit_s_h.getText().toString();
					String end = edit_e_h.getText().toString();	
					
					Schedule schedule = new Schedule(new Date(), start , end,classroom,false);
					schedule.initSchedule();
					
					initTable();
					
					NewSlotDialogFragment.this.getDialog().dismiss();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					NewSlotDialogFragment.this.getDialog().cancel();
				}
			});   

			Dialog d = builder.create();

			return d;
		}  
	}

	/**
	 * <b>Dialog used to create a new mark for all the students of a classroom</b>
	 */
	private class NewMarkDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflater = getActivity().getLayoutInflater();

			LinearLayout linear = (LinearLayout)inflater.inflate(R.layout.dialog_mark, null);
			final EditText edit_name = (EditText) linear.findViewById(R.id.edit_mark_name);
			final EditText edit_max = (EditText) linear.findViewById(R.id.edit_mark_max);

			builder.setTitle(R.string.create_mark)
			.setView(linear)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					
					Mark mark = new Mark(edit_name.getText().toString(), edit_max.getText().toString(), classroom);
					
					Bundle bundle = new Bundle();
					bundle.putSerializable("class", classroom);
					bundle.putSerializable("mark", mark);
					
					Intent intent = new Intent(getSherlockActivity(),CreateMarkActivity.class);
					intent.putExtras(bundle);
					
					startActivity(intent);
					
					NewMarkDialogFragment.this.getDialog().dismiss();
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					NewMarkDialogFragment.this.getDialog().cancel();
				}
			});   

			Dialog d = builder.create();

			return d;
		}
	}
	
	/**
	 * Create a new textview designed and styled to be a columns head in the table
	 * @return Textview styled as a columns head
	 */
	private TextView createTitleStyled(){
		TextView text = new TextView(getSherlockActivity());
		text.setText(R.string.students_title_form);
		text.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		text.setGravity(Gravity.CENTER_VERTICAL);
		text.setMinHeight(60);
		text.setTextAppearance(getSherlockActivity(), R.style.TextView_Title);
		return text;
	}
	
	/**
	 * Create a schedule column's head 
	 * @param schedule Time slot to display
	 * @return Layout used as column's head
	 */
	private LinearLayout createSlot(Schedule schedule){
		LayoutPerso layout = null;
		if(schedule == null){
			layout = new LayoutPerso(getSherlockActivity());
			layout.setOrientation(LinearLayout.VERTICAL);
			TextView t = new TextView(getSherlockActivity());
			t.setText("00:00");
			t.setEnabled(false);
			layout.addView(t);
			t = new TextView(getSherlockActivity());
			t.setText("00:00");
			t.setEnabled(false);
			layout.addView(t);
			layout.setPadding(3, 0, 3, 5);
		}else{
			layout = new LayoutPerso(getSherlockActivity(),schedule);
			layout.setOrientation(LinearLayout.VERTICAL);
			TextView t = new TextView(getSherlockActivity());
			t.setText(schedule.getBegin());
			layout.addView(t);
			t = new TextView(getSherlockActivity());
			t.setText(schedule.getEnd());
			layout.addView(t);
			layout.setPadding(3, 0, 3, 5);
			layout.setOnLongClickListener(layoutLongClickListener);
		}
		return layout;
	}
	
	/**
	 * Create an horizontal line to separate the table head line
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private View createLine(){
		View v = new View(getSherlockActivity());
		v.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 3));
		v.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_solid_green));
		return v;
	}
	
	/**
	 * Create a table line
	 * @param student The student to display
	 * @param list_creneaux List of time slots
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private TableRow createStudentRow(final Student student, ArrayList<Schedule> list_creneaux){
		TableRow row = new TableRow(getSherlockActivity());
		TextView t = new TextView(getSherlockActivity());
		t.setText(student.toString());
		t.setTextSize(23);
		t.setBackgroundDrawable(getResources().getDrawable(R.drawable.list_selector_holo_light));
		t.setGravity(Gravity.CENTER_VERTICAL);
		t.setPadding(0, 10, 0, 10);
		t.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Bundle bundle = new Bundle();
				bundle.putSerializable("student", student);
				Intent intent = new Intent(getSherlockActivity(),StudentActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});
		list_StudentNames.add(t);
		row.addView(t);

		if(list_creneaux.size() == 0){
			CheckBoxPerso ck = new CheckBoxPerso(getSherlockActivity());
			ck.setEnabled(false);			
			row.addView(ck);
		}
		else
			for(Schedule schedule: list_creneaux){
				CheckBoxPerso ck = new CheckBoxPerso(getSherlockActivity(),student,schedule);
				ck.setOnLongClickListener(checkLongClickListener);
				ck.setOnClickListener(checkClickListener);
				row.addView(ck);
				list_checkbox.get(schedule).add(ck);
			}
		return row;
	}
	
}
