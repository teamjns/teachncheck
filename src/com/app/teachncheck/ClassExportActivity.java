package com.app.teachncheck;


import java.util.ArrayList;

import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.io.ExportCSV;
import com.app.teachncheck.io.WrittePdf;
import com.app.teachncheck.io.WritteXls;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>Activity used to export data in CSV, Xls or PDF</b>
 * <p>The user can :</p>
 * <ul>
 * 		<li>export a classroom with it's students under Xls format</li>
 * 		<li>export all classrooms, students, or both under CSV format</li>
 * 		<li>export a classroom with it's students under Pdf format</li>
 * </ul>
 */
public class ClassExportActivity extends SherlockActivity {
	
	/**
	 * RadioButton for xls format
	 */
	private RadioButton radioXLS;
	/**
	 * RadioButton for csv format
	 */
	private RadioButton radioCSV;
	/**
	 * RadioButton for pdf format
	 */
	private RadioButton radioPDF;
	/**
	 * RadioButton for students export with CSV format
	 */
	private RadioButton radioStudents;
	/**
	 * RadioButton for classrooms export with CSV format
	 */
	private RadioButton radioClassrooms;
	/**
	 * RadioButton for classrooms with their students export with CSV format
	 */
	private RadioButton radioClassroomsAndStudents;
	
	/**
	 * Layout visible only when XLS or PDF format are selected
	 */
	private LinearLayout layoutXlsOrPdf;
	/**
	 * Layout visible only when CSV format is selected
	 */
	private LinearLayout layoutCsv;
	
	/**
	 * Textview containing a short text explaining the use of each format
	 */
	private TextView txtDescription;
	/**
	 * ListView listing all classrooms
	 */
	private ListView listViewClassrooms;
	/**
	 * List of all classrooms
	 */
	private ArrayList<Classroom> classrooms = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_export);
        
        ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		radioXLS = (RadioButton) findViewById(R.id.radio_xls);
		radioXLS.setOnClickListener(onClick_Xls);
		
		radioCSV = (RadioButton) findViewById(R.id.radio_csv);
		radioCSV.setOnClickListener(onClick_Csv);
		
		radioPDF = (RadioButton) findViewById(R.id.radio_pdf);
		radioPDF.setOnClickListener(onClick_Pdf);
		
		layoutCsv = (LinearLayout) findViewById(R.id.layout_csv);
		layoutXlsOrPdf = (LinearLayout) findViewById(R.id.layout_xls_pdf);
		
		txtDescription = (TextView) findViewById(R.id.txt_format_description);
		
		radioStudents = (RadioButton) findViewById(R.id.radio_students);
		radioClassrooms = (RadioButton) findViewById(R.id.radio_classs);
		radioClassroomsAndStudents = (RadioButton) findViewById(R.id.radio_students_classroom);
		
		//Filling classrooms list from database
		listViewClassrooms = (ListView) findViewById(R.id.list_class);
		classrooms = MainActivity.getDb().getAllClassroom();
		ArrayAdapter<Classroom> adapter = new ArrayAdapter<Classroom>(this,android.R.layout.simple_list_item_single_choice, classrooms);
		listViewClassrooms.setAdapter(adapter);
		listViewClassrooms.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
    	inflater.inflate(R.menu.menu_activity_class_import, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case android.R.id.home:
    			CommonMethods.returnHome(this);
    			return true;
    		case R.id.menu_settings:
    			CommonMethods.gotoSettings(this);
    			return true;
    		case R.id.menu_import:
    			processExport();
    			return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }
    
    /**
     * Manage the whole export process
     */
    private void processExport(){
    	
    	//XLS export
    	if(radioXLS.isChecked()){
    		
    		//Retrieve selected classroom in the ListView
        	Classroom classroom = classrooms.get(listViewClassrooms.getCheckedItemPosition());
    		
    		if(classroom != null){
    			WritteXls xls = new WritteXls(getApplicationContext(), classroom.getId());
    			xls.exportXls(Environment.getExternalStorageDirectory().getAbsolutePath()+"/TeachNcheckFiles/");
                Toast.makeText(this, getString(R.string.success_export_xls) + " " + classroom.toString(), Toast.LENGTH_SHORT).show();
    		}else {
    			Toast.makeText(this, R.string.you_have_to_choose_a_class, Toast.LENGTH_SHORT).show();
			}
    	}
    	//CSV export
    	else if(radioCSV.isChecked()){
    		//Export all students
    		if(radioStudents.isChecked()){
    			ExportCSV ec = new ExportCSV(getApplicationContext());
                ec.exportStudents(Environment.getExternalStorageDirectory().getAbsolutePath()+"/TeachNcheckFiles/");
                Toast.makeText(this, R.string.export_successful, Toast.LENGTH_SHORT);
    		}
    		//Export all classrooms
    		else if(radioClassrooms.isChecked()){
    			ExportCSV ec = new ExportCSV(getApplicationContext());
                ec.exportClassrooms(Environment.getExternalStorageDirectory().getAbsolutePath()+"/TeachNcheckFiles/");
                Toast.makeText(this, R.string.export_successful, Toast.LENGTH_SHORT);
    		}
    		//Export classrooms and students
    		else if(radioClassroomsAndStudents.isChecked()){
    			//ExportCSV ec = new ExportCSV(getApplicationContext());
    			//TODO on est cens� exporter toutes les classes pas seulement une seule donc on ne devrait pas avoir besoin de l'id de classroom surtout que la liste de classroom ne s'affiche pas pour le CSV
                //ec.exportClassroomsWithStudents(classroom.getId(), Environment.getExternalStorageDirectory().getAbsolutePath()+"/TeachNcheckFiles/");
    			Toast.makeText(this, R.string.export_successful, Toast.LENGTH_SHORT);
    		}
    		
    	}else if(radioPDF.isChecked()){
    		
    		//Retrieve selected classroom in the ListView
        	Classroom classroom = classrooms.get(listViewClassrooms.getCheckedItemPosition());
    		
    		if(classroom != null){
    			WrittePdf pdf = new WrittePdf(ClassExportActivity.this, classroom.getId());
                pdf.exporterPDF(Environment.getExternalStorageDirectory().getAbsolutePath()+"/TeachNcheckFiles/");
                Toast.makeText(this, getString(R.string.success_export_pdf), Toast.LENGTH_SHORT).show();
    		}else {
    			Toast.makeText(this, R.string.you_have_to_choose_a_class, Toast.LENGTH_SHORT).show();
			}
    	}

    	Toast.makeText(this, R.string.you_have_to_choose, Toast.LENGTH_SHORT).show();
    }
    
    /**
     * Display all XLS relative objects when the XLS RadioButton is selected
     */
    private OnClickListener onClick_Xls = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			manageRadioGroup(v);
			layoutXlsOrPdf.setVisibility(View.VISIBLE);
			layoutCsv.setVisibility(View.GONE);
			txtDescription.setText(R.string.format_xls_desc_export);
			listViewClassrooms.setVisibility(View.VISIBLE);
		}
	};
	
    /**
     * Display all CSV relative objects when the CSV RadioButton is selected
     */
    private OnClickListener onClick_Csv = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			manageRadioGroup(v);
			layoutXlsOrPdf.setVisibility(View.GONE);
			layoutCsv.setVisibility(View.VISIBLE);
			txtDescription.setText(R.string.format_csv_desc_export);
			listViewClassrooms.setVisibility(View.GONE);
		}
	};
	
    /**
     * Display all PDF relative objects when the PDF RadioButton is selected
     */
    private OnClickListener onClick_Pdf = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			manageRadioGroup(v);
			layoutXlsOrPdf.setVisibility(View.VISIBLE);
			layoutCsv.setVisibility(View.GONE);
			txtDescription.setText(R.string.format_pdf_desc_export);
			listViewClassrooms.setVisibility(View.VISIBLE);
		}
	};
	
	/**
	 * Used to make sure that only one RadioButton is checked at the same time
	 * @param v
	 */
	private void manageRadioGroup(View v){
		if(v == radioXLS){
			radioCSV.setChecked(false);
			radioPDF.setChecked(false);
		}
		else if(v==radioCSV){
			radioXLS.setChecked(false);
			radioPDF.setChecked(false);
		}else{
			radioCSV.setChecked(false);
			radioXLS.setChecked(false);
		}
	}
    
}
