package com.app.teachncheck;

import android.os.Bundle;
import android.widget.EditText;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>Activity used to modify details of a classroom</b>
 * 
 */
public class ClassModificationActivity extends SherlockActivity {

	/**
	 * EditText for the classroom's name
	 */
	private EditText edit_name;
	/**
	 * EditText for the classroom's short name
	 */
	private EditText edit_short_name;
	/**
	 * EditText for the classroom's level
	 */
	private EditText edit_level;
	/**
	 * EditText for the classroom's year
	 */
	private EditText edit_year;

	private Classroom classroom;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_class);

		Bundle bundle = this.getIntent().getExtras();
		classroom = (Classroom) bundle.getSerializable("class");

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		edit_name = (EditText) findViewById(R.id.edit_name_class);
		edit_name.setText(classroom.getName());

		edit_short_name = (EditText) findViewById(R.id.edit_short_name_class);
		edit_short_name.setText(classroom.getEntitled());

		edit_level = (EditText) findViewById(R.id.edit_level_class);
		edit_level.setText(classroom.getLevel());

		edit_year = (EditText) findViewById(R.id.edit_year_class);
		edit_year.setText(classroom.getYear());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_check_only, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			CommonMethods.returnHome(this);
			return true;
		case R.id.submit_form:
			validation();
			return true;
		case R.id.menu_settings:
			CommonMethods.gotoSettings(this);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Validates all data and then save the classroom
	 */
	private void validation(){
		if(!edit_name.getText().toString().equals("")
				&& !edit_short_name.getText().toString().equals("")
				&& !edit_year.getText().toString().equals("")){

			if(!(edit_year.getText().toString().length()<4)){

				classroom.setName(edit_name.getText().toString());
				classroom.setEntitled(edit_short_name.getText().toString());
				classroom.setLevel(edit_level.getText().toString());
				classroom.setYear(edit_year.getText().toString());
				classroom.saveClass();

				finish();
			}else{
				edit_year.setError(getString(R.string.four_digit_year) );
			}
		}
		else{
			if(edit_name.getText().toString().equals(""))
				edit_name.setError(getString(R.string.empty_fields) );
			if(edit_short_name.getText().toString().equals(""))
				edit_short_name.setError(getString(R.string.empty_fields) );
			if(edit_year.getText().toString().equals(""))
				edit_year.setError(getString(R.string.empty_fields) );        			
		}
	}
}
