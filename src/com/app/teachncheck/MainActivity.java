package com.app.teachncheck;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.DatabaseHandler;
import com.app.teachncheck.fragments.ClassListFragment;
import com.app.teachncheck.fragments.ManagerFragment;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>MainActivity is the main activity of this application, used as the first page.</b> 
 * <p>This activity contains two actionbar tabs : {@link ManagerFragment} and {@link ClassListFragment}. 
 * The Manager fragment is used to manage the students while the CLassList fragment offers access to the different classes.</p>
 * 
 * @see ManagerFragment
 * @see ClassListFragment
 */
public class MainActivity extends SherlockFragmentActivity {

	/**
	 * The menu inside the actionbar
	 */
	private Menu myMenu;
	
	/**
	 * The actionbar tabs (manager and classes)
	 */
	private ActionBar.Tab tabA,tabB;

	/**
	 * The database handler, initialized only once and used to access the database
	 */
	private static DatabaseHandler db;

	/**
	 * Method used to access the database by all the activities
	 * @return An instance of the database handler
	 */
	public static DatabaseHandler getDb() {
		return MainActivity.db;
	}

	@Override
	/**
	 * Close the session used to access the database.	
	 * {@inheritDoc}
	 */
	protected void onDestroy() {
		super.onDestroy();
		MainActivity.db.close();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Initialization of the database handler
		//TODO peut �tre v�rifier si null avant d'initialiser pour �viter de reconstruire si d�j� fait une fois ?
		MainActivity.db = new DatabaseHandler(getApplicationContext());

		//Creation of the tabs
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		tabA = actionBar.newTab().setText(R.string.tab_manager_title);
		tabB = actionBar.newTab().setText(R.string.tab_list_class_title);
		tabA.setTabListener(new TabListener<ManagerFragment>(this,"title1", ManagerFragment.class));
		tabB.setTabListener(new TabListener<ClassListFragment>(this,"title2", ClassListFragment.class));
		actionBar.addTab(tabA);
		actionBar.addTab(tabB);	
	}


	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			CommonMethods.gotoSettings(this);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * <b>Listener for the actionbar tabs.</b>
	 * <p>Here it is used to change the content of the action bar menu depending on the tab selected.</p>
	 * @param <T> An instance of {@link ManagerFragment} or {@link ClassListFragment}
	 */
	private class TabListener<T extends Fragment> implements ActionBar.TabListener {
		private Fragment mFragment;
		private final SherlockFragmentActivity mActivity;
		private final String mTag;
		private final Class<T> mClass;

		/**
		 * Constructor used each time a new tab is created.
		 * 
		 * @param classActivity
		 *            The host Activity, used to instantiate the fragment
		 * @param tag
		 *            The identifier tag for the fragment
		 * @param clz
		 *            The fragment's Class, used to instantiate the fragment
		 */
		public TabListener(MainActivity mainActivity, String tag, Class<T> clz) {
			mActivity = mainActivity;
			mTag = tag;
			mClass = clz;
		}

		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			try {
				//TODO essayer de r�gler le proibl�me des tabs supperpos�s en copiant la fa�on dont on utilise les tabs dans StudentActivity (add ou show en fonction de si le tab a d�j� �t� cr�� une fois, c'est peut etre la raison du probl�me
				if(myMenu!=null){
					//If the manager tab is selected
					if(tab.getPosition()==0){
						myMenu.clear();
						MenuInflater inflater = MainActivity.this.getSupportMenuInflater();
						inflater.inflate(R.menu.menu_modif, myMenu);
					}
					//If the class list tab is selected
					else{
						myMenu.clear();
						MenuInflater inflater = MainActivity.this.getSupportMenuInflater();
						inflater.inflate(R.menu.menu_activity_students_list, myMenu);
					}
				}

				mFragment = Fragment.instantiate(mActivity, mClass.getName());
				ft.add(android.R.id.content, mFragment, mTag);
				
			}catch (Exception ex) {
				//TODO remove	
				Log.w("tab error", ": Exception occurred: " + ex.toString());
			}

		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			if (mFragment != null) {
				// Detach the fragment, because another one is being attached
				ft.hide(mFragment);
				ft.detach(mFragment);
			}
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			// User selected the already selected tab. Usually do nothing.
		}
	}
}
