package com.app.teachncheck;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Parent;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>StudentAddActivity contains the form allowing to create a new Student.</b>
 * <p>Uses the activity_add_students layout.</p>
 * 
 * @see MainActivity
 * @see SettingsActivity
 */
public class StudentAddActivity extends SherlockActivity {
	
	/**
	 * Menu of this activity
	 */
	private Menu mMenu;
	/**
	 * EditText for student's id number
	 */
	private EditText edit_identifier;
	/**
	 * EditText for student's surname
	 */
	private EditText edit_surname;
	/**
	 * EditText for student's first name
	 */
	private EditText edit_first_name;
	/**
	 * EditText for student's birth date
	 */
	private EditText edit_birthday;
	/**
	 * EditText for student's address
	 */
	private EditText edit_address;
	/**
	 * EditText for student's phone number
	 */
	private EditText edit_phone;
	/**
	 * EditText for student's email address
	 */
	private EditText edit_mail;
	/**
	 * EditText for student's picture path (temp)
	 */
	private EditText edit_picture;
	/**
	 * EditText for first parent's surname
	 */
	private EditText edit_surname_p1;
	/**
	 * EditText for first parent's first name
	 */
	private EditText edit_first_name_p1; 
	/**
	 * EditText for first parent's birth date
	 */
	private EditText edit_birthday_p1;
	/**
	 * EditText for first parent's address
	 */
	private EditText edit_address_p1;
	/**
	 * EditText for first parent's phone number
	 */
	private EditText edit_phone_p1;
	/**
	 * EditText for first parent's email address
	 */
	private EditText edit_mail_p1;
	/**
	 * EditText for second parent's surname
	 */
	private EditText edit_surname_p2;
	/**
	 * EditText for second parent's first name
	 */
	private EditText edit_first_name_p2;
	/**
	 * EditText for second parent's birth date
	 */
	private EditText edit_birthday_p2;
	/**
	 * EditText for second parent's address
	 */
	private EditText edit_address_p2;
	/**
	 * EditText for second parent's phone number
	 */
	private EditText edit_phone_p2;
	/**
	 * EditText for second parent's email address
	 */
	private EditText edit_mail_p2;
	/**
	 * Checkbox for parent 1. Hide/Show first parent's fields.
	 */
	private CheckBox check_parent1;
	/**
	 * Checkbox for parent 2. Hide/Show second parent's fields.
	 */
	private CheckBox check_parent2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_students);
        
        ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		edit_surname = (EditText) findViewById(R.id.edit_surname_student);
		edit_surname.addTextChangedListener(validateOnChangeListener);
		
		edit_first_name = (EditText) findViewById(R.id.edit_first_name_student);
		edit_first_name.addTextChangedListener(validateOnChangeListener);
			
		edit_identifier = (EditText) findViewById(R.id.edit_identity_student);
		edit_birthday = (EditText) findViewById(R.id.edit_birthday_student);
		edit_address = (EditText) findViewById(R.id.edit_address_student);
		edit_phone = (EditText) findViewById(R.id.edit_phone_student);
		edit_mail = (EditText) findViewById(R.id.edit_mail_student);
		edit_picture = (EditText) findViewById(R.id.edit_picture_path_student);
		
		edit_surname_p1 = (EditText) findViewById(R.id.edit_surname_parent);
		edit_first_name_p1 = (EditText) findViewById(R.id.edit_first_name_parent);
		edit_birthday_p1 = (EditText) findViewById(R.id.edit_birthday_parent);
		edit_address_p1 = (EditText) findViewById(R.id.edit_address_parent);
		edit_phone_p1 = (EditText) findViewById(R.id.edit_phone_parent);
		edit_mail_p1 = (EditText) findViewById(R.id.edit_mail_parent);
		
		edit_surname_p2 = (EditText) findViewById(R.id.edit_surname_parent2);
		edit_first_name_p2 = (EditText) findViewById(R.id.edit_first_name_parent2);
		edit_birthday_p2 = (EditText) findViewById(R.id.edit_birthday_parent2);
		edit_address_p2 = (EditText) findViewById(R.id.edit_address_parent2);
		edit_phone_p2 = (EditText) findViewById(R.id.edit_phone_parent2);
		edit_mail_p2 = (EditText) findViewById(R.id.edit_mail_parent2);
		
		final LinearLayout layout_details = (LinearLayout) findViewById(R.id.layout_student_details);
		final LinearLayout layout_parent1 = (LinearLayout) findViewById(R.id.layout_parent1);
		final LinearLayout layout_parent2 = (LinearLayout) findViewById(R.id.layout_parent2);
		
		final CheckBox check_details = (CheckBox) findViewById(R.id.check_student_details);
		check_details.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				if(check_details.isChecked())
					layout_details.setVisibility(View.VISIBLE);
				else
					layout_details.setVisibility(View.GONE);
			}
		});
		final CheckBox check_parent1 = (CheckBox) findViewById(R.id.check_parent1);
		check_parent1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				if(check_parent1.isChecked())
					layout_parent1.setVisibility(View.VISIBLE);
				else
					layout_parent1.setVisibility(View.GONE);
			}
		});
		
		final CheckBox check_parent2 = (CheckBox) findViewById(R.id.check_parent2);
		check_parent2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				if(check_parent2.isChecked())
					layout_parent2.setVisibility(View.VISIBLE);
				else
					layout_parent2.setVisibility(View.GONE);
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
    	inflater.inflate(R.menu.menu_simple, menu);
    	mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CommonMethods.returnHome(this);
                return true;
            case R.id.submit_form:
    			validation();
    			return true;
            case R.id.menu_settings:
    			CommonMethods.gotoSettings(this);
    			return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    /**
     * Called each time something is typed in a field. The submit button is showed only if all the required fields are not empty.
     */
    private TextWatcher validateOnChangeListener = new TextWatcher() {

		public void afterTextChanged(Editable s) {
			if(noEmptyField()){	
				mMenu.clear();
				MenuInflater inflater = getSupportMenuInflater();
				inflater.inflate(R.menu.menu_check_only, mMenu);
			}
		}

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

		public void onTextChanged(CharSequence s, int start, int before, int count) {}

	};
    
    /**
     * Check the format of all fields and create the Student and the Parents (if necessary).
     */
    private void validation(){
    	
    	String identifier = edit_identifier.getText().toString();
    	String surname = edit_surname.getText().toString(); 			//Pr�nom
    	String first_name= edit_first_name.getText().toString();		//Nom de famille
    	String birthday= edit_birthday.getText().toString();
    	String address= edit_address.getText().toString();
    	String phone= edit_phone.getText().toString();
    	String mail= edit_mail.getText().toString();
    	String picture_path= edit_picture.getText().toString();
    	
    	String surname_p1 = edit_surname_p1.getText().toString(); 			//Pr�nom
    	String first_name_p1= edit_first_name_p1.getText().toString();		//Nom de famille
    	String birthday_p1= edit_birthday_p1.getText().toString();
    	String address_p1= edit_address_p1.getText().toString();
    	String phone_p1= edit_phone_p1.getText().toString();
    	String mail_p1 = edit_mail_p1.getText().toString();

    	String surname_p2 = edit_surname_p2.getText().toString(); 			//Pr�nom
    	String first_name_p2= edit_first_name_p2.getText().toString();		//Nom de famille
    	String birthday_p2= edit_birthday_p2.getText().toString();
    	String address_p2= edit_address_p2.getText().toString();
    	String phone_p2= edit_phone_p2.getText().toString();
    	String mail_p2 = edit_mail_p2.getText().toString();
    	
    	boolean validate = true;
    	
		if(surname.equals("")){
			validate = false;
			edit_surname.setError(getString(R.string.names_fields));
		}
		
		if(first_name.equals("")){
			validate = false;
			edit_first_name.setError(getString(R.string.names_fields));
		}
		
		if(validate && !(mail.equals("")) && !mail.matches("^([\\w]+)@([\\w]+)\\.([\\w]+)$")){
			validate = false;
			edit_mail.setError(getString(R.string.format_mail));
		}

		if(validate && check_parent1.isChecked() && ((surname_p1.equals("")) || (first_name_p1.equals("")))){
			validate = false;
			if(surname_p1.equals(""))
				edit_surname_p1.setError(getString(R.string.parent_surname_and_first_name));
			if(first_name_p1.equals(""))
				edit_first_name_p1.setError(getString(R.string.parent_surname_and_first_name));
		}
		
		if(validate && check_parent2.isChecked() && ((surname_p2.equals("")) || (first_name_p2.equals("")))){
			validate = false;
			if(surname_p2.equals(""))
				edit_surname_p2.setError(getString(R.string.parent_surname_and_first_name));
			if(first_name_p2.equals(""))
				edit_first_name_p2.setError(getString(R.string.parent_surname_and_first_name));
		}
		
		if(validate){
			Student student = new Student(identifier
					,first_name
					,surname
					,birthday
					,address
					,phone
					,mail
					,picture_path);

			student.initStudent();
			
			if(check_parent1.isChecked()){
				Parent parent = new Parent(first_name_p1, surname_p1, birthday_p1, address_p1, phone_p1, mail_p1);
				parent.initParent();
				parent.addStudentToParent(student);
			}
			
			if(check_parent2.isChecked()){
				Parent parent = new Parent(first_name_p2, surname_p2, birthday_p2, address_p2, phone_p2, mail_p2);
				parent.initParent();
				parent.addStudentToParent(student);
			}

			finish();
		}
	}
    
    /**
     * Verify that student's surname and student's first name are not empty.
     * @return True if the fields are not empty, False otherwise.
     */
    public boolean noEmptyField(){
		if(!edit_surname.getText().toString().equals("")
				&& !edit_first_name.getText().toString().equals(""))
			return true;
		return false;
	}
    
}
