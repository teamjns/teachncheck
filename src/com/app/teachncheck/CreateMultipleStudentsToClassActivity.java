package com.app.teachncheck;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>CreateMultipleStudentsToClassActivity allows to create multiple students by giving only their names.</b>
 * <p>Uses the activity_create_multiple_students_to_class layout.</p>
 * 
 * @see MainActivity
 * @see SettingsActivity
 */
public class CreateMultipleStudentsToClassActivity extends SherlockActivity {

	/**
	 * List of {@link Couple} with the surname and first name of each new Student
	 */
	private ArrayList<Couple> listSurnamesAndFirstNames;
	/**
	 * Layout containing all EditText
	 */
	private LinearLayout fields;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_multiple_students_to_class);
	
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		fields = (LinearLayout) findViewById(R.id.layout_edit);
		
		//Button to add another line of EditText, allowing to create a new Student
		ImageButton bt_plus = (ImageButton) findViewById(R.id.bt_add_student);
		bt_plus.setOnClickListener(onAddFieldClick);
		
		//TextView to add another line of EditText, allowing to create a new Student
		TextView txt_plus = (TextView) findViewById(R.id.txt_add_student);
		txt_plus.setOnClickListener(onAddFieldClick);
		
		listSurnamesAndFirstNames = new ArrayList<Couple>();
		
		//Add a first couple of surname and first name EditTexts
		addLayout();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.menu_check_only, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			CommonMethods.returnHome(this);
			return true;
		case R.id.submit_form:
			return_students();
			return true;
		case R.id.menu_settings:
			CommonMethods.gotoSettings(this);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	/**
	 * Add 2 EditText to allow the user to give the surname and first name of a new Student
	 */
	private void addLayout(){
		LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.fields,null);
		EditText txt_surname = (EditText) layout.findViewById(R.id.edit_surname);
		EditText txt_first_name = (EditText) layout.findViewById(R.id.edit_first_name);
		fields.addView(layout);
		listSurnamesAndFirstNames.add(new Couple(txt_surname, txt_first_name));
	}
	
	/**
	 * <b>Couple of 2 EditTexts, one for the surname of a new student, and the other one for his first name.</b>
	 */
	private class Couple{
		
		/**
		 * EditText to give the student's surname
		 */
		private EditText surname;
		/**
		 * EditText to give the student's first name
		 */
		private EditText first_name;

		public Couple(EditText surname, EditText first_name) {
			super();
			this.surname = surname;
			this.first_name = first_name;
		}		
	}
	
	/**
	 * Called when the button or the TextField to add new student's fields. Displays 2 new EditText.
	 */
	private OnClickListener onAddFieldClick = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			addLayout();				
		}
	};
	
	/**
	 * Verify the correct format of all fields, then create the new Students, and close the activity.
	 */
	private void return_students(){
		
		ArrayList<Student> list_Students = new ArrayList<Student>();
		int index = 0;
		boolean valid = true;
		while(index<listSurnamesAndFirstNames.size() && !listSurnamesAndFirstNames.get(index).surname.getText().toString().equals("")){
			Couple c = listSurnamesAndFirstNames.get(index);
			Log.w("tagtag", c.surname.getText().toString());
			if(c.first_name.getText().toString().equals("")){
				c.first_name.setError(getString(R.string.names_fields));
				valid = false;
			}else if(valid){
				list_Students.add(new Student(c.surname.getText().toString(),c.first_name.getText().toString()));
			}
			index++;
		}
		
		if(valid){
			
			//Initializes all new students
			for(Student student: list_Students)
				student.initStudent();
			
			Intent result = new Intent();
			result.putExtra("students", list_Students);
			setResult(RESULT_OK, result);
			finish();
		}
	}

}
