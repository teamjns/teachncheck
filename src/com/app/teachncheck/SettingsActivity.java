package com.app.teachncheck;

import java.util.ArrayList;

import android.os.Bundle;
import android.preference.ListPreference;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.utils.CommonMethods;

/**
 * Activity containings controls of all application's settings
 */
public class SettingsActivity extends SherlockPreferenceActivity {

    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		addPreferencesFromResource(R.xml.preferences);
		loadListClassrooms();
    }

    private void loadListClassrooms(){
    	
    	ArrayList<Classroom> data = MainActivity.getDb().getAllClassroom();
    	int numberOfClassroom = data.size();
    	
    	CharSequence[] entries = new CharSequence[numberOfClassroom];
    	CharSequence[] entryValues = new CharSequence[numberOfClassroom];
    	int i = 0;
    	for (Classroom classroom : data) {
			entries[i] = classroom.getName();
			entryValues[i] = String.valueOf(classroom.getId());
			i++;
		}
    	
    	@SuppressWarnings("deprecation")
		ListPreference lp = (ListPreference)findPreference("starredclass");
    	lp.setEntries(entries);
    	lp.setEntryValues(entryValues);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
    	inflater.inflate(R.menu.menu_activity_settings, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case android.R.id.home:
    			CommonMethods.returnHome(this);
    			return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }
}

