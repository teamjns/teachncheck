package com.app.teachncheck;


import java.io.File;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.classes.SendMail;
import com.app.teachncheck.explorer.utils.FileUtils;
import com.app.teachncheck.io.ImportCSV;
import com.app.teachncheck.io.ReadXls;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>Activity used to import data from CSV or XLS files</b>
 * <p>The user can :</p>
 * <ul>
 * 		<li>import a classroom and its students from an Excel (xls) file</li>
 * 		<li>send an xls model and an example file by email</li>
 * 		<li>import students and classes from a CSV file</li>
 * </ul>
 */
public class ClassImportActivity extends SherlockActivity {
	
	/**
	 * Request code used for activity result
	 */
	private static final int REQUEST_CODE = 6384; 
	
	/**
	 * RadioButton for XLS imports
	 */
	private RadioButton radioXLS;
	/**
	 * RadioButton for CSV imports
	 */
	private RadioButton radioCSV;
	
	/**
	 * RadioButton for students import from CSV
	 */
	private RadioButton radioStudents;
	/**
	 * RadioButton for classrooms from CSV
	 */
	private RadioButton radioClassrooms;
	/**
	 * RadioButton for classrooms and students from CSV
	 */
	@SuppressWarnings("unused")
	private RadioButton radioClassroomsStudents;

	/**
	 * Layout for XLS imports
	 */
	private LinearLayout layoutXLS;
	/**
	 * Layout for CSV imports
	 */
	private LinearLayout layoutCSV;
	
	/**
	 * Short text describing each kind of imports
	 */
	private TextView txtDescription;
	/**
	 * EditText containing the file to import
	 */
	private EditText editSourceFile;
	/**
	 * File from which the data will be imported
	 */
	private File file;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_import);
        
        ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		radioXLS = (RadioButton) findViewById(R.id.radio_xls);
		radioXLS.setOnClickListener(onClick_Xls);
		
		radioCSV = (RadioButton) findViewById(R.id.radio_csv);
		radioCSV.setOnClickListener(onClick_Csv);
		
		radioStudents = (RadioButton) findViewById(R.id.radio_students);
		radioClassrooms = (RadioButton) findViewById(R.id.radio_empty_class);
		radioClassroomsStudents = (RadioButton) findViewById(R.id.radio_students_classroom);
		
		layoutXLS = (LinearLayout) findViewById(R.id.layout_xls);
		layoutCSV = (LinearLayout) findViewById(R.id.layout_csv);
		txtDescription = (TextView) findViewById(R.id.txt_format_description);
		
		//Button used to send an XLS model and an example of XLS file by email
		ImageButton btSendMail = (ImageButton) findViewById(R.id.bt_send_mail);
		btSendMail.setOnClickListener(onClick_SendMail);
		
		//Button used to choose the file containing data to export
		ImageButton btChooseFile = (ImageButton) findViewById(R.id.bt_choose_file);
		btChooseFile.setOnClickListener(onClick_FileChooser);
		editSourceFile = (EditText) findViewById(R.id.edit_file);
		editSourceFile.setOnTouchListener(showFileChooser);
    }
  
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
    	inflater.inflate(R.menu.menu_activity_class_import, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case android.R.id.home:
    			CommonMethods.returnHome(this);
    			return true;
    		case R.id.menu_settings:
    			CommonMethods.gotoSettings(this);
    			return true;
    		case R.id.menu_import:
    			processImport();
    			return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }
    
    /**
     * Perform the whole import process
     */
    private void processImport(){
    	
    	if(!editSourceFile.getText().toString().isEmpty()){
	    	if(radioXLS.isChecked()){
	    		ReadXls read = new ReadXls(ClassImportActivity.this, "egemjgrg");
				read.setInputFile(file.getAbsolutePath());
				read.exportXls();
	    	}else{
	    		if(radioStudents.isChecked()){
	    			ImportCSV ic = new ImportCSV(getApplicationContext());
	        		ic.importStudents(file.getAbsolutePath());	
	    		}else if(radioClassrooms.isChecked()){
	    			ImportCSV ic = new ImportCSV(getApplicationContext());
	        		ic.importClassrooms(file.getAbsolutePath());
	    		}else{
	    			ImportCSV ic = new ImportCSV(getApplicationContext());
	        		ic.importClassroomsWithStudents(file.getAbsolutePath());
	    		}
	    		Toast.makeText(this, R.string.import_successful, Toast.LENGTH_SHORT).show();
	    	}
    	}else
    		Toast.makeText(this, R.string.you_have_to_choose, Toast.LENGTH_SHORT).show();
    }
    
    /**
     * Listener displaying the file chooser when the EditText is clicked
     */
    private OnTouchListener showFileChooser = new OnTouchListener() {
		public boolean onTouch (View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN){
				showChooser();
			}
			return true;
		}
	};
    
    /**
     * Listener displaying the file chooser when btChooseFile is clicked
     */
    private OnClickListener onClick_FileChooser = new OnClickListener() {
		@Override
		public void onClick(View v) {
			showChooser();
		}
	};
    
    /**
     * Send an email with an XLS model and example when btSendMail is clicked
     */
    private OnClickListener onClick_SendMail = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			SendMail mail = new SendMail(ClassImportActivity.this);
            mail.sendAutoMailXls();		
		}
	};
    
    /**
     * Show all Xls related objects when XLS RadioButton is clicked
     */
    private OnClickListener onClick_Xls = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			manageRadioGroup(v);
			layoutXLS.setVisibility(View.VISIBLE);
			layoutCSV.setVisibility(View.GONE);
			txtDescription.setText(R.string.format_xls_desc);
		}
	};
	
    /**
     * Show all CSV related objects when CSV RadioButton is clicked
     */
    private OnClickListener onClick_Csv = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			manageRadioGroup(v);
			layoutXLS.setVisibility(View.GONE);
			layoutCSV.setVisibility(View.VISIBLE);
			txtDescription.setText(R.string.format_csv_desc);
		}
	};
    
	/**
	 * @param v
	 */
	private void manageRadioGroup(View v){
		if(v == radioXLS)
			radioCSV.setChecked(false);
		else
			radioXLS.setChecked(false);	
	}
	
    /**
     * Show the file chooser
     */
    private void showChooser() {
		// Use the GET_CONTENT intent from the utility class
		Intent target = FileUtils.createGetContentIntent();
		// Create the chooser Intent
		Intent intent = Intent.createChooser(
				target, getString(R.string.chooser_title));
		try {
			startActivityForResult(intent, REQUEST_CODE);
		} catch (ActivityNotFoundException e) {
			// The reason for the existence of aFileChooser
		}				
	}
    
    /**
     * Used to retrieve the resulting file from the file chooser
     */
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_CODE:	
			// If the file selection was successful
			if (resultCode == RESULT_OK) {		
				if (data != null) {
					// Get the URI of the selected file
					final Uri uri = data.getData();

					try {
						// Create a file instance from the URI
						file = FileUtils.getFile(uri);
						editSourceFile.setText(file.getName());
					} catch (Exception e) {
						Log.e("FileTestActivity", "File select error", e);
					}
				}
			} 
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
    }
}

