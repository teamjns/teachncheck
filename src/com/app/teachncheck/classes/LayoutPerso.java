package com.app.teachncheck.classes;

import com.app.teachncheck.bdd.Schedule;
import com.app.teachncheck.bdd.Student;

import android.app.Activity;
import android.content.Context;
import android.widget.CheckBox;
import android.widget.LinearLayout;

public class LayoutPerso extends LinearLayout {

	private Schedule schedule;
	
	public LayoutPerso(Context context){
		super(context);
		schedule = null;
	}
	
	public LayoutPerso(Context context, Schedule sc){
		super(context);
		schedule = sc;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	
}
