package com.app.teachncheck.classes;

/**
 * Class used to send e-mails
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.app.teachncheck.R;

public class SendMail{
	Activity activite;
	
	
	public SendMail(Activity activite){
		this.activite = activite;
	}
	
	/**
	 * Send a mail to the main e-mail adress of the user
	 */
	@SuppressWarnings("static-access")
	public void sendAutoMailXls(){
		String possibleEmail = null;
		Intent email = new Intent(Intent.ACTION_SEND_MULTIPLE);
		email.setType("plain/text");
		AccountManager accountManager = AccountManager.get(activite); 
	    Account account = getAccount(accountManager);
	    
	    if (account == null) {
	    	possibleEmail = "mail@exemple.com";
	    } else {
	    	possibleEmail = account.name;
	    }
	    try {
	    	AssetManager am=activite.getAssets();
	    	 InputStream inputstream = am.open(activite.getString(R.string.Xls_bones) +".xls");	     
	    	OutputStream out = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/TeachNcheckFiles", activite.getString(R.string.Xls_bones) + ".xls"));     
	    	int read = 0;
	    	byte[] bytes = new byte[1024];
	     
	    	while ((read = inputstream.read(bytes)) != -1) {
	    		out.write(bytes, 0, read);
	    	}	     
	    	inputstream.close();
	    	out.flush();
	    	out.close();
	    	
	    	inputstream = am.open(activite.getString(R.string.xls_example) + ".xls");
		     
	    	// write the inputStream to a FileOutputStream
	    	out = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/TeachNcheckFiles", activite.getString(R.string.xls_example) + ".xls"));
	     
	    	read = 0;
	    	bytes = new byte[1024];
	     
	    	while ((read = inputstream.read(bytes)) != -1) {
	    		out.write(bytes, 0, read);
	    	}	     
	    	inputstream.close();
	    	out.flush();
	    	out.close();	     
	        } catch (IOException e) {
	    	System.out.println(e.getMessage());
	        }
	    String pathSquelette ="file://" + Environment.getExternalStorageDirectory().getAbsolutePath()+"/TeachNcheckFiles/" + activite.getString(R.string.Xls_bones) + ".xls";
	    String pathExemple ="file://" + Environment.getExternalStorageDirectory().getAbsolutePath()+"/TeachNcheckFiles/" + activite.getString(R.string.xls_example) + ".xls";
	    
	    Uri uriSquelette = Uri.parse(pathSquelette);
	    Uri uriExemple = Uri.parse(pathExemple);
	    ArrayList<Uri> uris = new ArrayList<Uri>();
	    uris.add(uriSquelette);
	    uris.add(uriExemple);
	    
		  email.putExtra(Intent.EXTRA_EMAIL, new String[]{possibleEmail});
		  email.putExtra(Intent.EXTRA_SUBJECT, activite.getString(R.string.subject_mail));
		  email.putExtra(Intent.EXTRA_TEXT, activite.getString(R.string.text_mail));
		  email.putExtra(Intent.EXTRA_STREAM, uris);
		  email.setType("message/rfc822");

		  activite.startActivity(Intent.createChooser(email, "Choisissez votre client Mail :"));		 
	}
	/**
	 * Use to get the main adress e-mail of the user
	 * @param accountManager
	 * @return
	 */
	private static Account getAccount(AccountManager accountManager) {
	    Account[] accounts = accountManager.getAccountsByType("com.google");
	    Account account;
	    if (accounts.length > 0) {
	      account = accounts[0];      
	    } else {
	      account = null;
	    }
	    return account;
	  }
}
