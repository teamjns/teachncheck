package com.app.teachncheck.classes;

import com.app.teachncheck.MainActivity;

import android.content.Context;
import android.preference.PreferenceManager;

public class AppPreferences {
	
	private static Context context = MainActivity.getDb().context;
	
	public static void setNimp(String nimp){
		PreferenceManager.getDefaultSharedPreferences(context).edit().putString("nimp", nimp).commit();
	}
	
	public static String getNimp(){
		return PreferenceManager.getDefaultSharedPreferences(context).getString("nimp", "");
	}
	
	public static int getStarredClass(){
		return Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString("starredclass", ""));
	}
	
	public static void setStarredClass(int id){
		PreferenceManager.getDefaultSharedPreferences(context).edit().putString("starredclass", String.valueOf(id)).commit();
	}
	
	public static String getUserEmail(){
		return PreferenceManager.getDefaultSharedPreferences(context).getString("email", "");
	}
	
	public static boolean getMarkNumericDisplay(){
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("markdisplay", false);
	}
	
	public static String getMarkMaxNumeric(){
		return PreferenceManager.getDefaultSharedPreferences(context).getString("maxmark", "");
	}
	
	public static String getRingtone(){
		return PreferenceManager.getDefaultSharedPreferences(context).getString("ringtone", "");
	}
	
	public static boolean getSongActivated(){
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("songs", false);
	}

}
