package com.app.teachncheck.classes;

import com.app.teachncheck.bdd.Schedule;
import com.app.teachncheck.bdd.Student;

import android.app.Activity;
import android.content.Context;
import android.widget.CheckBox;

public class CheckBoxPerso extends CheckBox {

	private Student student;
	private Schedule schedule;
	
	public CheckBoxPerso(Context context){
		super(context);
		student = null;
		schedule = null;
	}
	
	public CheckBoxPerso(Context context, Student st, Schedule sc){
		super(context);
		student = st;
		schedule = sc;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	
}
