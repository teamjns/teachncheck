package com.app.teachncheck;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Parent;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>This class display a form used to create a new Student and to add him to a classroom.</b>
 * <p>The user have to give at least the name and surname of the new Student.</p>
 * <p>Uses the activity_add_students layout.</p>
 *
 */
public class StudentCreateToClassActivity extends SherlockActivity {
	
	
	/**
	 * Just the menu
	 */
	private Menu mMenu;
	
	/**
	 * The edittext allowing to give the student's surname
	 */
	private EditText edit_surname;
	/**
	 * The edittext allowing to give the student's first name
	 */
	private EditText edit_first_name;
	/**
	 * The edittext allowing to give the student's identity number
	 */
	private EditText edit_identifier;
	/**
	 * The edittext allowing to give the student's birth date
	 */
	private EditText edit_birthday;
	/**
	 * The edittext allowing to give the student's address
	 */
	private EditText edit_address;
	/**
	 * The edittext allowing to give the student's phone number
	 */
	private EditText edit_phone;
	/**
	 * The edittext allowing to give the student's email address
	 */
	private EditText edit_mail;
	/**
	 * The edittext allowing to give the student's picture path (temp)
	 */
	private EditText edit_picture_path;
	
	/**
	 * The edittext allowing to give the first parent's surname
	 */
	private EditText edit_surname_p1;
	/**
	 * The edittext allowing to give the first parent's first name
	 */
	private EditText edit_first_name_p1;
	/**
	 * The edittext allowing to give the first parent's birth date
	 */
	private EditText edit_birthday_p1;
	/**
	 * The edittext allowing to give the first parent's addresss
	 */
	private EditText edit_address_p1;
	/**
	 * The edittext allowing to give the first parent's phone number
	 */
	private EditText edit_phone_p1;
	/**
	 * The edittext allowing to give the first parent's email address
	 */
	private EditText edit_mail_p1;
	
	/**
	 * The edittext allowing to give the second parent's surname
	 */
	private EditText edit_surname_p2;
	/**
	 * The edittext allowing to give the second parent's first name
	 */
	private EditText edit_first_name_p2;
	/**
	 * The edittext allowing to give the second parent's birth date
	 */
	private EditText edit_birthday_p2;
	/**
	 * The edittext allowing to give the second parent's address
	 */
	private EditText edit_address_p2;
	/**
	 * The edittext allowing to give the second parent's phone number
	 */
	private EditText edit_phone_p2;
	/**
	 * The edittext allowing to give the second parent's email address
	 */
	private EditText edit_mail_p2;
	
	/**
	 * Checkbox used to display/hide details fields
	 */
	private CheckBox check_details;
	/**
	 * Checkbox used to display/hide parent1's fields 
	 */
	private CheckBox check_parent1;
	/**
	 * Checkbox used to display/hide parent2's fields
	 */
	private CheckBox check_parent2;

	/**
	 * Layout containing details fields
	 */
	private LinearLayout layout_details;
	/**
	 * Layout containing parent1's fields
	 */
	private LinearLayout layout_parent1;
	/**
	 * Layout containing parent2's fields
	 */
	private LinearLayout layout_parent2;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_students);
        
        ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		//Minimum required fields
		edit_surname = (EditText) findViewById(R.id.edit_surname_student);
		edit_surname.addTextChangedListener(validateOnChangeListener);
		edit_first_name = (EditText) findViewById(R.id.edit_first_name_student);
		edit_first_name.addTextChangedListener(validateOnChangeListener);
			
		//Details fields
		edit_identifier = (EditText) findViewById(R.id.edit_identity_student);
		edit_birthday = (EditText) findViewById(R.id.edit_birthday_student);
		edit_address = (EditText) findViewById(R.id.edit_address_student);
		edit_phone = (EditText) findViewById(R.id.edit_phone_student);
		edit_mail = (EditText) findViewById(R.id.edit_mail_student);
		edit_picture_path = (EditText) findViewById(R.id.edit_picture_path_student);
		
		//Parent1's fields
		edit_surname_p1 = (EditText) findViewById(R.id.edit_surname_parent);
		edit_first_name_p1 = (EditText) findViewById(R.id.edit_first_name_parent);
		edit_birthday_p1 = (EditText) findViewById(R.id.edit_birthday_parent);
		edit_address_p1 = (EditText) findViewById(R.id.edit_address_parent);
		edit_phone_p1 = (EditText) findViewById(R.id.edit_phone_parent);
		edit_mail_p1 = (EditText) findViewById(R.id.edit_mail_parent);
		
		//Parent2's fields
		edit_surname_p2 = (EditText) findViewById(R.id.edit_surname_parent2);
		edit_first_name_p2 = (EditText) findViewById(R.id.edit_first_name_parent2);
		edit_birthday_p2 = (EditText) findViewById(R.id.edit_birthday_parent2);
		edit_address_p2 = (EditText) findViewById(R.id.edit_address_parent2);
		edit_phone_p2 = (EditText) findViewById(R.id.edit_phone_parent2);
		edit_mail_p2 = (EditText) findViewById(R.id.edit_mail_parent2);
		
		layout_details = (LinearLayout) findViewById(R.id.layout_student_details);
		layout_parent1 = (LinearLayout) findViewById(R.id.layout_parent1);
		layout_parent2 = (LinearLayout) findViewById(R.id.layout_parent2);
		
		check_details = (CheckBox) findViewById(R.id.check_student_details);
		check_details.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				if(check_details.isChecked())
					layout_details.setVisibility(View.VISIBLE);
				else
					layout_details.setVisibility(View.GONE);
			}
		});
		
		check_parent1 = (CheckBox) findViewById(R.id.check_parent1);
		check_parent1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				if(check_parent1.isChecked())
					layout_parent1.setVisibility(View.VISIBLE);
				else
					layout_parent1.setVisibility(View.GONE);
			}
		});
		
		check_parent2 = (CheckBox) findViewById(R.id.check_parent2);
		check_parent2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				if(check_parent2.isChecked())
					layout_parent2.setVisibility(View.VISIBLE);
				else
					layout_parent2.setVisibility(View.GONE);
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
    	inflater.inflate(R.menu.menu_simple, menu);
    	mMenu = menu;
        return true;
    }
  
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CommonMethods.returnHome(this);
                return true;
            case R.id.submit_form:
    			validation();
    			return true;
            case R.id.menu_settings:
    			CommonMethods.gotoSettings(this);
    			return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    /**
     * Display a menu with a validate icon when all required fields contains at least 1 character
     */
    private TextWatcher validateOnChangeListener = new TextWatcher() {

		public void afterTextChanged(Editable s) {

			if(noEmptyField()){
				mMenu.clear();
				MenuInflater inflater = getSupportMenuInflater();
				inflater.inflate(R.menu.menu_check_only, mMenu);
			}
			else{
				mMenu.clear();
				MenuInflater inflater = getSupportMenuInflater();
				inflater.inflate(R.menu.menu_simple, mMenu);
			}
		}

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

		public void onTextChanged(CharSequence s, int start, int before, int count) {}

	};
    
	/**
	 * Validates all data, then create a new students using those and return this student to the calling activity
	 */
	private void validation(){
    	
		//Student's data
    	String identifier = edit_identifier.getText().toString();
    	String surname = edit_surname.getText().toString(); 			
    	String first_name= edit_first_name.getText().toString();		
    	String birthday= edit_birthday.getText().toString();
    	String address= edit_address.getText().toString();
    	String phone= edit_phone.getText().toString();
    	String mail= edit_mail.getText().toString();
    	String picture_path= edit_picture_path.getText().toString();
    	
    	//First parent's data
    	String surname_p1 = edit_surname_p1.getText().toString(); 			
    	String first_name_p1= edit_first_name_p1.getText().toString();		
    	String birthday_p1= edit_birthday_p1.getText().toString();
    	String address_p1= edit_address_p1.getText().toString();
    	String phone_p1= edit_phone_p1.getText().toString();
    	String mail_p1 = edit_mail_p1.getText().toString();

    	//Second parent's data
    	String surname_p2 = edit_surname_p2.getText().toString(); 			
    	String first_name_p2= edit_first_name_p2.getText().toString();		
    	String birthday_p2= edit_birthday_p2.getText().toString();
    	String address_p2= edit_address_p2.getText().toString();
    	String phone_p2= edit_phone_p2.getText().toString();
    	String mail_p2 = edit_mail_p2.getText().toString();
    	
    	//True if all validation check are successful
    	boolean validate = true;
    	
    	//Check that surname and name are not empty
		if(surname.equals("")
			|| first_name.equals("")){
			validate = false;
			edit_surname.setError(getString(R.string.names_fields));
		}
		
		//Check the email format
		if(validate && !(mail.equals("")) && !mail.matches("^([\\w]+)@([\\w]+)\\.([\\w]+)$")){
			validate = false;
			edit_mail.setError(getString(R.string.format_mail));
		}

		//Check that the first parent's names are not empty
		if(validate && check_parent1.isChecked() && ((surname_p1.equals("")) || (first_name_p1.equals("")))){
			validate = false;
			if(surname_p1.equals(""))
				edit_surname_p1.setError(getString(R.string.parent_surname_and_first_name));
			else
				edit_first_name_p1.setError(getString(R.string.parent_surname_and_first_name));
		}
		
		//Check that the second parent's names are not empty
		if(validate && check_parent2.isChecked() && ((surname_p2.equals("")) || (first_name_p2.equals("")))){
			validate = false;
			if(surname_p1.equals(""))
				edit_surname_p2.setError(getString(R.string.parent_surname_and_first_name));
			else
				edit_first_name_p2.setError(getString(R.string.parent_surname_and_first_name));
		}
		
		//If all validation process are successfull, the student and the parents are created and the activity is terminated
		if(validate){
			Student student = new Student(identifier
					,first_name
					,surname
					,birthday
					,address
					,phone
					,mail
					,picture_path);

			student.initStudent();
			
			if(check_parent1.isChecked()){
				Parent parent = new Parent(first_name_p1, surname_p1, birthday_p1, address_p1, phone_p1, mail_p1);
				parent.initParent();
				parent.addStudentToParent(student);
			}
			
			if(check_parent2.isChecked()){
				Parent parent = new Parent(first_name_p2, surname_p2, birthday_p2, address_p2, phone_p2, mail_p2);
				parent.initParent();
				parent.addStudentToParent(student);
			}
			
			Intent intent = new Intent();
			intent.putExtra("student", student);
			setResult(RESULT_OK, intent);
			finish();
		}
	}
    
    /**
     * Check that student's names fields are not empty
     * @return 
     * 	<ul><li>True : names are not empty</li><li>False : at least one name is empty</li></ul>
     */
    public boolean noEmptyField(){
		if(!edit_surname.getText().toString().equals("")
				&& !edit_first_name.getText().toString().equals(""))
			return true;
		return false;
	}
}
