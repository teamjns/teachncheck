package com.app.teachncheck;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.DatabaseBackup;
import com.app.teachncheck.utils.CommonMethods;


public class BackupActivity extends SherlockActivity {

	/**
	 * RadioButton for Backup
	 */
	private RadioButton radioBackup;
	/**
	 * RadioButton for Retrieving bdd backup
	 */
	private RadioButton radioRetrieve;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backup);
        
        ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		radioBackup = (RadioButton) findViewById(R.id.radio_backup);
		radioBackup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				radioRetrieve.setChecked(false);
				
			}
		});
		
		radioRetrieve = (RadioButton) findViewById(R.id.radio_retrieve);
		radioRetrieve.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				radioBackup.setChecked(false);
				
			}
		});
		
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
    	inflater.inflate(R.menu.menu_check_only, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case android.R.id.home:
    			CommonMethods.returnHome(this);
    			return true;
    		case R.id.menu_settings:
    			CommonMethods.gotoSettings(this);
    			return true;
    		case R.id.submit_form:
    			performBackup();
    			return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }

	private void performBackup() {
		DatabaseBackup dbu = new DatabaseBackup(getApplicationContext());
		
		if(radioBackup.isChecked())
			dbu.backupDatabase();
		else
			dbu.restoreDatabase();
	}
}
