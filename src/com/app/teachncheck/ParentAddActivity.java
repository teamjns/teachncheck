package com.app.teachncheck;

import android.os.Bundle;
import android.widget.EditText;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Parent;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.fragments.StudentDetailsFragment;
import com.app.teachncheck.utils.CommonMethods;


/**
 * <b>ParentAddActivity display a form to create a new Parent</b>
 * <p>When the parent is created, this activity is finished and the application return to the previous instance {@link StudentDetailsFragment}.</p>
 * <p>Uses the activity_parent_add layout.</p>
 * 
 * @see MainActivity
 * @see SettingsActivity
 * @see StudentDetailsFragment
 */
public class ParentAddActivity extends SherlockActivity {

	/**
	 * Edittext for the parent's surname
	 */
	private EditText edit_surname_p1;
	/**
	 * Edittext for the parent's first name
	 */
	private EditText edit_first_name_p1;
	/**
	 * Edittext for the parent's birth date
	 */
	private EditText edit_birthday_p1;
	/**
	 * Edittext for the parent's address
	 */
	private EditText edit_address_p1;
	/**
	 * Edittext for the parent's phone number
	 */
	private EditText edit_phone_p1;
	/**
	 * Edittext for the parent's email address
	 */
	private EditText edit_mail_p1;
	/**
	 * The student which is added a parent
	 */
	private Student student;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_parent_add);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		//Retrieve the student
		Bundle bundle = getIntent().getExtras();
		student = (Student) bundle.getSerializable("student");

		edit_surname_p1 = (EditText) findViewById(R.id.edit_surname_parent);
		edit_first_name_p1 = (EditText) findViewById(R.id.edit_first_name_parent);
		edit_birthday_p1 = (EditText) findViewById(R.id.edit_birthday_parent);
		edit_address_p1 = (EditText) findViewById(R.id.edit_address_parent);
		edit_phone_p1 = (EditText) findViewById(R.id.edit_phone_parent);
		edit_mail_p1 = (EditText) findViewById(R.id.edit_mail_parent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_check_only, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			CommonMethods.returnHome(this);
			return true;
		case R.id.submit_form:
			validation();
			return true;
		case R.id.menu_settings:
			CommonMethods.gotoSettings(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Validate the new parent's data, then create it.
	 */
	private void validation(){

		String surname_p1 = edit_surname_p1.getText().toString(); 			//Nom de famille
		String first_name_p1= edit_first_name_p1.getText().toString();		//Pr�nom
		String birthday_p1= edit_birthday_p1.getText().toString();
		String address_p1= edit_address_p1.getText().toString();
		String phone_p1= edit_phone_p1.getText().toString();
		String mail_p1 = edit_mail_p1.getText().toString();

		boolean validate = true;

		//Verify that the surname and first name are not empty
		if(surname_p1.equals("") || first_name_p1.equals("")){
			validate = false;
			if( surname_p1.equals(""))
				edit_surname_p1.setError(getString(R.string.names_fields));
			else
				edit_first_name_p1.setError(getString(R.string.names_fields));
		}

		//Verify the email format
		if(validate && !(mail_p1.equals("")) && !mail_p1.matches("^([\\w]+)@([\\w]+)\\.([\\w]+)$")){
			validate = false;
			edit_mail_p1.setError(getString(R.string.format_mail));
		}

		//Create the parent if the validation process is successfull
		if(validate){		
			Parent parent = new Parent(first_name_p1, surname_p1, birthday_p1, address_p1, phone_p1, mail_p1);
			parent.initParent();
			parent.addStudentToParent(student);
			finish();
		}
	}
}
