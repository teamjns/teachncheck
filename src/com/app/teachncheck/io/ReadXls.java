package com.app.teachncheck.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.app.teachncheck.MainActivity;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.bdd.DatabaseHandler;
import com.app.teachncheck.bdd.Student;


public class ReadXls {
	
	private Context context;
	private DatabaseHandler db;
	private String inputFile;
	private String classeName, abreviation, ann�e, niveauScolaire;
	private String erreurImport = null;
	private Classroom nouvelleClasse;
	
	
public ReadXls(Context context, String inputFile){
	db = MainActivity.getDb();
	this.context = context;
	this.inputFile = inputFile;
}

public void exportXls(){	
	new ReadXlsTask().execute();
}

private class ReadXlsTask extends AsyncTask<Void, Void, Boolean>{
	
	
	@Override
	protected Boolean doInBackground(Void... params) {
		
		Classroom classe = new Classroom();
		
		try {
			read();
		} catch (IOException e) {
			e.printStackTrace();
			return false;			
		}		
		return true;
		}
	
	protected void onPostExecute(final Boolean success){
        if (success){
        	Toast.makeText(context, "Import successful!", Toast.LENGTH_SHORT).show();
        }else{
        	Toast.makeText(context, "Import Error", Toast.LENGTH_SHORT).show();
        }
    }
}

public void read() throws IOException  {
	
	
	File inputWorkbook = new File(inputFile);
   // File inputWorkbook = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/TechNcheckFiles/xls_francais.xls");
    Workbook w;
    try {
      w = Workbook.getWorkbook(inputWorkbook);
      // Get the first sheet
      Sheet sheet = w.getSheet(0);
      Cell cell = sheet.getCell(0, 1);
      CellType type = cell.getType();
      if (type == CellType.LABEL) {
    	  this.classeName = cell.getContents();
      }
      else{
    	  this.erreurImport = "error import Name Class";
      }
      
      cell = sheet.getCell(1, 1);
      type = cell.getType();
      if (type == CellType.LABEL) {
    	  this.abreviation = cell.getContents();
      }
      else{
    	  this.abreviation = "";
      }
      
      cell = sheet.getCell(2, 1);
      type = cell.getType();
      if ((type == CellType.NUMBER) || (type == CellType.LABEL)) {
    	  this.ann�e = cell.getContents();
      }
      else{
    	  this.erreurImport = "erreur import Ann�e Classe";
      }
      
      cell = sheet.getCell(3, 1);
      type = cell.getType();
      if (type == CellType.LABEL) {
    	  this.niveauScolaire = cell.getContents();
      }
      else{
    	  this.erreurImport = "erreur Niveau Scolaire";
      }
  
	if(erreurImport == null){
		nouvelleClasse = new Classroom(classeName,  niveauScolaire, abreviation, ann�e);
		nouvelleClasse.initClass();
	}
	
	Sheet sheetStudent = w.getSheet(1);
	// read all lignes
	for (int i = 1; i < sheetStudent.getRows(); i++) {
		String name ="", surname="", birthday="", adress="", mail="", phone="";
		
		// download name cell
		cell = sheetStudent.getCell(0, i);
		type = cell.getType();	
		if (type == CellType.LABEL) {
			name = cell.getContents();
          }
		else {
			erreurImport = "erreur nom Etudiant";
          }
		
		
		// download last name cell
		cell = sheetStudent.getCell(1, i);
		type = cell.getType();		
		if (type == CellType.LABEL) {
			surname = cell.getContents();
          }
		else {
			erreurImport = "erreur Prenom Etudiant";
          }
		
		// download phone number cell
		cell = sheetStudent.getCell(2, i);
		type = cell.getType(); 		
		if ((type == CellType.NUMBER) || (type == CellType.LABEL)) {
			phone = cell.getContents();
          }
		else if (type == CellType.EMPTY) {
			phone = "";
          }
		else{			
			erreurImport = "erreur telepone Etudiant";
		}
		
		// download birthday cell
		cell = sheetStudent.getCell(3, i);
		type = cell.getType();
		if (type != CellType.EMPTY) {
			birthday = cell.getContents();
          }
		
		// download adress cell
		cell = sheetStudent.getCell(4, i);
		type = cell.getType();	
		if (type == CellType.LABEL) {
			adress = cell.getContents();
          }
		else if (type == CellType.EMPTY) {
			adress = "";
          }
		else{
			erreurImport = "erreur adresse Etudiant";
		}
		
		// download e-mail cell
		cell = sheetStudent.getCell(5, i);
		type = cell.getType();		
		if (type == CellType.LABEL) {
			mail = cell.getContents();
          }
		else if (type == CellType.EMPTY) {
			mail = "";
          }
		else{
			erreurImport = "erreur mail Etudiant";
		}
		
		
		if(erreurImport == null){
			Student eleve = new Student("", name, surname, birthday, adress, phone, mail, "");
			eleve.initStudent();
			this.nouvelleClasse.addStudentToClass(eleve);
		}
	}  
	 w.close();
    } catch (BiffException e) {
      e.printStackTrace();
    }
   
  }

public void setInputFile(String inputFile) {
    this.inputFile = inputFile;
  }
}