package com.app.teachncheck.io;
import java.io.File;
import java.io.IOException;

import jxl.CellView;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.app.teachncheck.MainActivity;
import com.app.teachncheck.R;
import com.app.teachncheck.bdd.DatabaseHandler;
import com.app.teachncheck.bdd.Student;

public class WritteXls {
	
	private Context context;
	private DatabaseHandler db;
	private int idClass;
	
	
public WritteXls(Context context, int idClass){
	db = MainActivity.getDb();
	this.context = context;
	this.idClass = idClass;	
}
	
public void exportXls(String path){
	
	new ExportXlsTask().execute(path);
}

private class ExportXlsTask extends AsyncTask<String, Void, Boolean>{
	
	private int currentSudent = 0;
	private WritableCellFormat times;
	
	@Override
	protected Boolean doInBackground(String... params) {
		try {
			File exportDir = new File(params[0]);
	    	if (!exportDir.exists()) {
	    		exportDir.mkdirs();
	    	}
			WritableWorkbook workbook = Workbook.createWorkbook(new File(params[0], db.getClassroom(idClass).getName() +".xls"));

			WritableSheet feuilleDetailClasse = workbook.createSheet(context.getString(R.string.class_detail), 0);
			
			WritableSheet feuilleDetailEleve = workbook.createSheet(context.getString(R.string.student_detail), 1);	
			//Maybe useless
			//WritableSheet feuilleNote = workbook.createSheet(context.getString(R.string.detail_mark), 2);
			//WritableSheet feuillePresence = workbook.createSheet(context.getString(R.string.i_vanish), 3);
			
			/*Tester pour definir la taille du xls
			 * 
			 WritableFont times10pt = new WritableFont(WritableFont.TIMES, 60);
			    // Define the cell format
			    times = new WritableCellFormat(times10pt);
			    // Lets automatically wrap the cells
			    times.setWrap(true);
			// feuille detail classe
			    CellView cv = new CellView();
			    cv.setFormat(times);
			*/
			feuilleDetailClasse.addCell(new Label(0, 0, context.getString(R.string.class_name)));
			feuilleDetailClasse.addCell(new Label(1, 0, context.getString(R.string.abreviation)));
			feuilleDetailClasse.addCell(new Label(2, 0, context.getString(R.string.class_year)));
			feuilleDetailClasse.addCell(new Label(3, 0, context.getString(R.string.school_lvl)));
			feuilleDetailClasse.addCell(new Label(4, 0, context.getString(R.string.student_number)));
			
			String test  =  Integer.toString(db.getClassroom(idClass).getAllStudentsOfClassroom().size());
			feuilleDetailClasse.addCell(new Label(0, 1, db.getClassroom(idClass).getName()));
			feuilleDetailClasse.addCell(new Label(1, 1, db.getClassroom(idClass).getEntitled()));
			feuilleDetailClasse.addCell(new Label(2, 1, db.getClassroom(idClass).getYear()));
			feuilleDetailClasse.addCell(new Label(3, 1, db.getClassroom(idClass).getLevel()));
			feuilleDetailClasse.addCell(new Label(4, 1, Integer.toString(db.getClassroom(idClass).getAllStudentsOfClassroom().size()) ));
			
			// feuille detail �leve			
			feuilleDetailEleve.addCell(new Label(0, 0, context.getString(R.string.name_real))); 
			feuilleDetailEleve.addCell(new Label(1, 0, context.getString(R.string.surname_real)));
			feuilleDetailEleve.addCell(new Label(2, 0, context.getString(R.string.phone_real)));
			feuilleDetailEleve.addCell(new Label(3, 0, context.getString(R.string.birthday)));
			feuilleDetailEleve.addCell(new Label(4, 0, context.getString(R.string.adress_real)));			
			feuilleDetailEleve.addCell(new Label(5, 0, context.getString(R.string.mail_real)));
			
			// feuille detail note
			/*
			feuilleNote.addCell(new Label(0, 0, Resources.getSystem().getString(R.string.name_real))); 
			feuilleNote.addCell(new Label(1, 0, Resources.getSystem().getString(R.string.surname_real)));
			
			// remplis les intutil� des notes
			for(int i = 0; i < db.getClassroom(idClasse).getAllMarksOfClassroom().size(); i++){
				feuilleNote.addCell(new Label(i+2, 0, db.getClassroom(idClasse).getMark(i)));
			}
			*/
			
			
			//remplissage de la feuille detail eleve
			for(Student s : db.getAllStudent(db.getClassroom(idClass))) {
				currentSudent++;
				feuilleDetailEleve.addCell(new Label(0, currentSudent, s.getName() )); 			
				feuilleDetailEleve.addCell(new Label(1, currentSudent, s.getSurname() )); 
				feuilleDetailEleve.addCell(new Label(2, currentSudent, s.getPhone() )); 
				feuilleDetailEleve.addCell(new Label(3, currentSudent, s.getBirthday() )); 
				feuilleDetailEleve.addCell(new Label(4, currentSudent, s.getAddress() )); 
				feuilleDetailEleve.addCell(new Label(5, currentSudent, s.getMail() )); 			
				
			}
			// remplis la feuille detail eleve
			// Gerer si pas de note
			/*
			for(Student s : db.getAllStudent(db.getClassroom(idClasse))) {
				ArrayList<StudentMark> temp =  s.getAllMarkOfStudent();

				currentSudent++;
				feuilleNote.addCell(new Label(0, currentSudent, s.getName() )); 			
				feuilleNote.addCell(new Label(1, currentSudent, s.getSurname() )); 
				for(int i = 0; i < db.getClassroom(idClasse).getAllMarksOfClassroom().size(); i++){
					feuilleNote.addCell(new Label(i+2, 0, temp.get(i).getvalue() + "/" + temp.get(i).getMark().getMax_value()));
				}
			}		*/
			
			workbook.write(); 
			workbook.close();			
			return true;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	}
}

