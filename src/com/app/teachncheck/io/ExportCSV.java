package com.app.teachncheck.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.app.teachncheck.R;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.bdd.DatabaseHandler;
import com.app.teachncheck.bdd.Student;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

public class ExportCSV{
	
	private Context context;
	private DatabaseHandler db;
	
	public ExportCSV(Context context){
		this.context = context;
		db = new DatabaseHandler(context);
	}
	
	public void exportStudents(String url){
		new ExportCsvTask().execute("student", url);
	}
	
	public void exportClassrooms(String url){
		new ExportCsvTask().execute("classroom", url);
	}
	
	public void exportClassroomsWithStudents(int idClassroom, String url){
		new ExportCsvTask().execute("classroomWstudent", url, String.valueOf(idClassroom));
	}
	
	private class ExportCsvTask extends AsyncTask<String, Void, Boolean>{

	    //private final ProgressDialog dialog = new ProgressDialog(context);

	    protected void onPreExecute(){
	        //this.dialog.setMessage("Exporting...");
	        //this.dialog.show();
	    }   

	    protected Boolean doInBackground(final String... args){
	    	boolean ok = false;
	    	CellProcessor[] processors = null;
	    	String[] header = null;
	    	
	    	if(args[0].equals("student")){
		    	processors = getProcessorsStudent();
		    	header = getHeaderStudent();
		    	try {
		    		File exportDir = new File(args[1]);
			    	if (!exportDir.exists()) {
			    		exportDir.mkdirs();
			    	}
		    		CsvBeanWriter beanWriter = new CsvBeanWriter(new FileWriter(exportDir.getAbsolutePath()+"/students.csv"), CsvPreference.STANDARD_PREFERENCE);
		    		beanWriter.writeHeader(header);
					for(Student s : db.getAllStudent()) {
	                    beanWriter.write(s, header, processors);
					}
					if( beanWriter != null ) {
	                    beanWriter.close();
					}
					ok=true;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					ok=false;
				}
	    	}else if(args[0].equals("classroom")){
	    		processors = getProcessorsClassroom();
		    	header = getHeaderClassroom();
		    	try {
		    		File exportDir = new File(args[1]);
			    	if (!exportDir.exists()) {
			    		exportDir.mkdirs();
			    	}
		    		CsvBeanWriter beanWriter = new CsvBeanWriter(new FileWriter(exportDir.getAbsolutePath()+"/classrooms.csv"), CsvPreference.STANDARD_PREFERENCE);
					beanWriter.writeHeader(header);
					for(Classroom c : db.getAllClassroom()) {
	                    beanWriter.write(c, header, processors);
					}
					if( beanWriter != null ) {
	                    beanWriter.close();
					}
					ok=true;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					ok=false;
				}
	    	}else if(args[0].equals("classroomWstudent")){
		    	try {
		    		
		    		int idClass = Integer.valueOf(args[2]);
		    		
		    		processors = getProcessorsClassroom();
			    	header = getHeaderClassroom();
			    	File exportDir = new File(args[1]);
			    	if (!exportDir.exists()) {
			    		exportDir.mkdirs();
			    	}
			    	CsvBeanWriter beanWriter = new CsvBeanWriter(new FileWriter(exportDir.getAbsolutePath()+"/ClassroomWithStudents.csv"), CsvPreference.STANDARD_PREFERENCE);
					beanWriter.writeHeader(header);
	                beanWriter.write(db.getClassroom(idClass), header, processors);
					
	                processors = getProcessorsStudent();
			    	header = getHeaderStudent();
					beanWriter.writeHeader(header);
					for(Student s : db.getAllStudent(db.getClassroom(idClass))) {
	                    beanWriter.write(s, header, processors);
					}
					
					if( beanWriter != null ) {
	                    beanWriter.close();
					}
					ok=true;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					ok=false;
				}
	    	}
	    	
	    	return ok;
	    }
	    
	    private String[] getHeaderStudent(){
	    	return new String[] { "id", "identity", "name", "surname",
                    "birthday", "address", "phone", "mail", "picture_path" };
	    }
	    
	    private CellProcessor[] getProcessorsStudent() {
	        return new CellProcessor[] {
	                new UniqueHashCode(), // id
	                new NotNull(), // identity
	                new NotNull(), // name
	                new NotNull(), // surname
	                new Optional(), // birthDate
	                new Optional(), // address 
	                new Optional(), // phone 
	                new Optional(), // mail
	                new Optional(), // picture path
	        };
	    }
	    
	    private String[] getHeaderClassroom(){
	    	return new String[] { "id", "name", "level", "entitled",
                    "year" };
	    }
	    
	    private CellProcessor[] getProcessorsClassroom() {
	        return new CellProcessor[] {
	                new UniqueHashCode(), // id
	                new NotNull(), // name
	                new NotNull(), // level
	                new Optional(), // entitled
	                new NotNull(), // year
	        };
	    }

	    protected void onPostExecute(final Boolean success){
	        /*if (this.dialog.isShowing()){
	        	this.dialog.dismiss();
	        }*/
	    	if (success){
                Toast.makeText(context, context.getString(R.string.export_successful), Toast.LENGTH_SHORT).show();
	        }else{
	        	Toast.makeText(context, context.getString(R.string.export_failed), Toast.LENGTH_SHORT).show();
	        }
	    }
	}
}
