package com.app.teachncheck.io;

import java.io.FileReader;
import java.io.IOException;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import com.app.teachncheck.R;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.bdd.DatabaseHandler;
import com.app.teachncheck.bdd.Student;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

public class ImportCSV {
	
	private Context context;
	private DatabaseHandler db;
	
	public ImportCSV(Context context){
		this.context = context;
		db = new DatabaseHandler(context);
	}
	
	public void importStudents(String url){
		new ImportCsvTask().execute("student", url);
	}
	
	public void importClassrooms(String url){
		new ImportCsvTask().execute("classroom", url);
	}
	
	public void importClassroomsWithStudents(String url){
		new ImportCsvTask().execute("classroomWstudent", url);
	}
	
	private class ImportCsvTask extends AsyncTask<String, Void, Boolean>{

//	    private final ProgressDialog dialog = new ProgressDialog(context);

	    protected void onPreExecute(){
//	        this.dialog.setMessage("Importing...");
//	        this.dialog.show();
	    }   

	    protected Boolean doInBackground(final String... args){
	    	boolean ok = false;
	    	CellProcessor[] processors = null;
	    	String[] header = null;
	    	
	    	if(args[0].equals("student")){
		    	processors = getProcessorsStudent();
		    	header = getHeaderStudent();
		    	try {
		    		CsvBeanReader beanReader = new CsvBeanReader(new FileReader(args[1]), CsvPreference.STANDARD_PREFERENCE);
	                beanReader.getHeader(true);
	                header = getHeaderStudent();
	                processors = getProcessorsStudent();
	                Student student;
	                while((student = beanReader.read(Student.class, header, processors)) != null ) {
//	                	System.out.println(String.format("lineNo=%s, rowNo=%s, student=%s", beanReader.getLineNumber(), beanReader.getRowNumber(), student));
	                	db.addStudent(student);
	                }
	                if( beanReader != null ) {
                        beanReader.close();
	                }
					ok=true;
				} catch (IOException e) {
					e.printStackTrace();
					ok=false;
				}
	    	}else if(args[0].equals("classroom")){
	    		processors = getProcessorsStudent();
		    	header = getHeaderStudent();
		    	try {
		    		CsvBeanReader beanReader = new CsvBeanReader(new FileReader(args[1]), CsvPreference.STANDARD_PREFERENCE);
		    		beanReader.getHeader(true);
	                header = getHeaderClassroom();
	                processors = getProcessorsClassroom();
	                Classroom classroom;
	                while((classroom = beanReader.read(Classroom.class, header, processors)) != null ) {
	                	System.out.println(String.format("lineNo=%s, rowNo=%s, classroom=%s", beanReader.getLineNumber(), beanReader.getRowNumber(), classroom));
	                	db.addClassroom(classroom);
	                }
	                if( beanReader != null ) {
                        beanReader.close();
	                }
					ok=true;
				} catch (IOException e) {
					e.printStackTrace();
					ok=false;
				}
	    	}else if(args[0].equals("classroomWstudent")){
		    	try {
		    		CsvBeanReader beanReader = new CsvBeanReader(new FileReader(args[1]), CsvPreference.STANDARD_PREFERENCE);
		    		beanReader.getHeader(true);
	                header = getHeaderClassroom();
	                processors = getProcessorsClassroom();
	                Classroom classroom = null;
	                Student student = null;
	                Classroom cl = null;
	                int i = 2;
	                while(i!=0) {
	                	if(i == 3){
	                		processors = getProcessorsStudent();
	    			    	header = getHeaderStudent();
	    			    	if((student = beanReader.read(Student.class, header, processors)) != null){
	    			    		
	    			    	}else{
	    			    		i=-1;
	    			    	}
	                	}
	                	if(i >= 3){
	                		if((student = beanReader.read(Student.class, header, processors)) != null){
	    	                	System.out.println(String.format("lineNo=%s, rowNo=%s, student=%s", beanReader.getLineNumber(), beanReader.getRowNumber(), student));
	    	                	Student st = new Student(student);
	    	                	st.initStudent();
	    	                	cl.addStudentToClass(st);
	                		}else{
	    			    		i=-1;
	    			    	}
	                	}else{
	                		if((classroom = beanReader.read(Classroom.class, header, processors)) != null){
	                			System.out.println(String.format("lineNo=%s, rowNo=%s, classroom=%s", beanReader.getLineNumber(), beanReader.getRowNumber(), classroom));
	                			cl = new Classroom(classroom);
	                			cl.initClass();
	                			
	                		}else{
	    			    		i=-1;
	    			    	}
	                	}
	                	i++;
	                }
	                if( beanReader != null ) {
                        beanReader.close();
	                }
					ok=true;
				} catch (IOException e) {
					e.printStackTrace();
					ok=false;
				}
	    	}
	    	
	    	return ok;
	    }
	    
	    private String[] getHeaderStudent(){
	    	return new String[] { null, "identity", "name", "surname",
                    "birthday", "address", "phone", "mail", "picture_path" };
	    }
	    
	    private CellProcessor[] getProcessorsStudent() {
	        return new CellProcessor[] {
	                null, // id
	                new Optional(), // identity
	                new NotNull(), // name
	                new NotNull(), // surname
	                new Optional(), // birthDate
	                new Optional(), // address 
	                new Optional(), // phone 
	                new Optional(), // mail
	                new Optional(), // picture path
	        };
	    }
	    
	    private String[] getHeaderClassroom(){
	    	return new String[] { null, "name", "level", "entitled",
                    "year" };
	    }
	    
	    private CellProcessor[] getProcessorsClassroom() {
	        return new CellProcessor[] {
	                null, // id
	                new NotNull(), // name
	                new NotNull(), // level
	                new Optional(), // entitled
	                new NotNull(), // year
	        };
	    }

	    protected void onPostExecute(final Boolean success){
	        /*if (this.dialog.isShowing()){
	        	this.dialog.dismiss();
	        }*/
	    	if (success){
                Toast.makeText(context, context.getString(R.string.import_successful), Toast.LENGTH_SHORT).show();
	        }else{
	            Toast.makeText(context, context.getString(R.string.import_failed), Toast.LENGTH_SHORT).show();
	        }
	    }
	}
	
}
