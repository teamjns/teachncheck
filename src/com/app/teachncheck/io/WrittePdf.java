package com.app.teachncheck.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.app.teachncheck.MainActivity;
import com.app.teachncheck.R;
import com.app.teachncheck.bdd.DatabaseHandler;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.pdf.utils.PDFWriter;
import com.app.teachncheck.pdf.utils.PaperSize;
import com.app.teachncheck.pdf.utils.StandardFonts;
import com.app.teachncheck.pdf.utils.Transformation;
/**
 * 
 * Limite bas c'est 575
 * @author Nicolas
 *
 */
public class WrittePdf {
	
	Activity activite;
	private DatabaseHandler db;
	TextView mText;
	int idClass;
	int currentStudent;	

public WrittePdf(Activity activite, int idClass){
	this.activite = activite;
	db = MainActivity.getDb();
	this.idClass = idClass;
	
	//String pdfcontent = writteInfoStudent();
	//outputToFile( db.getClassroom(idClass).getName() + ".pdf",pdfcontent,"ISO-8859-1");
	
}

public void exporterPDF(String path){
	new ExportPdfTask().execute(path);
}

private String writteInfoStudent() {
	PDFWriter mPDFWriter = new PDFWriter(PaperSize.A4_WIDTH, PaperSize.A4_HEIGHT);
	mPDFWriter.setFont(StandardFonts.SUBTYPE, StandardFonts.TIMES_ROMAN, StandardFonts.WIN_ANSI_ENCODING);
	
	try{
		mPDFWriter.addRawContent("0 1 0 RG\n");
		
		mPDFWriter.addText(20, 25, 22, activite.getString(R.string.class_name) + " : " + db.getClassroom(idClass).getName(), Transformation.DEGREES_270_ROTATION );		
		mPDFWriter.addText(40, 25, 22, activite.getString(R.string.class_year) + " : " + db.getClassroom(idClass).getYear(), Transformation.DEGREES_270_ROTATION);
		mPDFWriter.addText(60, 25, 22, activite.getString(R.string.student_number) + " : " + Integer.toString(db.getClassroom(idClass).getAllStudentsOfClassroom().size()), Transformation.DEGREES_270_ROTATION);
		mPDFWriter.addRawContent("0 1 0 rg\n");
		mPDFWriter.addText(40, 650, 16, "Teach'n'Check", Transformation.DEGREES_270_ROTATION);
		mPDFWriter.addImage(65, 750,BitmapFactory.decodeResource(activite.getResources(),R.drawable.ic_launcher), Transformation.DEGREES_270_ROTATION);
		mPDFWriter.addLine(63, 25, 67, 350);
		
		mPDFWriter.addRawContent("0 0 0 rg\n");
		mPDFWriter.addText(80, 25, 12, activite.getString(R.string.name_real), Transformation.DEGREES_270_ROTATION);
		mPDFWriter.addText(80, 125, 12, activite.getString(R.string.surname_real), Transformation.DEGREES_270_ROTATION);
		mPDFWriter.addText(80, 225, 12, activite.getString(R.string.phone), Transformation.DEGREES_270_ROTATION);
		mPDFWriter.addText(80, 325, 12, activite.getString(R.string.birthday), Transformation.DEGREES_270_ROTATION);
		mPDFWriter.addText(80, 425, 12, activite.getString(R.string.adress_real), Transformation.DEGREES_270_ROTATION);
		mPDFWriter.addText(80, 545, 12, activite.getString(R.string.mail_real), Transformation.DEGREES_270_ROTATION);
		
		
		
		
		for(Student s : db.getAllStudent(db.getClassroom(idClass))) {
			currentStudent++;
			if((85 + currentStudent * 14) <= 575){
				mPDFWriter.addText((85 + currentStudent * 14), 25, 11, s.getName(), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText((85 + currentStudent * 14), 125, 11, s.getSurname(), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText((85 + currentStudent * 14), 225, 11, s.getPhone(), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText((85 + currentStudent * 14), 325, 11, s.getBirthday(), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText((85 + currentStudent * 14), 425, 11, s.getAddress(), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText((85 + currentStudent * 14), 545, 11, s.getMail(), Transformation.DEGREES_270_ROTATION);
			}
			else{			
				mPDFWriter.newPage();
				mPDFWriter.addText(20, 25, 22, activite.getString(R.string.class_name) + db.getClassroom(idClass).getName(), Transformation.DEGREES_270_ROTATION );		
				mPDFWriter.addText(40, 25, 22, activite.getString(R.string.class_year) + " " + db.getClassroom(idClass).getYear(), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText(60, 25, 22, activite.getString(R.string.student_number) + " " + Integer.toString(db.getClassroom(idClass).getAllStudentsOfClassroom().size()), Transformation.DEGREES_270_ROTATION);
				
				mPDFWriter.addText(80, 25, 12, activite.getString(R.string.name_real), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText(80, 125, 12, activite.getString(R.string.surname_real), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText(80, 225, 12, activite.getString(R.string.phone), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText(80, 325, 12, activite.getString(R.string.birthday), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText(80, 425, 12, activite.getString(R.string.adress_real), Transformation.DEGREES_270_ROTATION);
				mPDFWriter.addText(80, 545, 12, activite.getString(R.string.mail_real), Transformation.DEGREES_270_ROTATION);				
				currentStudent = 0;
				
			}			
		}
	}
	catch(Exception e){
		mPDFWriter.addText(50, 800, 32, "Error with PDF export ");
	}
    String s = mPDFWriter.asString();
    return s;
}

private void outputToFile(String fileName, String pdfContent, String encoding, String path) {
    File newFile = new File(path + fileName);
    try {
        newFile.createNewFile();
        try {
        	FileOutputStream pdfFile = new FileOutputStream(newFile);
        	pdfFile.write(pdfContent.getBytes(encoding));
            pdfFile.close();
        } catch(FileNotFoundException e) {
        	System.out.println("File not found");
        }
    } catch(IOException e) {
    	System.out.println("Error with IO");
    }
}

private class ExportPdfTask extends AsyncTask<String, Void, Boolean>{

	@Override
	protected Boolean doInBackground(String... arg) {
		try{
		String pdfcontent = writteInfoStudent();
		File exportDir = new File(arg[0]);
    	if (!exportDir.exists()) {
    		exportDir.mkdirs();
    	}
		outputToFile(db.getClassroom(idClass).getName() + ".pdf",pdfcontent,"ISO-8859-1", arg[0]);
		return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	protected void onPostExecute(final Boolean success){
        if (!success){
        	Toast.makeText(activite, "Error with PDF export", Toast.LENGTH_SHORT).show();
        }
    }
	
}
}
