package com.app.teachncheck;

import java.util.ArrayList;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.utils.CommonMethods;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;


/**
 * <b>AddStudentClassActivity allows to add students to a classroom.</b>
 * <p>The user can select existing students or create new ones, one by one or several at once.</p>
 * <p>Uses the activity_add_student_class layout.</p>
 * 
 * @see MainActivity
 * @see SettingsActivity
 * @see ListStudentToChooseActivity
 * @see StudentCreateToClassActivity
 * @see CreateMultipleStudentsToClassActivity 
 */
public class AddStudentClassActivity extends SherlockActivity {

	/**
	 * Classroom in which the students will be added
	 */
	private Classroom classroom;
	/**
	 * ListView listing all students in the classroom
	 */
	private ListView list_students;	
	/**
	 * Indicates if the calling activity asked for a result
	 */
	private String specialCall = null;
	 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student_class);        
        
        //Get the classroom
        Bundle bundle = this.getIntent().getExtras();
        classroom = (Classroom) bundle.getSerializable("class");
        
        //Verify if it is a special call, in which case the validation will not launch the home activity
        specialCall = bundle.getString("special");
        
        ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		//Button used to select students in the list of all existing students
		ImageButton bt_select = (ImageButton) findViewById(R.id.bt_select_student);
		bt_select.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AddStudentClassActivity.this,ListStudentToChooseActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("class", classroom);
				intent.putExtras(bundle);
				startActivityForResult(intent, 0);	
			}
		});
		
		//Button used to create a single student and add it to the classroom
		ImageButton bt_add_student = (ImageButton) findViewById(R.id.bt_add_student);
		bt_add_student.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AddStudentClassActivity.this,StudentCreateToClassActivity.class);				
				startActivityForResult(intent, 1);
			}
		});
		
		//Button used to create and add several new students
		ImageButton bt_add_students = (ImageButton) findViewById(R.id.bt_add_students);
		bt_add_students.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AddStudentClassActivity.this,CreateMultipleStudentsToClassActivity.class);				
				startActivityForResult(intent, 2);
			}
		});
		
		list_students = (ListView) findViewById(R.id.list_students);
		
    }

    /**
     * Update the students list
     */
    protected void onResume() {
		super.onResume();
		initList();
	}

    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
    	inflater.inflate(R.menu.menu_check_only, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case android.R.id.home:
    			CommonMethods.returnHome(this);
    			return true;
    		case R.id.menu_settings:
    			CommonMethods.gotoSettings(this);
    			return true;
    		case R.id.submit_form:
    			if(specialCall != null){
    				finish();
    			}else{
    				CommonMethods.returnHome(this);
    			}
    			return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if(data != null){
    		//Add all students selected in the existing students list
			if (requestCode == 0) {
				@SuppressWarnings("unchecked")
				ArrayList<Student> lStudents_toAdd= (ArrayList<Student>) data.getSerializableExtra("students");
				for(Student s:lStudents_toAdd){
					classroom.addStudentToClass(s);
				}
			}
			//Add the new student to the classroom
			else if (requestCode == 1){
				Student s = (Student) data.getSerializableExtra("student");
				classroom.addStudentToClass(s);
			}
			//Add each new student to the classroom
			else if (requestCode == 2){
				@SuppressWarnings("unchecked")
				ArrayList<Student> lStudents_toAdd = (ArrayList<Student>) data.getSerializableExtra("students");
				for(Student s:lStudents_toAdd)
					classroom.addStudentToClass(s);		
			}
		}
	}
    
	/**
	 * 
	 */
	private void initList(){
		ArrayList<Student> l_students = classroom.getAllStudentsOfClassroom();
		list_students.setAdapter(new ArrayAdapter<Student>(AddStudentClassActivity.this,android.R.layout.simple_list_item_1, l_students));
	}
    
}

