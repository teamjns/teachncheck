package com.app.teachncheck.utils;

import android.content.Context;
import android.content.Intent;

import com.app.teachncheck.MainActivity;
import com.app.teachncheck.SettingsActivity;

public class CommonMethods {

    public static void returnHome(Context c){
    	Intent myIntent = new Intent(c, MainActivity.class);
    	myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        c.startActivity(myIntent);
    }
	
    public static void gotoSettings(Context c){
    	Intent myIntent = new Intent(c, SettingsActivity.class);
    	myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(myIntent);
    }
}
