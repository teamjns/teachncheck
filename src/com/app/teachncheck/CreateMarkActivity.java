package com.app.teachncheck;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.bdd.Mark;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.bdd.StudentMark;
import com.app.teachncheck.classes.CheckBoxPerso;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>Activity used to create a new mark to all students of a classroom.</b>
 * <p>A student can have a number as a mark or no mark. In the last case, the right checkbox have to be checked.</p>
 */
public class CreateMarkActivity extends SherlockActivity {

	/**
	 * List of couples CheckBox and EditText
	 */
	private HashMap<CheckBoxPerso, EditText> listEdit;
	/**
	 * Mark added to the classroom's students
	 */
	private Mark mark;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark);
        
        //Retrieve classroom and the mark created in the previous activity
		Bundle bundle = this.getIntent().getExtras();
        Classroom classroom = (Classroom) bundle.getSerializable("class");
        mark = (Mark)bundle.getSerializable("mark");
        
        ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle(mark.getName());
		
		listEdit = new HashMap<CheckBoxPerso, EditText>();
		
		TableLayout table = (TableLayout) findViewById(R.id.mark_table);
		
		//Tables head
    	TableRow r = new TableRow(this);
    	TextView t4 = new TextView(this);
    	TextView t5 = new TextView(this);
    	TextView t6 = new TextView(this);
    	t6.setText(R.string.no_mark);
    	r.addView(t4);
    	r.addView(t5);
    	r.addView(t6);
    	table.addView(r,new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
    	//Fills the students list
		ArrayList<Student> students = classroom.getAllStudentsOfClassroom();
		for(Student student: students){
        	TableRow row = new TableRow(this);
        	TextView t = new TextView(this);
        	t.setText(student.toString());
        	t.setTextSize(20);
        	t.setGravity(Gravity.CENTER_VERTICAL);
        	row.addView(t);
        	
        	//EditText used to provide a value for the mark and this student
        	LinearLayout linear = new LinearLayout(this);
        	linear.setOrientation(LinearLayout.HORIZONTAL);
        	EditText edit = new EditText(this);
        	edit.setHint(mark.getMax_value()+"");
        	edit.setInputType(InputType.TYPE_CLASS_NUMBER);
        	TextView t2 = new TextView(this);
        	t2.setText("/ ");
        	t2.setTextSize(20);
        	TextView t3 = new TextView(this);
        	t3.setText(mark.getMax_value()+"");
        	t3.setTextSize(20);
        	linear.addView(edit);
        	linear.addView(t2);
        	linear.addView(t3);
        	row.addView(linear);

        	//Checkbox used to indicates that a student is not evaluated (no mark)
        	LinearLayout linear2 = new LinearLayout(this);
        	linear2.setOrientation(LinearLayout.HORIZONTAL);
        	CheckBoxPerso check = new CheckBoxPerso(this);
        	check.setStudent(student);
        	check.setOnClickListener(checkOnClickListener);
        	linear2.addView(check);
        	linear2.setGravity(Gravity.CENTER_HORIZONTAL);
        	row.addView(linear2);
        	
            table.addView(row,new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        
            listEdit.put(check, edit);
		}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
    	inflater.inflate(R.menu.menu_check_only, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case android.R.id.home:
    			CommonMethods.returnHome(this);
    			return true;
    		case R.id.submit_form:
    			validation();
    			return true;
    		case R.id.menu_settings:
    			CommonMethods.gotoSettings(this);
    			return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }
    
    /**
     * Hide/Show the EditText when a CheckBox is checked.
     */
    private OnClickListener checkOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			CheckBoxPerso checkbox = (CheckBoxPerso) v;
			EditText edit = listEdit.get(checkbox);
			if(checkbox.isChecked()){
				edit.setVisibility(EditText.INVISIBLE);
			}else{
				edit.setVisibility(EditText.VISIBLE);
			}
		}
	};
	
	/**
	 * Validates all data, then saves values and close the activity
	 */
	private void validation(){
		if(noEmptyMarks()){
			if(noTooHighMarks()){
				mark.initMark();
				for(CheckBoxPerso check : listEdit.keySet()){
					if(!check.isChecked()){
						EditText edit = listEdit.get(check);
						StudentMark st_mark = new StudentMark(edit.getText().toString(), mark, check.getStudent());
						st_mark.initStudentMark();
					}
				}
				finish();
			}else
				Toast.makeText(this, getString(R.string.too_high_mark), Toast.LENGTH_LONG).show();
		}
		else
			Toast.makeText(this, getString(R.string.empty_marks), Toast.LENGTH_LONG).show();	        			
	}
	
	/**
	 * Verifies that no value go higher than the maximum mark's value
	 * @return
	 * 		<ul>
	 * 			<li>True : all values are under the max value</li>
	 * 			<li>False : at least one value is too high</li>
	 * 		</ul>
	 */
	private boolean noTooHighMarks(){
		for(CheckBoxPerso check : listEdit.keySet()){
			if(!check.isChecked()){
				EditText edit = listEdit.get(check);
				if(Float.parseFloat(edit.getText().toString()) > Float.parseFloat(mark.getMax_value()))
					return false;
			}
		}
		return true;
	}
	
	/**
	 * Verifies that all marks are provided, except if the no mark CheckBox is checked
	 * @return
	 * 		<ul>
	 * 			<li>True : no required mark is empty</li>
	 * 			<li>False : at least one required mark is empty</li>
	 * 		</ul>
	 */
	private boolean noEmptyMarks(){
		for(CheckBoxPerso check : listEdit.keySet()){
			EditText edit = listEdit.get(check);
			if(edit.getText().toString().equals("") && edit.getVisibility() == EditText.VISIBLE)
				return false;
		}
		return true;
	}
}
