package com.app.teachncheck;

import java.util.Calendar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>ClassAddActivity contains the form allowing to create a new Classroom.</b>
 * <p>Offers the possibility to add students</p>
 * <p>Uses the activity_add_class layout.</p>
 * 
 * @see MainActivity
 * @see SettingsActivity
 * @see AddStudentClassActivity
 */
public class ClassAddActivity extends SherlockActivity {

	/**
	 * EditText for classroom's name
	 */
	private EditText edit_name;
	/**
	 * EditText for classroom's short name
	 */
	private EditText edit_short_name;
	/**
	 * EditText for classroom's level
	 */
	private EditText edit_level;
	/**
	 * EditText for classroom's year
	 */
	private EditText edit_year;
	/**
	 * Menu of this activity
	 */
	private Menu mMenu;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_class);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		edit_name = (EditText) findViewById(R.id.edit_name_class);
		edit_name.addTextChangedListener(validateOnChangeListener);

		edit_short_name = (EditText) findViewById(R.id.edit_short_name_class);
		edit_short_name.addTextChangedListener(validateOnChangeListener);

		edit_level = (EditText) findViewById(R.id.edit_level_class);
		edit_level.addTextChangedListener(validateOnChangeListener);

		edit_year = (EditText) findViewById(R.id.edit_year_class);
		edit_year.setText(Calendar.getInstance().get(Calendar.YEAR)+"");
		edit_year.addTextChangedListener(validateOnChangeListener);	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_simple, menu);
		mMenu = menu;
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			CommonMethods.returnHome(this);
			return true;
		case R.id.submit_form:
			validation();
			return true;
		case R.id.menu_settings:
			CommonMethods.gotoSettings(this);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Called each time something is typed in a field. The submit button is showed only if all the required fields are not empty.
	 */
	private TextWatcher validateOnChangeListener = new TextWatcher() {

		public void afterTextChanged(Editable s) {
			if(noEmptyField()){
				mMenu.clear();
				MenuInflater inflater = getSupportMenuInflater();
				inflater.inflate(R.menu.menu_check_only, mMenu);
			}
		}

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

		public void onTextChanged(CharSequence s, int start, int before, int count) {}

	};

	/**
	 * Check the format of all fields and create the classroom.
	 */
	private void validation(){
		if(!edit_name.getText().toString().equals("")
				&& !edit_short_name.getText().toString().equals("")
				&& !edit_year.getText().toString().equals("")){

			if(!(edit_year.getText().toString().length()<4)){

				//Create a new dialog which offers to add students to the new classroom
				AlertDialog addStudentDialog = new AlertDialog.Builder(ClassAddActivity.this).create();
				addStudentDialog.setTitle(getString(R.string.create));
				addStudentDialog.setMessage(getString(R.string.do_you_add_student));
				
				//If the user choose YES, an instance of AddStudentClassActivity is started 
				addStudentDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
				      public void onClick(DialogInterface dialog, int which) {
				    	  
				    	  Classroom classroom = createCLassroom();
				    	  
				    	  Intent myIntent = new Intent(ClassAddActivity.this, AddStudentClassActivity.class);
						  myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						  myIntent.putExtra("class", classroom);
						  startActivity(myIntent);	 
				    } });
				
				//If the user choose NO, then the application return to the main activity
				addStudentDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no), new DialogInterface.OnClickListener() {
				      public void onClick(DialogInterface dialog, int which) {
				    	  
				    	  createCLassroom();
				    	  
				    	  Intent myIntent = new Intent(ClassAddActivity.this, MainActivity.class);
						  myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						  startActivity(myIntent);	 
				    } });

				addStudentDialog.show();
				
			}else{
				edit_year.setError(getString(R.string.four_digit_year) );
			}
		}
		else{
			if(edit_name.getText().toString().equals(""))
				edit_name.setError(getString(R.string.empty_fields) );
			if(edit_short_name.getText().toString().equals(""))
				edit_short_name.setError(getString(R.string.empty_fields) );
			if(edit_year.getText().toString().equals(""))
				edit_year.setError(getString(R.string.empty_fields) );        			
		}
	}
	
	/**
	 * Creates a new classroom
	 */
	private Classroom createCLassroom(){
		Classroom classroom = new Classroom(edit_name.getText().toString()
				,edit_level.getText().toString()
				,edit_short_name.getText().toString()
				,edit_year.getText().toString());
		
		classroom.initClass();
		
		return classroom;
	}

	/**
     * Verify that the main fields are not empty.
     * @return True if the fields are not empty, False otherwise.
     */
	private boolean noEmptyField(){
		if(!edit_name.getText().toString().equals("")
				&& !edit_short_name.getText().toString().equals("")
				&& !edit_year.getText().toString().equals(""))
			return true;
		return false;
	}

}
