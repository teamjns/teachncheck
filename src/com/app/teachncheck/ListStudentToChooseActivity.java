package com.app.teachncheck;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnActionExpandListener;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.bdd.Student;

/**
 * <b>This class displays the list of all existing students that can be added to the classroom.</b>
 * <p>The user can select several students and then validate his choices.</p>
 * <p>Uses the activity_dialog_list_student layout.</p>
 */
public class ListStudentToChooseActivity extends SherlockActivity {

	/**
	 * ListView displaying students name and surname
	 */
	private ListView listViewStudents;
	/**
	 * List of all existing students
	 */
	private ArrayList<Student> listStudents;
	/**
	 * Adapter of the ListView
	 */
	private ArrayAdapter<Student> adapter = null;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_list_student);

        //Retrieve the classroom
        Bundle bundle = this.getIntent().getExtras();
        Classroom classroom = (Classroom) bundle.getSerializable("class");
        
        listViewStudents = (ListView) findViewById(R.id.list_students);
        
        //Retrieve students from the database and remove students already in the classroom
        listStudents = MainActivity.getDb().getAllStudent();
        ArrayList<Student> listStudentsInClassroom = MainActivity.getDb().getAllStudent(classroom);
        Iterator<Student> iterStudents = listStudents.iterator();
        while(iterStudents.hasNext()){
        	Student student = iterStudents.next();
        	for(Student s : listStudentsInClassroom)
        		if(s.getId() == student.getId())
        			iterStudents.remove();
        }
		
        //Fill the ListView
        adapter = new ArrayAdapter<Student>(ListStudentToChooseActivity.this,android.R.layout.simple_list_item_multiple_choice, listStudents);
		listViewStudents.setAdapter(adapter);
		listViewStudents.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
    	inflater.inflate(R.menu.menu_activity_list_student_to_choose, menu);
    	
    	//Set the search zone behavior
		View v = (View) menu.findItem(R.id.menu_search).getActionView();	
        final EditText edit_search = ( EditText ) v.findViewById(R.id.txt_search);
        edit_search.addTextChangedListener(searchOnChangeListener);
    	
    	menu.findItem(R.id.menu_search).setOnActionExpandListener(new OnActionExpandListener() {
            
    		//Expand the search zone when clicked and display the keyboard
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
            	edit_search.post(new Runnable() {
                    @Override
                    public void run() {
                    	edit_search.requestFocus();
                		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                		imm.showSoftInput(edit_search, InputMethodManager.SHOW_FORCED);
                    }
                });
                return true;
            }
            
            //Collapse the search zone and hide the keyboard
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
            	edit_search.post(new Runnable() {
                    @Override
                    public void run() {
                    	edit_search.clearFocus();
                    }
                });
                edit_search.setText("");
                return true;
            }
        });
		
        return true;
    }
	
	public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case R.id.submit_form:
    			
    			//Lists selected students
    			SparseBooleanArray listModified = listViewStudents.getCheckedItemPositions();
				ArrayList<Student> lStudent_Selected = new ArrayList<Student>();
				for(int pos=0;pos<listModified.size();pos++){
					if(listModified.get(listModified.keyAt(pos))==true){
						Student student = listStudents.get(listModified.keyAt(pos));
						lStudent_Selected.add(student);				
					}
				}
				
				//Send the students list to the parent activity
				Intent result = new Intent();
				result.putExtra("students", lStudent_Selected);
				setResult(RESULT_OK, result);
				finish();
    			return true;
    			
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }
	
	/**
	 * Filter the students ListView content according to the search zone content
	 */
	private TextWatcher searchOnChangeListener = new TextWatcher() {

		public void afterTextChanged(Editable s) {
		}

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			adapter.getFilter().filter(s);
		}

	};
}
