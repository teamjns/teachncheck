package com.app.teachncheck;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.teachncheck.bdd.Classroom;
import com.app.teachncheck.fragments.ClassDetailsFragment;
import com.app.teachncheck.fragments.ClassMarksFragment;
import com.app.teachncheck.fragments.ClassStudentsFragment;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>This activity contains the three fragments used to display data about a classroom (Details, marks, students).</b>
 *
 * @see ClassDetailsFragment
 * @see ClassMarksFragment
 * @see ClassStudentsFragment
 */
public class ClassActivity extends SherlockFragmentActivity {

	/**
	 * Classroom whose data are displayed
	 */
	private Classroom classroom;
	/**
	 * Just the menu
	 */
	private Menu myMenu;
	
	/**
	 * Actionbar tab for classroom's details
	 */
	private ActionBar.Tab tabDetails;
	/**
	 * Actionbar tab for classroom's students
	 */
	private ActionBar.Tab tabStudents;
	/**
	 * Actionbar tab for classroom's marks
	 */
	private ActionBar.Tab tabMarks;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //Retrieve the classroom
        Bundle bundle = this.getIntent().getExtras();
        classroom = (Classroom) bundle.getSerializable("class");
        
        ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);	
		actionBar.setTitle(classroom.getName());
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		tabDetails = actionBar.newTab().setText(R.string.tab_class_details);
		tabStudents = actionBar.newTab().setText(R.string.tab_class_students);
		tabMarks = actionBar.newTab().setText(R.string.tab_class_marks);
		
		tabDetails.setTabListener(new TabListener<ClassDetailsFragment>(this,"title1", ClassDetailsFragment.class));
		tabStudents.setTabListener(new TabListener<ClassStudentsFragment>(this,"title2", ClassStudentsFragment.class));
		tabMarks.setTabListener(new TabListener<ClassMarksFragment>(this,"title3", ClassMarksFragment.class));
		
		actionBar.addTab(tabDetails);
		actionBar.addTab(tabStudents);
		actionBar.addTab(tabMarks);	
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case android.R.id.home:
    			CommonMethods.returnHome(this);
    			return true;
    		case R.id.menu_settings:
    			CommonMethods.gotoSettings(this);
    			return true;
    		default:
    			return super.onOptionsItemSelected(item);
    	}
    }

    /**
	 * <b>Listener for the ActionBar tabs.</b>
	 * <p>Here it is used to change the content of the action bar menu depending on the tab selected.</p>
	 * @param <T> An instance of {@link ClassDetailsFragment}, {@link ClassMarksFragment} or {@link ClassStudentsFragment}
	 */
    private class TabListener<T extends Fragment> implements
		    ActionBar.TabListener {
		private Fragment mFragment;
		private final SherlockFragmentActivity mActivity;
		private final String mTag;
		private final Class<T> mClass;
		
		/**
		 * Constructor used each time a new tab is created.
		 * 
		 * @param classActivity
		 *            The host Activity, used to instantiate the fragment
		 * @param tag
		 *            The identifier tag for the fragment
		 * @param clz
		 *            The fragment's Class, used to instantiate the fragment
		 */
		public TabListener(ClassActivity classActivity, String tag, Class<T> clz) {
		    mActivity = classActivity;
		    mTag = tag;
		    mClass = clz;
		}
		
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
		    
			if(myMenu!=null){
				if(tab.getPosition()==0){
					myMenu.clear();
		        	MenuInflater inflater = ClassActivity.this.getSupportMenuInflater();
		        	inflater.inflate(R.menu.menu_modif, myMenu);
				}else{
					myMenu.clear();
		        	MenuInflater inflater = ClassActivity.this.getSupportMenuInflater();
		        	inflater.inflate(R.menu.menu_activity_students_list, myMenu);
				}
			}
			
		    if (mFragment == null) {
		        
		    	Bundle bundle = new Bundle();
				bundle.putSerializable("class",classroom);
				
		        mFragment = Fragment.instantiate(mActivity, mClass.getName(),bundle);
		        ft.add(android.R.id.content, mFragment, mTag);
		        
		    } else {      
		    	
		        ft.attach(mFragment);
		    }
		}
		
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		    if (mFragment != null) {
		        // Detach the fragment, because another one is being attached
		    	ft.detach(mFragment);
		    }
		}
		
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		    // User selected the already selected tab. Usually do nothing.
		}
    }   
}
