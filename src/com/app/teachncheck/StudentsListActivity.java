package com.app.teachncheck;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnActionExpandListener;
import com.app.teachncheck.bdd.Student;
import com.app.teachncheck.utils.CommonMethods;

/**
 * <b>StudentsListActivity display a list of all students.</b>
 * <p>This activity allows to :</p>
 * <ul>
 * 		<li>Access a student's details</li>
 * 		<li>Search for a particular student</li>
 * 		<li>Create a new student</li>
 * 		<li>Create several students at once</li>
 * 		<li>Delete one or more students</li>
 * </ul>
 * <p>Uses the activity_students_list layout.</p>
 * 
 * @see CreateMultipleStudentsToClassActivity
 * @see StudentActivity
 * @see StudentAddActivity
 */
public class StudentsListActivity extends SherlockActivity {

	/**
	 * List of students
	 */
	private ArrayList<Student> listStudents;
	/**
	 * ListView displaying students
	 */
	private ListView listView_StudentList;
	/**
	 * Action mode currently started
	 */
	private ActionMode mMode;
	/**
	 * Number of selected students
	 */
	private int nbStudentsSelected;
	/**
	 * Textview in at the head of checkbox column
	 */
	private TextView text_open;
	/**
	 * Icon displayed when {@link DeleteActionMode} is activated
	 * 
	 * @see DeleteActionMode
	 */
	private ImageView trashIcon;	
	/**
	 * Listview's adapter
	 */
	private ArrayAdapter<Student> adapter = null;
	/**
	 * Edittext used for the student's search
	 */
	private EditText edit_search;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_students_list);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		listView_StudentList = (ListView) findViewById(R.id.list_students);
		
		//Fills the listview with students
		initList();
		
		listView_StudentList.setOnItemClickListener(onStudentClicked);
		listView_StudentList.setOnItemLongClickListener(onStudentLongClicked);
		listView_StudentList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		text_open = (TextView) findViewById(R.id.text_open);
		trashIcon = (ImageView) findViewById(R.id.icon_delete_edit);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		for(int pos=0;pos<listView_StudentList.getAdapter().getCount();pos++){
			listView_StudentList.setItemChecked(pos, false);
		}      
		initList();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_activity_students_list, menu);
		
		//Set the behavior of the search bar
		menu.findItem(R.id.menu_search).setOnActionExpandListener(new OnActionExpandListener() {
            
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
            	edit_search.post(new Runnable() {
                    @Override
                    public void run() {
                    	edit_search.requestFocus();
                		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

                		imm.showSoftInput(edit_search, InputMethodManager.SHOW_FORCED);
                    }
                });
                return true;
            }
            
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
            	edit_search.post(new Runnable() {
                    @Override
                    public void run() {
                    	edit_search.clearFocus();
                    }
                });
                edit_search.setText("");
                return true;
            }
        });
		
		View v = (View) menu.findItem(R.id.menu_search).getActionView();
		
        edit_search = ( EditText ) v.findViewById(R.id.txt_search);
        edit_search.addTextChangedListener(searchOnChangeListener);
        
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			CommonMethods.returnHome(this);
			return true;
		case R.id.menu_add_student:
			Intent myIntent = new Intent(this, StudentAddActivity.class);
	    	myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        startActivity(myIntent);
			return true;
		case R.id.menu_add_students:
			Intent myIntent2 = new Intent(this, CreateMultipleStudentsToClassActivity.class);
	    	myIntent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        startActivityForResult(myIntent2,0);
			return true;
		case R.id.menu_settings:
			CommonMethods.gotoSettings(this);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	/**
	 * Filter the students list depending on what is typed in the search EditText
	 */
	private TextWatcher searchOnChangeListener = new TextWatcher() {

		public void afterTextChanged(Editable s) {}

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			adapter.getFilter().filter(s);
		}

	};

	/**
	 * Action mode allowing to delete the selected students
	 */
	private final class DeleteActionMode implements ActionMode.Callback {
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			
			//Display menu with delete button
			MenuInflater inflater = getSupportMenuInflater();
			inflater.inflate(R.menu.menu_delete, menu);
			
			nbStudentsSelected = 0;
			
			text_open.setVisibility(TextView.GONE);
			trashIcon.setVisibility(ImageView.VISIBLE);
			
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

			switch (item.getItemId()) {
			case R.id.delete:	
				
				//Gets all selected students
				SparseBooleanArray listModified = listView_StudentList.getCheckedItemPositions();
				final ArrayList<Student>lStudent_to_Delete = new ArrayList<Student>();
				for(int pos=0;pos<listModified.size();pos++){
					if(listModified.get(listModified.keyAt(pos))==true){
						Student student1 = listStudents.get(listModified.keyAt(pos));
						lStudent_to_Delete.add(student1);
					}
				}
				
				//Ask confirmation in a dialog
				AlertDialog addStudentDialog = new AlertDialog.Builder(StudentsListActivity.this).create();
				addStudentDialog.setTitle(getString(R.string.confirmation));
				addStudentDialog.setMessage(getString(R.string.delete_confirmation_student));
				addStudentDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
				      public void onClick(DialogInterface dialog, int which) {
				    	  for(Student c:lStudent_to_Delete){
				    		  c.deleteStudent();	
				    	  }
				    	  initList();
				}});
				
				addStudentDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no),new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}});
				
				addStudentDialog.show();
				
				mMode.finish();
				
				return true;
				
			default:
				return true;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			for(int pos=0;pos<listView_StudentList.getAdapter().getCount();pos++){
				listView_StudentList.setItemChecked(pos, false);
			}
			listView_StudentList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			mMode = null;
			
			//Display text and hide icon
			text_open.setVisibility(TextView.VISIBLE);
			trashIcon.setVisibility(ImageView.GONE);
		}
	}
	
	/**
	 * Fills the ListView with students
	 */
	public void initList(){
		listStudents = MainActivity.getDb().getAllStudent();
		adapter = new ArrayAdapter<Student>(this,android.R.layout.simple_list_item_multiple_choice, listStudents);
		listView_StudentList.setAdapter(adapter);
	}
	
	/**
	 * Called when a checkbox is clicked
	 */
	private OnItemClickListener onStudentClicked = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
			
			//Count the students
			if(listView_StudentList.isItemChecked(position))
				nbStudentsSelected++;
			else
				nbStudentsSelected--;

			if(mMode==null){		
				Bundle bundle = new Bundle();
				bundle.putSerializable("student",listStudents.get(position));
				Intent newIntent = new Intent(StudentsListActivity.this, StudentActivity.class);
				newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				newIntent.putExtras(bundle);
				startActivity(newIntent);	
			}else if(nbStudentsSelected == 0){
				mMode.finish();
			}
		}
	};
	
	/**
	 * Called when a checkbox is long clicked. Start the action mode.
	 */
	private OnItemLongClickListener onStudentLongClicked = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view,
				int position, long id) {
			if(mMode==null){
				mMode = startActionMode(new DeleteActionMode());
				listView_StudentList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			}
			return false;
		}
	};
	
	/**
	 * 	Retrieve the list of students to add.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if(data != null){
				if(requestCode == 0){
					initList();
				}
			}
		}
}
