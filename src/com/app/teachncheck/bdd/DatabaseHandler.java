package com.app.teachncheck.bdd;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * 
 * @author Julien
 *
 * Class to handle the database
 *
 */
public class DatabaseHandler extends SQLiteOpenHelper {

	// Database Version
	private static final int DATABASE_VERSION = 14;
	
	ArrayList<String> tablesModified = new ArrayList<String>();

	// Database Name
	private static final String DATABASE_NAME = "db";
	
	public Context context;

	// Contacts table name
	private static final String TABLE_STUDENT = "student";
	private static final String TABLE_CLASS_STUDENT = "class_student";
	private static final String TABLE_CLASS = "class";
	private static final String TABLE_SCHEDULE = "schedule";
	private static final String TABLE_ABSENCE = "absence";
	private static final String TABLE_PARENT_STUDENT = "parent_student";
	private static final String TABLE_PARENT = "parent";
	private static final String TABLE_MARK = "mark";
	private static final String TABLE_STUDENT_MARK = "student_mark";

	// Contacts Table Columns names
	private static final String id_student = "id_student";
	private static final String identity_student = "identity_student";
	private static final String name_student = "name_student";
	private static final String surname_student = "surname_student";
	private static final String birthday_student = "birthday_student";
	private static final String address_student = "address_student";
	private static final String phone_student = "phone_student";
	private static final String mail_student = "mail_student";
	private static final String picture_path_student = "picture_path_student";

	private static final String id_class = "id_class";
	private static final String name_class = "name_class";
	private static final String level_class = "level_class";
	private static final String entitled_class = "entitled_class";
	private static final String year_class = "year_class";

	private static final String id_schedule = "id_schedule";
	private static final String day_schedule = "day_schedule";
	private static final String begin_schedule = "begin_schedule";
	private static final String end_schedule = "end_schedule";
	private static final String id_class_schedule = "id_class";
	private static final String checked_schedule = "checked_schedule";

	private static final String id_absence = "id_absence";
	private static final String justified_absence = "justified_absence";
	private static final String reason_absence = "reason_absence";

	private static final String id_parent = "id_parent";
	private static final String first_name_parent = "first_name_parent";
	private static final String surname_parent = "surname_parent";
	private static final String birthday_parent = "birthday_parent";
	private static final String address_parent = "address_parent";
	private static final String phone_parent = "phone_parent";
	private static final String mail_parent = "mail_parent";

	private static final String id_mark = "id_mark";
	private static final String name_mark = "name_mark";
	private static final String max_value_mark = "max_value_mark";

	private static final String id_student_mark = "id_student_mark";
	private static final String value_mark = "value_mark";

	/**
	 * Constructor 
	 * 
	 * @param context
	 */
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		if (!db.isReadOnly()) {
			// Enable foreign key constraints
			db.execSQL("PRAGMA foreign_keys=ON;");
			
//			if(!dataExists(TABLE_STUDENT, db)){
//				new InitDb().createExample();
//			}
		}
	}
	
	public boolean dataExists(String tableName, SQLiteDatabase db){
		if(tableName.equals(TABLE_STUDENT) && getStudentsCount(db)>0){
			return true;
		}else{
			return false;
		}
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_STUDENT_TABLE = 
				"CREATE TABLE " + TABLE_STUDENT + "("
						+ id_student + " INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ identity_student + " TEXT,"
						+ name_student + " TEXT,"
						+ surname_student + " TEXT,"
						+ birthday_student + " TEXT,"
						+ address_student + " TEXT,"
						+ phone_student + " TEXT,"
						+ mail_student + " TEXT,"
						+ picture_path_student + " TEXT)";
		//+ " FOREIGN KEY(" + id_class_student + ") REFERENCES nameTable(nameKey))";
		try{
			db.execSQL(CREATE_STUDENT_TABLE);
		}catch (Exception e) {
			e.printStackTrace();
		}

		String CREATE_CLASS_TABLE = 
				"CREATE TABLE " + TABLE_CLASS + "("
						+ id_class + " INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ name_class + " TEXT,"
						+ level_class + " TEXT,"
						+ entitled_class + " TEXT,"
						+ year_class + " TEXT)";
		try{
			db.execSQL(CREATE_CLASS_TABLE);

		}catch (Exception e) {
			e.printStackTrace();
		}

		String CREATE_CLASS_STUDENT_TABLE = 
				"CREATE TABLE " + TABLE_CLASS_STUDENT + "("
						+ id_student + " INTEGER NOT NULL,"
						+ id_class + " INTEGER NOT NULL,"
						+ " FOREIGN KEY(" + id_student + ") REFERENCES "+ TABLE_STUDENT + "("+ id_student +") ON DELETE CASCADE,"
						+ " FOREIGN KEY(" + id_class + ") REFERENCES "+ TABLE_CLASS + "("+ id_class +")  ON DELETE CASCADE)";
		try{
			db.execSQL(CREATE_CLASS_STUDENT_TABLE);

		}catch (Exception e) {
			e.printStackTrace();
		}

		String CREATE_SCHEDULE_TABLE = 
				"CREATE TABLE " + TABLE_SCHEDULE + "("
						+ id_schedule + " INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ day_schedule + " TEXT,"
						+ begin_schedule + " TEXT,"
						+ end_schedule + " TEXT,"
						+ id_class + " INTEGER NOT NULL,"
						+ checked_schedule + " TEXT DEFAULT 'false',"
						+ " FOREIGN KEY(" + id_class + ") REFERENCES "+ TABLE_CLASS + "("+ id_class +")  ON DELETE CASCADE)";
		try{
			db.execSQL(CREATE_SCHEDULE_TABLE);
		}catch (Exception e) {
			e.printStackTrace();
		}

		String CREATE_ABSENCE_TABLE = 
				"CREATE TABLE " + TABLE_ABSENCE + "("
						+ id_absence + " INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ justified_absence + " TEXT DEFAULT 'false' NOT NULL,"
						+ reason_absence + " TEXT,"
						+ id_student + " INTEGER NOT NULL,"
						+ id_schedule + " INTEGER NOT NULL,"
						+ " FOREIGN KEY(" + id_student + ") REFERENCES "+ TABLE_STUDENT + "("+ id_student +")  ON DELETE CASCADE,"
						+ " FOREIGN KEY(" + id_schedule + ") REFERENCES "+ TABLE_SCHEDULE + "("+ id_schedule +")  ON DELETE CASCADE);";
		try{
			db.execSQL(CREATE_ABSENCE_TABLE);
		}catch (Exception e) {
			e.printStackTrace();
		}

		String CREATE_PARENT_TABLE = 
				"CREATE TABLE " + TABLE_PARENT + "("
						+ id_parent + " INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ first_name_parent + " TEXT,"
						+ surname_parent + " TEXT,"
						+ birthday_parent + " TEXT,"
						+ address_parent + " TEXT,"
						+ phone_parent + " TEXT,"
						+ mail_parent + " TEXT)";
		try{
			db.execSQL(CREATE_PARENT_TABLE);
		}catch (Exception e) {
			e.printStackTrace();
		}

		String CREATE_PARENT_STUDENT_TABLE = 
				"CREATE TABLE " + TABLE_PARENT_STUDENT + "("
						+ id_parent + " INTEGER NOT NULL,"
						+ id_student + " INTEGER NOT NULL,"
						+ " FOREIGN KEY(" + id_parent + ") REFERENCES "+ TABLE_PARENT + "("+ id_parent +")  ON DELETE CASCADE,"
						+ " FOREIGN KEY(" + id_student + ") REFERENCES "+ TABLE_STUDENT + "("+ id_student +")  ON DELETE CASCADE)";

		try{
			db.execSQL(CREATE_PARENT_STUDENT_TABLE);
		}catch (Exception e) {
			e.printStackTrace();
		}

		String CREATE_MARK_TABLE = 
				"CREATE TABLE " + TABLE_MARK + "("
						+ id_mark + " INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ name_mark + " TEXT,"
						+ max_value_mark + " TEXT,"
						+ id_class + " INTEGER NOT NULL,"
						+ " FOREIGN KEY(" + id_class + ") REFERENCES "+ TABLE_CLASS + "("+ id_class +")  ON DELETE CASCADE)";

		try{
			db.execSQL(CREATE_MARK_TABLE);
		}catch (Exception e) {
			e.printStackTrace();
		}

		String CREATE_STUDENT_MARK_TABLE = 
				"CREATE TABLE " + TABLE_STUDENT_MARK + "("
						+ id_student_mark + " INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ value_mark + " TEXT,"
						+ id_mark + " INTEGER NOT NULL,"
						+ id_student + " INTEGER NOT NULL,"
						+ " FOREIGN KEY(" + id_mark + ") REFERENCES "+ TABLE_MARK + "("+ id_mark +")  ON DELETE CASCADE,"
						+ " FOREIGN KEY(" + id_student + ") REFERENCES "+ TABLE_STUDENT + "("+ id_student +")  ON DELETE CASCADE)";

		try{
			db.execSQL(CREATE_STUDENT_MARK_TABLE);
		}catch (Exception e) {
			e.printStackTrace();
		}

		//Create trigger
		/*try{
        	//Delete absence when delete a schedule
        	db.execSQL("CREATE TRIGGER delete_absence_with_schedule BEFORE DELETE ON " + TABLE_SCHEDULE + " "
     		       +  "FOR EACH ROW BEGIN"
     		       +         " DELETE FROM " + TABLE_ABSENCE + " WHERE " + TABLE_SCHEDULE + "." + id_schedule + " = " + TABLE_ABSENCE + "." + id_schedule + "; "
     		       +  "END");
        	//Delete schedules when delete a classroom
        	db.execSQL("CREATE TRIGGER delete_schedule_with BEFORE DELETE ON " + TABLE_CLASS + " "
     		       +  "FOR EACH ROW BEGIN"
     		       +         " DELETE FROM  " + TABLE_SCHEDULE + " WHERE " + TABLE_CLASS + "." + id_class + "= " + TABLE_SCHEDULE + "." + id_class + "; "
     		       +  "END");
        	//Delete absences when delete a student
        	db.execSQL("CREATE TRIGGER delete_absences_with_student BEFORE DELETE ON " + TABLE_STUDENT + " "
      		       +  "FOR EACH ROW BEGIN"
      		       +         " DELETE FROM  " + TABLE_ABSENCE + " WHERE " + TABLE_STUDENT + "." + id_student + "= " + TABLE_ABSENCE + "." + id_student + "; "
      		       +  "END");
        	//Delete student relation with parent
        	db.execSQL("CREATE TRIGGER delete_student_parent_with BEFORE DELETE ON " + TABLE_STUDENT + " "
       		       +  "FOR EACH ROW BEGIN"
       		       +         " DELETE FROM  " + TABLE_PARENT_STUDENT + " WHERE " + TABLE_STUDENT + "." + id_student + "= " + TABLE_PARENT_STUDENT + "." + id_student + "; "
       		       +  "END");
        	//Delete parent relation with student
        	db.execSQL("CREATE TRIGGER delete_parent_student_with BEFORE DELETE ON " + TABLE_PARENT + " "
       		       +  "FOR EACH ROW BEGIN"
       		       +         " DELETE FROM  " + TABLE_PARENT_STUDENT + " WHERE " + TABLE_PARENT + "." + id_parent + "= " + TABLE_PARENT_STUDENT + "." + id_parent + "; "
       		       +  "END");
        	//Delete classroom relation with student
        	db.execSQL("CREATE TRIGGER delete_class_student_with BEFORE DELETE ON " + TABLE_CLASS + " "
       		       +  "FOR EACH ROW BEGIN"
       		       +         " DELETE FROM  " + TABLE_CLASS_STUDENT + " WHERE " + TABLE_CLASS + "." + id_class + "= " + TABLE_CLASS_STUDENT + "." + id_class + "; "
       		       +  "END");
        	//Delete student relation with classroom
        	db.execSQL("CREATE TRIGGER delete_student_class_with BEFORE DELETE ON " + TABLE_STUDENT + " "
       		       +  "FOR EACH ROW BEGIN"
       		       +         " DELETE FROM  " + TABLE_PARENT_STUDENT + " WHERE " + TABLE_STUDENT + "." + id_student + "= " + TABLE_PARENT_STUDENT + "." + id_student + "; "
       		       +  "END");

        	db.close();
        }catch (Exception e) {
			e.printStackTrace();
		}*/
		
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		//Creating a list of all database table name
		ArrayList<String> tables = new ArrayList<String>();
		tables.add(TABLE_STUDENT);
		tables.add(TABLE_CLASS_STUDENT);
		tables.add(TABLE_CLASS);
		tables.add(TABLE_SCHEDULE);
		tables.add(TABLE_ABSENCE);
		tables.add(TABLE_PARENT);
		tables.add(TABLE_PARENT_STUDENT);
		tables.add(TABLE_MARK);
		tables.add(TABLE_STUDENT_MARK);
		
		//Map to store data of database
		HashMap<String, ArrayList<Object>> data = new HashMap<String, ArrayList<Object>>();
		
		//Insert all different table name of database
		for (String str : tables) {
			data.put(str, new ArrayList<Object>());
		}
		
		for (String str : tables) {
			if(str.equals(TABLE_STUDENT) && !tablesModified.contains(TABLE_STUDENT)){
				data.get(TABLE_STUDENT).add(new ArrayList<Object>());
				for (Student s : getAllStudent()) {
					data.get(TABLE_STUDENT).add(s);
				}
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENT);
			}else if(str.equals(TABLE_CLASS_STUDENT) && !tablesModified.contains(TABLE_CLASS_STUDENT)){
				data.get(TABLE_CLASS_STUDENT).add(new ArrayList<Object>());
				for (ClassroomStudent cs : getAllClassroomStudentByObject()) {
					data.get(TABLE_CLASS_STUDENT).add(cs);
				}
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLASS_STUDENT);
			}else if(str.equals(TABLE_CLASS) && !tablesModified.contains(TABLE_CLASS)){
				data.get(TABLE_CLASS).add(new ArrayList<Object>());
				for (Classroom c : getAllClassroom()) {
					data.get(TABLE_CLASS).add(c);
				}
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLASS);
			}else if(str.equals(TABLE_SCHEDULE)){
				
			}else if(str.equals(TABLE_ABSENCE)){
				
			}else if(str.equals(TABLE_PARENT)){
				
			}else if(str.equals(TABLE_PARENT_STUDENT)){
				
			}else if(str.equals(TABLE_MARK)){
				
			}else if(str.equals(TABLE_STUDENT_MARK)){
				
			}
		}
		
		// Drop older table if existed
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENT);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLASS_STUDENT);
//		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLASS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ABSENCE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARENT_STUDENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MARK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENT_MARK);

		// Create tables again
		onCreate(db);
		
		HashMap<Integer, Integer> oldIdClassroom = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> oldIdStudent = new HashMap<Integer, Integer>();
		
		//Insert data without link
		for (Entry<String, ArrayList<Object>> ar : data.entrySet()) {
			for (Object object : ar.getValue()) {
				if(object instanceof Student){
					
					int idTmp = addStudent((Student) object);
					oldIdStudent.put(((Student) object).getId(), idTmp);
					
				}else if(object instanceof Classroom){
					
					int idTmp = addClassroom((Classroom) object);
					oldIdClassroom.put(((Classroom) object).getId(), idTmp);
					
				}
			}
		}
		
		//Create link for data
		for (Object object : data.get(TABLE_CLASS_STUDENT)) {
			addClass_Student(oldIdClassroom.get(((ClassroomStudent) object).getId_classroom()), oldIdStudent.get(((ClassroomStudent) object).getId_student()));
		}
	}
	
	public void deleteAllData(){
		//Creating a list of all database table name
		ArrayList<String> tables = new ArrayList<String>();
		tables.add(TABLE_STUDENT);
		tables.add(TABLE_CLASS_STUDENT);
		tables.add(TABLE_CLASS);
		tables.add(TABLE_SCHEDULE);
		tables.add(TABLE_ABSENCE);
		tables.add(TABLE_PARENT);
		tables.add(TABLE_PARENT_STUDENT);
		tables.add(TABLE_MARK);
		tables.add(TABLE_STUDENT_MARK);
		
		try{
			for (String table : tables) {
				SQLiteDatabase db = this.getWritableDatabase();
				db.delete(table, null, null);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
	}

	// Adding new student
	public int addStudent(Student student) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(identity_student, student.getIdentity());
			values.put(name_student, student.getName());
			values.put(surname_student, student.getSurname());
			values.put(birthday_student, student.getBirthday());
			values.put(address_student, student.getAddress());
			values.put(phone_student, student.getPhone());
			values.put(mail_student, student.getMail());
			values.put(picture_path_student, student.getPicture_path());

			// Inserting Row
			result = (int) db.insert(TABLE_STUDENT, null, values);
			db.close(); // Closing database connection
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// Adding new mark
	public int addMark(Mark mark) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(name_mark, mark.getName());
			values.put(max_value_mark, mark.getMax_value());
			values.put(id_class, mark.getClassroom().getId());

			// Inserting Row
			result = (int) db.insert(TABLE_MARK, null, values);
			db.close(); // Closing database connection
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// Adding new student mark
	public int addStudentMark(StudentMark studentMark) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(value_mark, studentMark.getvalue());
			values.put(id_mark, studentMark.getMark().getId());
			values.put(id_student, studentMark.getStudent().getId());

			// Inserting Row
			result = (int) db.insert(TABLE_STUDENT_MARK, null, values);
			db.close(); // Closing database connection
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// Adding new classroom
	public int addClassroom(Classroom classroom) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(name_class, classroom.getName());
			values.put(level_class, classroom.getLevel());
			values.put(entitled_class, classroom.getEntitled());
			values.put(year_class, classroom.getYear());

			// Inserting Row
			result = (int) db.insert(TABLE_CLASS, null, values);
			db.close(); // Closing database connection
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int addParent_Student(int id_p, int id_s) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(id_parent, id_p);
			values.put(id_student, id_s);

			// Inserting Row
			result = (int) db.insert(TABLE_PARENT_STUDENT, null, values);
			db.close(); // Closing database connection
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


	public int addParent(Parent parent) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(first_name_parent, parent.getFirstName());
			values.put(surname_parent, parent.getSurname());
			values.put(birthday_parent, parent.getBirthday());
			values.put(address_parent, parent.getAddress());
			values.put(phone_parent, parent.getPhone());
			values.put(mail_parent, parent.getMail());

			// Inserting Row
			result = (int) db.insert(TABLE_PARENT, null, values);
			db.close(); // Closing database connection
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int addSchedule(Schedule schedule) {
		int result = -1;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(day_schedule, dateFormat.format(schedule.getDay()));
			values.put(begin_schedule, schedule.getBegin());
			values.put(end_schedule, schedule.getEnd());
			values.put(id_class_schedule, schedule.getClassroom().getId());
			values.put(checked_schedule, String.valueOf(schedule.isChecked()));
			// Inserting Row
			result = (int) db.insert(TABLE_SCHEDULE, null, values);
			db.close(); // Closing database connection
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int addAbsence(Absence absence) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(justified_absence, String.valueOf(absence.getJustified()));
			values.put(reason_absence, absence.getReason());
			values.put(id_student, absence.getStudent().getId());
			values.put(id_schedule, absence.getSchedule().getId());

			// Inserting Row
			result = (int) db.insert(TABLE_ABSENCE, null, values);
			db.close(); // Closing database connection
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int addClass_Student(int id_c, int id_s) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(id_class, id_c);
			values.put(id_student, id_s);

			// Inserting Row
			result = (int) db.insert(TABLE_CLASS_STUDENT, null, values);
			db.close(); // Closing database connection
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


	// Getting single contact
	public Student getStudent(int id) {
		Student student = null;
		try{
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.query(TABLE_STUDENT, new String[] { 
					id_student,
					name_student, 
					surname_student, 
					birthday_student, 
					address_student, 
					phone_student, 
					mail_student,
					picture_path_student}, id_student + "=?",
							new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			student = new Student(
					Integer.parseInt(cursor.getString(0)),
					cursor.getString(1), 
					cursor.getString(2), 
					cursor.getString(3), 
					cursor.getString(4), 
					cursor.getString(5), 
					cursor.getString(6), 
					cursor.getString(7), 
					cursor.getString(8));
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return student;
	}

	// Getting classroom
	public Classroom getClassroom(int id) {
		Classroom classroom = null;
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_CLASS + " WHERE " + TABLE_CLASS + "." + id_class + "=?";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(id) });

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				classroom = new Classroom(/*context,*/
						Integer.parseInt(cursor.getString(0)),
						cursor.getString(1), 
						cursor.getString(2), 
						cursor.getString(3),
						cursor.getString(4));
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return classroom list
		return classroom;
	}

	public Mark getMark(int id) {
		Mark mark = null;
		try{
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.query(TABLE_MARK, new String[] { 
					id_mark,
					name_mark, 
					max_value_mark,
					id_class}, id_mark + "=?",
							new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			mark = new Mark(
					Integer.parseInt(cursor.getString(0)),
					cursor.getString(1), 
					cursor.getString(2), 
					getClassroom(Integer.parseInt(cursor.getString(3)))
					);
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mark;
	}
	
	// Getting single absence
	public Absence getAbsence(int id) {
		Absence absence = null;
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_ABSENCE + " WHERE " + TABLE_ABSENCE + "." + id_absence + "=?";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(id) });

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				absence = new Absence(
						Integer.parseInt(cursor.getString(0)),
						Boolean.valueOf(cursor.getString(1)), 
						cursor.getString(2),
						getStudent(Integer.parseInt(cursor.getString(3))),
						getSchedule(Integer.parseInt(cursor.getString(4)))
						);
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return absence
		return absence;
	}
	
	// Getting single schedule
		public Schedule getSchedule(int id) {
			Schedule schedule = null;
			try{
				// Select All Query
				String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE + " WHERE " + TABLE_SCHEDULE + "." + id_schedule + "=?";
				
				DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
				
				SQLiteDatabase db = this.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(id) });

				if (cursor.moveToFirst()) {
					Date a = dfm.parse(cursor.getString(1));
					schedule = new Schedule(
							Integer.parseInt(cursor.getString(0)),
							a, 
							cursor.getString(2), 
							cursor.getString(3),
							getClassroom(Integer.parseInt(cursor.getString(4))),
							Boolean.valueOf(cursor.getString(5)));
				}
				cursor.close();
				db.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
			// return absence
			return schedule;
		}

	public StudentMark getStudentMark(int id) {
		StudentMark mark = null;
		try{
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.query(TABLE_STUDENT_MARK, new String[] { 
					id_student_mark,
					value_mark, 
					id_mark,
					id_student}, id_student_mark + "=?",
							new String[] { String.valueOf(id) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			mark = new StudentMark(
					Integer.parseInt(cursor.getString(0)),
					cursor.getString(1), 
					getMark(Integer.parseInt(cursor.getString(2))), 
					getStudent(Integer.parseInt(cursor.getString(3)))
					);
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mark;
	}
	
	public StudentMark getStudentMark(int id_m,int id_s) {
		StudentMark mark = null;
		try{
			SQLiteDatabase db = this.getReadableDatabase();

			Cursor cursor = db.query(TABLE_STUDENT_MARK, new String[] { 
					id_student_mark,
					value_mark, 
					id_mark,
					id_student}, id_student + "=? AND " + id_mark + "=?",
							new String[] { String.valueOf(id_s),String.valueOf(id_m) }, null, null, null, null);
			if (cursor != null)
				cursor.moveToFirst();

			mark = new StudentMark(
					Integer.parseInt(cursor.getString(0)),
					cursor.getString(1), 
					getMark(Integer.parseInt(cursor.getString(2))), 
					getStudent(Integer.parseInt(cursor.getString(3)))
					);
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return mark;
	}

	// Getting schedule of absence
	public Schedule getSchedule(Absence absence) {
		Schedule schedule = null;
		try{
			SQLiteDatabase db = this.getReadableDatabase();
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");

			String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE + ", "+ TABLE_ABSENCE
					+ " WHERE " + TABLE_SCHEDULE + "." + id_schedule + " = "+ TABLE_ABSENCE + "." + id_schedule
					+ " AND " + TABLE_ABSENCE + "." + id_absence + " =?";
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(absence.getId()) });
			if (cursor != null)
				cursor.moveToFirst();

			schedule = new Schedule(
					Integer.parseInt(cursor.getString(0)),
					dfm.parse(cursor.getString(1)), 
					cursor.getString(2), 
					cursor.getString(3),
					getClassroom(Integer.parseInt(cursor.getString(4))),
					Boolean.valueOf(cursor.getString(5)));
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return schedule;
	}

	// Getting schedule of absence
	public Absence getAbsence(Schedule schedule, Student student) {
		Absence absence = null;
		try{
			SQLiteDatabase db = this.getReadableDatabase();

			String selectQuery = "SELECT * FROM " + TABLE_ABSENCE
					+ " WHERE " + TABLE_ABSENCE + "." + id_schedule + " =?"
					+ " AND " + TABLE_ABSENCE + "." + id_student + " =?";
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(schedule.getId()),String.valueOf(student.getId()) });
			if (cursor != null){
				cursor.moveToFirst();

//				absence = new Absence(
//						Integer.parseInt(cursor.getString(0)),
//						Boolean.valueOf(cursor.getString(1)), 
//						cursor.getString(2),
//						Integer.parseInt(cursor.getString(3)),
//						Integer.parseInt(cursor.getString(4))
//						);
				absence = new Absence(
						Integer.parseInt(cursor.getString(0)),
						Boolean.valueOf(cursor.getString(1)), 
						cursor.getString(2),
						student,
						schedule
						);
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			//e.printStackTrace();
		}
		return absence;
	}

	// Getting All Students
	public ArrayList<Student> getAllStudent() {
		ArrayList<Student> studentArrayList = new ArrayList<Student>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_STUDENT +" order by "+surname_student+ ","+name_student;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Student student = new Student(/*context,*/
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							cursor.getString(2), 
							cursor.getString(3), 
							cursor.getString(4), 
							cursor.getString(5), 
							cursor.getString(6), 
							cursor.getString(7), 
							cursor.getString(8));
					studentArrayList.add(student);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return contact list
		return studentArrayList;
	}

	// Getting All Marks
	public ArrayList<Mark> getAllMark() {
		ArrayList<Mark> markArrayList = new ArrayList<Mark>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_MARK +" order by "+id_mark + " DESC";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Mark mark = new Mark(/*context,*/
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							cursor.getString(2),
							getClassroom(Integer.parseInt(cursor.getString(3))));
					markArrayList.add(mark);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return contact list
		return markArrayList;
	}
	
	// Getting All Marks
	public ArrayList<ClassroomStudent> getAllClassroomStudentByObject() {
		ArrayList<ClassroomStudent> classroomStudentArrayList = new ArrayList<ClassroomStudent>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_CLASS_STUDENT +" order by "+ id_class + " DESC";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					ClassroomStudent classroomStudent = new ClassroomStudent(/*context,*/
							Integer.parseInt(cursor.getString(0)),
							Integer.parseInt(cursor.getString(1)));
					classroomStudentArrayList.add(classroomStudent);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return contact list
		return classroomStudentArrayList;
	}

	// Getting All Marks
	public ArrayList<StudentMark> getAllStudentMark() {
		ArrayList<StudentMark> markArrayList = new ArrayList<StudentMark>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_STUDENT_MARK;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					StudentMark mark = new StudentMark(/*context,*/
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							getMark(Integer.parseInt(cursor.getString(2))),
							getStudent(Integer.parseInt(cursor.getString(3))));
					markArrayList.add(mark);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return contact list
		return markArrayList;
	}

	// Getting All Students of a parent
	public ArrayList<Student> getAllStudent(Parent parent) {
		ArrayList<Student> studentArrayList = new ArrayList<Student>();
		try{
			String selectQuery = "SELECT * "
					+ " FROM " + TABLE_STUDENT + "," + TABLE_PARENT + "," + TABLE_PARENT_STUDENT 
					+ " WHERE " + TABLE_PARENT + "." + id_parent + " = "+ TABLE_PARENT_STUDENT + "." + id_parent
					+ " AND " + TABLE_STUDENT + "." + id_student + " = " + TABLE_PARENT_STUDENT + "." + id_student
					+ " AND " + TABLE_PARENT + "." + id_parent + " =?";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(parent.getId()) });

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Student student = new Student(/*context,*/
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							cursor.getString(2), 
							cursor.getString(3), 
							cursor.getString(4), 
							cursor.getString(5), 
							cursor.getString(6), 
							cursor.getString(7), 
							cursor.getString(8));
					studentArrayList.add(student);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return student list
		return studentArrayList;
	}

	// Getting All Students of a classroom
	public ArrayList<Student> getAllStudent(Classroom classroom) {
		ArrayList<Student> studentArrayList = new ArrayList<Student>();
		try{
			String selectQuery = "SELECT * "
					+ " FROM " + TABLE_STUDENT + "," + TABLE_CLASS + "," + TABLE_CLASS_STUDENT 
					+ " WHERE " + TABLE_CLASS + "." + id_class + " = "+ TABLE_CLASS_STUDENT + "." + id_class
					+ " AND " + TABLE_STUDENT + "." + id_student + " = " + TABLE_CLASS_STUDENT + "." + id_student
					+ " AND " + TABLE_CLASS + "." + id_class + " =?"
					+" order by "+TABLE_STUDENT+"."+surname_student+ ","+TABLE_STUDENT+"."+name_student;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(classroom.getId()) });

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Student student = new Student(/*context,*/
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							cursor.getString(2), 
							cursor.getString(3), 
							cursor.getString(4), 
							cursor.getString(5), 
							cursor.getString(6), 
							cursor.getString(7), 
							cursor.getString(8));
					studentArrayList.add(student);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return student list
		return studentArrayList;
	}

	// Getting All Students not in classroom
	public ArrayList<Student> getAllStudentNotIn(Classroom classroom) {
		ArrayList<Student> studentArrayList = new ArrayList<Student>();
		try{
			/*String selectQuery = "SELECT * "
					+ " FROM " + TABLE_STUDENT + "," + TABLE_CLASS + "," + TABLE_CLASS_STUDENT 
					+ " WHERE " + TABLE_CLASS + "." + id_class + " = "+ TABLE_CLASS_STUDENT + "." + id_class
					+ " AND " + TABLE_STUDENT + "." + id_student + " = " + TABLE_CLASS_STUDENT + "." + id_student
					+ " AND " + TABLE_CLASS + "." + id_class + " !=?";*/
			
			//Cens� fonctionner mais apparemment le Left Join ne fonctionne pas quand on met en plus un WHERE en SQLite, un bug quoi
			String selectQuery = "SELECT * " +
					" FROM " + 
					TABLE_STUDENT + " LEFT OUTER JOIN " + TABLE_CLASS_STUDENT + " ON " + TABLE_STUDENT + "." + id_student + "=" + TABLE_CLASS_STUDENT + "." + id_student +
					" WHERE " + TABLE_CLASS + "." + id_class + "!=?";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(classroom.getId()) });

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Student student = new Student(/*context,*/
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							cursor.getString(2), 
							cursor.getString(3), 
							cursor.getString(4), 
							cursor.getString(5), 
							cursor.getString(6), 
							cursor.getString(7), 
							cursor.getString(8));
					studentArrayList.add(student);
				} while (cursor.moveToNext());
			}
			
			
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return student list
		return studentArrayList;
	}

	// Getting All Students of a classroom
	public ArrayList<Student> getAllStudentAbsentBySchedule(Classroom classroom, Schedule schedule) {
		ArrayList<Student> studentArrayList = new ArrayList<Student>();
		try{
			String selectQuery = "SELECT * "
					+ " FROM " + TABLE_STUDENT + "," + TABLE_CLASS + "," + TABLE_CLASS_STUDENT + "," + TABLE_SCHEDULE + "," + TABLE_ABSENCE

					+ " WHERE " + TABLE_CLASS + "." + id_class + " = "+ TABLE_CLASS_STUDENT + "." + id_class
					+ " AND " + TABLE_STUDENT + "." + id_student + " = " + TABLE_CLASS_STUDENT + "." + id_student

					+ " AND " + TABLE_SCHEDULE + "." + id_class + " = " + TABLE_CLASS + "." + id_class

					+ " AND " + TABLE_ABSENCE + "." + id_student + " = " + TABLE_STUDENT + "." + id_student
					+ " AND " + TABLE_ABSENCE + "." + id_schedule + " = " + TABLE_SCHEDULE + "." + id_schedule

					+ " AND " + TABLE_CLASS + "." + id_class + " =?"
					+ " AND " + TABLE_SCHEDULE + "." + id_schedule + " =?";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(classroom.getId()), String.valueOf(schedule.getId()) });

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Student student = new Student(/*context,*/
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							cursor.getString(2), 
							cursor.getString(3), 
							cursor.getString(4), 
							cursor.getString(5), 
							cursor.getString(6), 
							cursor.getString(7), 
							cursor.getString(8));
					studentArrayList.add(student);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return student list
		return studentArrayList;
	}

	// Getting All Parents
	public ArrayList<Parent> getAllParent() {
		ArrayList<Parent> parentArrayList = new ArrayList<Parent>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_PARENT;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Parent parent = new Parent(
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							cursor.getString(2), 
							cursor.getString(3), 
							cursor.getString(4), 
							cursor.getString(5), 
							cursor.getString(6));
					parentArrayList.add(parent);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return parent list
		return parentArrayList;
	}

	// Getting All Parent of a student
	public ArrayList<Parent> getAllParent(Student student) {
		ArrayList<Parent> parentArrayList = new ArrayList<Parent>();
		try{
			// Select Query
			String selectQuery = "SELECT " + TABLE_PARENT +"."+id_parent +","+ TABLE_PARENT + "." + first_name_parent +","+ TABLE_PARENT + "." + surname_parent +","+ TABLE_PARENT + "." + birthday_parent +","+ TABLE_PARENT + "." + address_parent +","+ TABLE_PARENT + "." + phone_parent +","+ TABLE_PARENT + "." + mail_parent
					+ " FROM " + TABLE_STUDENT + "," + TABLE_PARENT + "," + TABLE_PARENT_STUDENT 
					+ " WHERE " + TABLE_PARENT + "." + id_parent + " = "+ TABLE_PARENT_STUDENT + "." + id_parent
					+ " AND " + TABLE_STUDENT + "." + id_student + " = " + TABLE_PARENT_STUDENT + "." + id_student
					+ " AND " + TABLE_STUDENT + "." + id_student + " =?";
					

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(student.getId()) });

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Parent parent = new Parent(
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							cursor.getString(2), 
							cursor.getString(3), 
							cursor.getString(4), 
							cursor.getString(5), 
							cursor.getString(6));
					parentArrayList.add(parent);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return parent list
		return parentArrayList;
	}

	//Get all absences of a student
	public ArrayList<Absence> getAllAbsence(Student student) {
		ArrayList<Absence> absenceArrayList = new ArrayList<Absence>();
		try{
			String selectQuery = "SELECT * "
					+ " FROM " + TABLE_ABSENCE 
					+ " WHERE " + TABLE_ABSENCE + "." + id_student + " =?"
					+ " order by "+id_absence+" DESC";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, new String[] {String.valueOf(student.getId())});

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
//					Absence absence = new Absence(
//							Integer.parseInt(cursor.getString(0)),
//							Boolean.valueOf(cursor.getString(1)), 
//							cursor.getString(2),
//							Integer.parseInt(cursor.getString(3)),
//							Integer.parseInt(cursor.getString(4)));
					Absence absence = new Absence(
							Integer.parseInt(cursor.getString(0)),
							Boolean.valueOf(cursor.getString(1)), 
							cursor.getString(2),
							student,
							getSchedule(Integer.parseInt(cursor.getString(4)))
							);
					absenceArrayList.add(absence);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return parent list
		return absenceArrayList;
	}

	// Getting All Absences
	public ArrayList<Absence> getAllAbsence() {
		ArrayList<Absence> absenceArrayList = new ArrayList<Absence>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_ABSENCE;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Absence absence = new Absence(
							Integer.parseInt(cursor.getString(0)),
							Boolean.valueOf(cursor.getString(1)), 
							cursor.getString(2),
							getStudent(Integer.parseInt(cursor.getString(3))),
							getSchedule(Integer.parseInt(cursor.getString(4))));
					absenceArrayList.add(absence);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return parent list
		return absenceArrayList;
	}

	// Getting All Schedules
	public ArrayList<Schedule> getAllSchedule() {
		ArrayList<Schedule> scheduleArrayList = new ArrayList<Schedule>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
					Date a = dfm.parse(cursor.getString(1));

					Schedule schedule = new Schedule(
							Integer.parseInt(cursor.getString(0)),
							a, 
							cursor.getString(2), 
							cursor.getString(3),
							getClassroom(Integer.parseInt(cursor.getString(4))),
							Boolean.valueOf(cursor.getString(5)));
					scheduleArrayList.add(schedule);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return parent list
		return scheduleArrayList;
	}

	// Getting All Schedules
	@SuppressWarnings("deprecation")
	public ArrayList<Schedule> getAllSchedule(Classroom classroom) {
		ArrayList<Schedule> scheduleArrayList = new ArrayList<Schedule>();
		try{
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE + " WHERE " + id_class + " =?" + " and " + day_schedule + "=?"
					+ " order by "+id_schedule+" DESC";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(classroom.getId()), dfm.format(new Date()) });

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					
					Date a = dfm.parse(cursor.getString(1));
					Date b = new Date();
					if(a.getDay() == b.getDay()){
						Schedule schedule = new Schedule(
								Integer.parseInt(cursor.getString(0)),
								a, 
								cursor.getString(2), 
								cursor.getString(3),
								getClassroom(Integer.parseInt(cursor.getString(4))),
								Boolean.valueOf(cursor.getString(5)));
						scheduleArrayList.add(schedule);
					}
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return schedule list
		return scheduleArrayList;
	}

	// Getting students Count
	public int getStudentsCount(SQLiteDatabase db) {
		String countQuery = "SELECT * FROM " + TABLE_STUDENT;
//		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();
		// return count
		return cursor.getCount();
	}

	// Getting All classroom
	public ArrayList<Classroom> getAllClassroom() {
		ArrayList<Classroom> classroomArrayList = new ArrayList<Classroom>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_CLASS
					+ " order by "+id_class+" DESC";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Classroom classroom = new Classroom(/*context,*/
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							cursor.getString(2), 
							cursor.getString(3),
							cursor.getString(4));
					classroomArrayList.add(classroom);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return classroom list
		return classroomArrayList;
	}

	// Getting All Parents_Student
	public ArrayList<String> getAllParent_Student() {
		ArrayList<String> parentArrayList = new ArrayList<String>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_PARENT_STUDENT;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					parentArrayList.add(cursor.getString(0));
					parentArrayList.add(cursor.getString(1));
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return parent list
		return parentArrayList;
	}

	// Getting All Class_Student
	public ArrayList<String> getAllClassroom_Student() {
		ArrayList<String> parentArrayList = new ArrayList<String>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_CLASS_STUDENT;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					parentArrayList.add(cursor.getString(0));
					parentArrayList.add(cursor.getString(1));
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return parent list
		return parentArrayList;
	}

	public ArrayList<Mark> getAllMark(Classroom classroom) {
		ArrayList<Mark> markArrayList = new ArrayList<Mark>();
		try{
			// Select Query
			String selectQuery = "SELECT * "
					+ " FROM " + TABLE_MARK
					+ " WHERE " + TABLE_MARK + "." + id_class + " =?"
					+ " order by "+id_mark+" DESC";

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(classroom.getId()) });

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					Mark mark = new Mark(
							Integer.parseInt(cursor.getString(0)),
							cursor.getString(1), 
							cursor.getString(2),
							getClassroom(Integer.parseInt(cursor.getString(3))));
					markArrayList.add(mark);
				} while (cursor.moveToNext());
			}
			cursor.close();
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}

		return markArrayList;
	}
	

	public ArrayList<StudentMark> getAllStudentMark(Student student) {
		ArrayList<StudentMark> markArrayList = new ArrayList<StudentMark>();
		try{
			// Select All Query
			String selectQuery = "SELECT * FROM " + TABLE_STUDENT_MARK
					+ " WHERE " + TABLE_STUDENT_MARK + "." + id_student + " =?"
					+ " order by "+id_student_mark+" DESC";

					SQLiteDatabase db = this.getWritableDatabase();
					Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(student.getId())});

					// looping through all rows and adding to list
					if (cursor.moveToFirst()) {
						do {
							StudentMark mark = new StudentMark(/*context,*/
									Integer.parseInt(cursor.getString(0)),
									cursor.getString(1), 
									getMark(Integer.parseInt(cursor.getString(2))),
									getStudent(Integer.parseInt(cursor.getString(3))));
							markArrayList.add(mark);
						} while (cursor.moveToNext());
					}
					cursor.close();
					db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		// return contact list
		return markArrayList;
	}
	/*
    public ArrayList<StudentMark> getAllMark(Student student, Classroom classroom) {
    	ArrayList<Mark> markArrayList = new ArrayList<Mark>();
        try{
        	// Select Query
        	String selectQuery = "SELECT * "
            		+ " FROM " + TABLE_STUDENT + "," + TABLE_CLASS + "," + TABLE_MARK 
            		+ " WHERE " + TABLE_MARK + "." + id_mark + " = "+ TABLE_CLASS + "." + id_class
            		+ " AND " + TABLE_MARK + "." + id_student + " = " + TABLE_STUDENT + "." + id_student
            		+ " AND " + TABLE_STUDENT + "." + id_student + " =?"
            		+ " AND " + TABLE_CLASS + "." + id_class + " =?";

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, new String[] { String.valueOf(student.getId()), String.valueOf(classroom.getId()) });

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                	Mark mark = new Mark(
    	                Integer.parseInt(cursor.getString(0)),
    	                cursor.getString(1), 
    	                cursor.getString(2), 
    	                cursor.getString(3),
    	                Integer.parseInt(cursor.getString(4)),
                        Integer.parseInt(cursor.getString(5)));
                	markArrayList.add(mark);
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();
        }catch (Exception e) {
        	e.printStackTrace();
		}
        // return mark list
        return markArrayList;
	}*/

	// Updating single student
	public int updateStudent(Student student) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(identity_student, student.getIdentity());
			values.put(name_student, student.getName());
			values.put(surname_student, student.getSurname());
			values.put(birthday_student, student.getBirthday());
			values.put(address_student, student.getAddress());
			values.put(phone_student, student.getPhone());
			values.put(mail_student, student.getMail());
			values.put(picture_path_student, student.getPicture_path());

			// updating row
			result = db.update(TABLE_STUDENT, values, id_student + " = ?",
					new String[] { String.valueOf(student.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// Updating single class
	public int updateClassroom(Classroom classroom) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(name_class, classroom.getName());
			values.put(level_class, classroom.getLevel());
			values.put(entitled_class, classroom.getEntitled());
			values.put(year_class, classroom.getYear());

			// updating row
			result = db.update(TABLE_CLASS, values, id_class + " = ?",
					new String[] { String.valueOf(classroom.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int updateSchedule(int id_c, Schedule schedule) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			ContentValues values = new ContentValues();
			values.put(day_schedule, dateFormat.format(schedule.getDay()));
			values.put(begin_schedule, schedule.getBegin());
			values.put(end_schedule, schedule.getEnd());
			values.put(id_class, id_c);

			// updating row
			result = db.update(TABLE_SCHEDULE, values, id_schedule + " = ?",
					new String[] { String.valueOf(schedule.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int updateSchedule(Schedule schedule) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			ContentValues values = new ContentValues();
			values.put(day_schedule, dateFormat.format(schedule.getDay()));
			values.put(begin_schedule, schedule.getBegin());
			values.put(end_schedule, schedule.getEnd());
			values.put(id_class_schedule, schedule.getClassroom().getId());
			values.put(checked_schedule, String.valueOf(schedule.isChecked()));

			// updating row
			result = db.update(TABLE_SCHEDULE, values, id_schedule + " = ?",
					new String[] { String.valueOf(schedule.getId()) });
			db.close();
		}catch (Exception e) {
			
		}
		return result;
	}

	public int updateAbsence(Absence absence) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(justified_absence, String.valueOf(absence.getJustified()));
			values.put(reason_absence, absence.getReason());

			// updating row
			result = db.update(TABLE_ABSENCE, values, id_absence + " = ?",
					new String[] { String.valueOf(absence.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int updateAbsence(int id_stu, Schedule schedule, Absence absence) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(justified_absence, String.valueOf(absence.getJustified()));
			values.put(reason_absence, absence.getReason());
			values.put(id_student, id_stu);
			values.put(id_schedule, schedule.getId());

			// updating row
			result = db.update(TABLE_ABSENCE, values, id_absence + " = ?",
					new String[] { String.valueOf(absence.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int updateParent(Parent parent) {
		int result = -1;
		try{

			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(first_name_parent, parent.getFirstName());
			values.put(surname_parent, parent.getSurname());
			values.put(birthday_parent, parent.getBirthday());
			values.put(address_parent, parent.getAddress());
			values.put(phone_parent, parent.getPhone());
			values.put(mail_parent, parent.getMail());

			// updating row
			result = db.update(TABLE_PARENT, values, id_parent + " = ?",
					new String[] { String.valueOf(parent.getId()) });
			Log.d("tagtag",parent.getId()+"");
			db.close();
		}catch (Exception e) {
			
		}
		return result;
	}

	public int updateMark(Mark mark) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(name_mark, mark.getName());
			values.put(max_value_mark, mark.getMax_value());
			values.put(id_class, mark.getClassroom().getId());

			// updating row
			result = db.update(TABLE_MARK, values, id_mark + " = ?",
					new String[] { String.valueOf(mark.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public int updateMark(int id_c, Mark mark) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(name_mark, mark.getName());
			values.put(max_value_mark, mark.getMax_value());
			values.put(id_class, id_c);

			// updating row
			result = db.update(TABLE_MARK, values, id_mark + " = ?",
					new String[] { String.valueOf(mark.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int updateStudentMark(StudentMark mark) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(value_mark, mark.getvalue());
			values.put(id_mark, mark.getMark().getId());
			values.put(id_student, mark.getStudent().getId());

			// updating row
			result = db.update(TABLE_STUDENT_MARK, values, id_student_mark + " = ?",
					new String[] { String.valueOf(mark.getId_student_mark()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public int updateStudentMark(int id, StudentMark sm, Mark m) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(value_mark, sm.getvalue());
			values.put(id_mark, m.getId());
			values.put(id_student, id);

			// updating row
			result = db.update(TABLE_STUDENT_MARK, values, id_student_mark + " = ?",
					new String[] { String.valueOf(sm.getId_student_mark()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// Deleting single student
	public int deleteStudent(Student student) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_STUDENT, id_student + " = ?",
					new String[] { String.valueOf(student.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// Deleting single parent
	public int deleteParent(Parent parent) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_PARENT, id_parent + " = ?",
					new String[] { String.valueOf(parent.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// Deleting single mark
	public int deleteMark(Mark mark) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_MARK, id_mark + " = ?",
					new String[] { String.valueOf(mark.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// Deleting single mark
	public int deleteStudentMark(StudentMark mark) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_STUDENT_MARK, id_student_mark + " = ?",
					new String[] { String.valueOf(mark.getId_student_mark()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteClass_Student(int id_c, int id_s) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_CLASS_STUDENT, 
					id_class + " = ? AND " + id_student + " = ?",
					new String[] { String.valueOf(id_c), String.valueOf(id_s) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


	public int deleteSchedule(Schedule schedule) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_SCHEDULE, id_schedule + " = ?",
					new String[] { String.valueOf(schedule.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteAbsence(Absence absence) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_ABSENCE, id_absence + " = ?",
					new String[] { String.valueOf(absence.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteClassroom(Classroom classroom) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			db.delete(TABLE_CLASS, id_class + " = ?",
					new String[] { String.valueOf(classroom.getId()) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


	public int deleteParent_Student(int id_p, int id_s) {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			result = db.delete(TABLE_PARENT_STUDENT, 
					id_parent + " = ? AND " + id_student + " = ?",
					new String[] { String.valueOf(id_p), String.valueOf(id_s) });
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteAllStudents() {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			result = db.delete(TABLE_STUDENT, "1", null);
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteAllParents() {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			result = db.delete(TABLE_PARENT, "1", null);
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteAllAbsences() {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			result = db.delete(TABLE_ABSENCE, "1", null);
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteAllClassrooms() {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			result = db.delete(TABLE_CLASS, "1", null);
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int deleteAllSchedules() {
		int result = -1;
		try{
			SQLiteDatabase db = this.getWritableDatabase();
			result = db.delete(TABLE_SCHEDULE, "1", null);
			db.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
