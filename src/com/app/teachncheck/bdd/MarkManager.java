package com.app.teachncheck.bdd;

import java.util.ArrayList;
import java.util.List;

import com.app.teachncheck.MainActivity;

/**
 * 
 * @author Julien
 *
 * Manage marks
 *
 */
public class MarkManager {
	
	private ArrayList<Mark> marks;
	
	/**
	 * Constructor
	 */
	public MarkManager(){
		marks = new ArrayList<Mark>();
		initMarks();
	}

	/**
	 * Load all marks of the database
	 */
	public boolean initMarks(){
		marks = MainActivity.getDb().getAllMark();
		if(marks.size() != 0){
			return true;
		}else{
			return false;
		}		
	}
	
	/**
	 * Reload all marks of the database. Same operation as initMarks
	 */
	public boolean reloadMarks(){
		return initMarks();
	}
	
	public ArrayList<Mark> getMarks() {
		return marks;
	}
}
