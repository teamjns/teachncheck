package com.app.teachncheck.bdd;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.app.teachncheck.MainActivity;

/**
 * 
 * @author Julien
 * 
 * Class to manage absence
 *
 */
public class AbsenceManager {
	
	private ArrayList<Absence> absences;
	
	/**
	 * Constructor
	 */
	public AbsenceManager(){
		absences = new ArrayList<Absence>();
		initAbsences();
	}
	
	/**
	 * Load all classrooms of the database
	 */
	public boolean initAbsences(){
		absences = MainActivity.getDb().getAllAbsence();
		if(absences.size() != 0){
			return true;
		}else{
			return false;
		}		
	}
	
	/**
	 * Reload all classrooms of the database. Same operation as initClassrooms
	 */
	public boolean reloadAbsences(){
		return initAbsences();
	}

	/**
	 * Getter for the list of absences
	 * @return ArrayList<Absence>
	 */
	public ArrayList<Absence> getAbsences() {
		return absences;
	}
}
