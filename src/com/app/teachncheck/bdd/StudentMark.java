package com.app.teachncheck.bdd;

import java.io.Serializable;

import android.util.Log;

import com.app.teachncheck.MainActivity;

/**
 * 
 * @author Julien
 *
 * StudentMark object
 *
 */
public class StudentMark implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id_student_mark;
	private String value;
	private Mark mark;
	private Student student;
	
	/**
	 * Constructor
	 * 
	 * @param id
	 * @param value
	 * @param mark
	 * @param student
	 */
	public StudentMark(int id, String value, Mark mark, Student student) {
		super();
		this.id_student_mark = id;
		this.value = value;
		this.mark = mark;
		this.student = student; 
	}

	/**
	 * Constructor
	 * 
	 * @param value
	 * @param mark
	 * @param student
	 */
	public StudentMark(String value, Mark mark, Student student) {
		super();
		this.id_student_mark = 0;
		this.value = value;
		this.mark = mark;
		this.student = student;
	}
	
	/**
	 * Constructor
	 * 
	 * @param value
	 */
	public StudentMark(String value) {
		super();
		this.id_student_mark = 0;
		this.value = value;
	}
	
	/**
	 * Constructor by copy
	 * 
	 * @param mark
	 */
	private StudentMark(StudentMark mark) {
		super();
		this.value = mark.value;
		this.mark = mark.mark;
		this.student = mark.student;
	}
	
	/**
	 * Create a Mark in database and add the id to the current mark
	 */
	public boolean initStudentMark(){
		this.id_student_mark = (MainActivity.getDb().addStudentMark(new StudentMark(this)));
		if(this.id_student_mark >= 1){
			MainActivity.getDb().close();
			return true;
		}else{
			MainActivity.getDb().close();
			return false;
		}
	}
	
	/**
	 * Save each modification of the mark in database
	 */
	public boolean saveMark(){
		if(this.getId_student_mark() != 0){
			if(MainActivity.getDb().updateStudentMark(this)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In saveMark()", "Can't save a mark that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * delete a mark in database
	 */
	public boolean deleteMark(){
		if(this.getId_student_mark() != 0){
			if(MainActivity.getDb().deleteStudentMark(this)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In deleteMark()", "Can't delete a mark that doesn't exist in database");
			return false;
		}
	}

	public int getId_student_mark() {
		return id_student_mark;
	}

	public void setId_student_mark(int id_student_mark) {
		this.id_student_mark = id_student_mark;
	}

	public String getvalue() {
		return value;
	}

	public void setvalue(String value) {
		this.value = value;
	}

	public Mark getMark() {
		return mark;
	}

	public void setMark(Mark mark) {
		this.mark = mark;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	@Override
	public String toString() {
		Mark mark = getMark();
		return mark.getName() + "\t" +value+" / "+mark.getMax_value();
	}
	
}
