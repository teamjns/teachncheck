package com.app.teachncheck.bdd;

import java.util.Date;

import com.app.teachncheck.MainActivity;

public class InitDb {
	
	public InitDb(){
		
	}
	
	public void createExample(){
		
		//Create classroom
		Classroom c1 = new Classroom("Class1", "1", "a", "2013");
		c1.initClass();
		
		//Create Students
		Student s1 = new Student("1234", "Julien", "COLLE", "24/08/1991", "6 rue de la playa", "0998766534", "julien@tns.com", "");
		s1.initStudent();
		Student s2 = new Student("4321", "Nicolas", "FRIEDERICH", "23/12/1990", "6 rue de la cama", "9876564543", "nicolas@tns.com", "");
		s2.initStudent();
		Student s3 = new Student("1324", "Samuel", "DURAND", "13/04/1984", "6 rue de la alfombra", "7865874356", "samuel@tns.com", "");
		s3.initStudent();
		
		//Create parent
		Parent p1 = new Parent("Cristina", "De LAROYURE", "13/09/1971", "6 rue de la playa", "0998766534", "cristina@tns.com");
		p1.initParent();
		
		//Add parent to student
		p1.addStudentToParent(s1);
		
		//Add students to classroom
		c1.addStudentToClass(s1);
		c1.addStudentToClass(s2);
		c1.addStudentToClass(s3);
		
		//Create schedules
		Schedule sc1 = new Schedule(new Date(), "10:00", "12:00", false);
		sc1.initSchedule();
		Schedule sc2 = new Schedule(new Date(), "14:00", "16:00", false);
		sc2.initSchedule();
		
		//Add Schedules to classroom
		c1.addScheduleToClass(sc1);
		c1.addScheduleToClass(sc2);
		
		//Create absence
		Absence a = new Absence(true, "Malade");
		a.initAbsence();
		
		//Add absence to student
		s1.addAbsenceToStudent(sc1, a);
		
		//Create mark
		Mark m = new Mark("Final", "20");
		m.initMark();
		
		//Add mark to classroom
		c1.addMarkToClass(m);
		
		//Create student mark
		StudentMark sm = new StudentMark("18", m, s1);
		sm.initStudentMark();
		
		//Add mark to student
		s1.addMarkToStudent(sm, m);
				
	}
	
	public void deleteExample(){
		MainActivity.getDb().deleteAllData();
	}
}
