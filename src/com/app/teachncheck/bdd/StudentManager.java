package com.app.teachncheck.bdd;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.app.teachncheck.MainActivity;

/**
 * 
 * @author Julien
 *
 * Manage Students
 *
 */
public class StudentManager {
	
	private List<Student> students;
	
	/**
	 * Constructor
	 */
	public StudentManager(){
		students = new ArrayList<Student>();
		initStudents();
	}
	
	/**
	 * Load all students of the database
	 */
	public boolean initStudents(){
		students = MainActivity.getDb().getAllStudent();
		if(students.size() != 0){
			return true;
		}else{
			return false;
		}		
	}
	
	/**
	 * Reload all students of the database. Same operation as initStudent
	 */
	public boolean reloadStudents(){
		return initStudents();
	}

	public List<Student> getStudents() {
		return students;
	}
}
