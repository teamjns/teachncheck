package com.app.teachncheck.bdd;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.app.teachncheck.MainActivity;
import com.app.teachncheck.R;

import android.content.Context;
import android.util.Log;

/**
 * 
 * @author Julien
 * 
 * Mark object
 *
 */
public class Mark implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String max_value;
	private Classroom classroom;
	
	/**
	 * Constructor
	 * 
	 * @param id
	 * @param name
	 * @param max_value
	 * @param classroom
	 */
	public Mark(int id, String name, String max_value, Classroom classroom) {
		super();
		this.id = id;
		this.name = name;
		this.max_value = max_value;
		this.classroom = classroom;
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 * @param max_value
	 * @param classroom
	 */
	public Mark(String name, String max_value, Classroom classroom) {
		super();
		this.id = 0;
		this.name = name;
		this.max_value = max_value;
		this.classroom = classroom;
	}
	
	/**
	 * Constructor
	 * 
	 * @param name
	 * @param max_value
	 */
	public Mark(String name, String max_value) {
		super();
		this.id = 0;
		this.name = name;
		this.max_value = max_value;
	}
	
	/**
	 * Constructor by copy
	 * 
	 * @param mark
	 */
	private Mark(Mark mark) {
		super();
		this.name = mark.name;
		this.max_value = mark.max_value;
		this.classroom = mark.classroom;
	}
	
	/**
	 * Create a Mark in database and add the id to the current mark
	 */
	public boolean initMark(){
		this.id = (MainActivity.getDb().addMark(new Mark(this)));
		if(this.id >= 1){
			MainActivity.getDb().close();
			return true;
		}else{
			MainActivity.getDb().close();
			return false;
		}
	}
	
	/**
	 * Save each modification of the mark in database
	 */
	public boolean saveMark(){
		if(this.getId() != 0){
			if(MainActivity.getDb().updateMark(this)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In saveMark()", "Can't save a mark that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * delete a mark in database
	 */
	public boolean deleteMark(){
		if(this.getId() != 0){
			if(MainActivity.getDb().deleteMark(this)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In deleteMark()", "Can't delete a mark that doesn't exist in database");
			return false;
		}
	}
	
	public StudentMark getStudentMark(Student s){
		return MainActivity.getDb().getStudentMark(this.id, s.getId());
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMax_value() {
		return max_value;
	}

	public void setMax_value(String max_value) {
		this.max_value = max_value;
	}
	
	@Override
	public String toString() {
		return name;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	
	public float getAverage(){
		float average = 0;
		int nbMarks = 0;
//		Classroom classroom = MainActivity.getDb().getClassroom(classroom.getId());
		ArrayList<Student> students = classroom.getAllStudentsOfClassroom();
		for(Student s : students){
			StudentMark mark = getStudentMark(s);
			if(mark != null){
				average += Float.parseFloat(mark.getvalue());
				nbMarks++;
			}
		}
		if(nbMarks > 0)
			average /= nbMarks;
		return average;
	}
	
}
