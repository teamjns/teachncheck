package com.app.teachncheck.bdd;

import java.util.ArrayList;
import java.util.List;
import com.app.teachncheck.MainActivity;

/**
 * 
 * @author Julien
 * 
 * Class to manage classroom
 * 
 */
public class ClassroomManager {
	
	private List<Classroom> classrooms;
	
	public ClassroomManager(){
		classrooms = new ArrayList<Classroom>();
		initClassrooms();
	}
	
	/**
	 * Load all classrooms of the database
	 */
	public boolean initClassrooms(){
		classrooms = MainActivity.getDb().getAllClassroom();
		if(classrooms.size() != 0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Reload all classrooms of the database. Same operation as initClassrooms
	 */
	public boolean reloadClassrooms(){
		return initClassrooms();
	}

	public List<Classroom> getClassrooms() {
		return classrooms;
	}
}
