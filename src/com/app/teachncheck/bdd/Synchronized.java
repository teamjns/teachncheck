package com.app.teachncheck.bdd;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class Synchronized extends SherlockActivity {
	
	private ArrayList<Object> queue;
	
	private int timeResfresh = 60000; //1 minutes
	
	public String strURL = "http://juliencolle.netau.net/kdo/ajouter_personne.php";
	
	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	
	public Synchronized(final ArrayList<Object> queue) {
		super();
		this.queue = queue;
		
		new Thread(new Runnable() {
            @Override
            public void run() {
				if(queue.size() != 0){
					executeQueue();
				}
				try {
					Thread.sleep(timeResfresh);
				} catch (InterruptedException e) {}
            }
        }).start();
	}
	
	private void executeQueue() {
		for (Object obj : queue) {
			if(obj instanceof Student){
				//Launch an async task to add a student
				strURL = "";
				
				nameValuePairs.add(new BasicNameValuePair("nom", ""));
				nameValuePairs.add(new BasicNameValuePair("pass", ""));
				
			}else if(obj instanceof Classroom){
				
			}else if(obj instanceof ClassroomStudent){
				
			}
		}
	}
	
	public void addDataToQueue(Object obj){
		queue.add(obj);
	}

	public ArrayList<Object> getQueue() {
		return queue;
	}

	public void setQueue(ArrayList<Object> queue) {
		this.queue = queue;
	}

	public int getTimeResfresh() {
		return timeResfresh;
	}

	public void setTimeResfresh(int timeResfresh) {
		this.timeResfresh = timeResfresh;
	}
	
	class AddAccount extends AsyncTask<Void, Integer, Boolean>{
    	
    	String nom;
    	String mdp;

		public AddAccount(String nom, String mdp) {
			this.nom = nom;
			this.mdp = mdp;
		}
		
		public void onPreExecute(){
//			pB1.setVisibility(0);
		}
		
		public void onProgressUpdate(Integer... values){
			super.onProgressUpdate(values);
//			pB1.setProgress(values[0]);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			
			InputStream is = null;
			String result = "";
			
//			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//			nameValuePairs.add(new BasicNameValuePair("nom", nom));
//			nameValuePairs.add(new BasicNameValuePair("pass", mdp));
			
			int progress = 1;
			
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(strURL);
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
				
				publishProgress(progress++);

			}catch(Exception e){
				Log.e("log_tag", "Error in http connection " + e.toString());
				return false;
			}
			
			try{
				BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				result=sb.toString();
				System.out.println("le result vaut : "+result);
				publishProgress(progress++);
				
			}catch(Exception e){
				Log.e("log_tag", "Error converting result " + e.toString());
				return false;
			}
			
			try{
				
				JSONArray jArray = new JSONArray(result);
				
				for(int i=0;i<jArray.length();i++){
					
					publishProgress(progress++);
					
					JSONObject json_data = jArray.getJSONObject(i);
					
//					SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//					SharedPreferences.Editor editor = preferences.edit();
//					editor.putString("login", nom);
//					editor.putString("password", mdp);
//					editor.commit();
//					
//					finish();
					
				}
			}catch(JSONException e){
				Log.e("log_tag", "Error parsing data " + e.toString());
				Toast.makeText(getApplicationContext(), "Hum, probl�me...", Toast.LENGTH_SHORT).show();
				return false;
			}
			
			return true;
		}
		
		protected void onPostExecute(Boolean bool){
			if(!bool){
				Toast.makeText(getApplicationContext(), "Il y a eu un probl�me...", Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(getApplicationContext(), "Tout est ok", Toast.LENGTH_SHORT).show();
			}
//			pB1.setVisibility(1);
		}
    	
    }
	
}
