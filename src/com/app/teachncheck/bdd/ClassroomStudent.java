package com.app.teachncheck.bdd;

/**
 * 
 * @author Julien
 *
 * Object ClassroomStudent. Link for a Classroom and a Student
 * 
 * Used by the Database handler
 *
 */
public class ClassroomStudent {
	private int id_student;
	private int id_classroom;
	
	/**
	 * 
	 * @param id_student
	 * @param id_classroom
	 * 
	 * Constructor
	 */
	public ClassroomStudent(int id_student, int id_classroom) {
		super();
		this.id_student = id_student;
		this.id_classroom = id_classroom;
	}

	public int getId_student() {
		return id_student;
	}

	public void setId_student(int id_student) {
		this.id_student = id_student;
	}

	public int getId_classroom() {
		return id_classroom;
	}

	public void setId_classroom(int id_classroom) {
		this.id_classroom = id_classroom;
	}
}
