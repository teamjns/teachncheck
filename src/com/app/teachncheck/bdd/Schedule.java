package com.app.teachncheck.bdd;

import java.io.Serializable;
import java.util.Date;
import android.util.Log;

import com.app.teachncheck.MainActivity;

/**
 * 
 * @author Julien
 *
 * Schedule object
 *
 */
public class Schedule implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private Date day;
	private String begin;
	private String end;
	private Classroom classroom;
	private boolean checked;
	
	/**
	 * Constructor
	 * 
	 * @param day
	 * @param begin
	 * @param end
	 * @param classroom
	 * @param checked
	 */
	public Schedule(Date day, String begin, String end, Classroom classroom, boolean checked) {
		super();
		this.day = day;
		this.begin = begin;
		this.end = end;
		this.classroom = classroom;
		this.checked = checked;
	}
	
	/**
	 * Constructor
	 * 
	 * @param day
	 * @param begin
	 * @param end
	 * @param checked
	 */
	public Schedule(Date day, String begin, String end, boolean checked) {
		super();
		this.day = day;
		this.begin = begin;
		this.end = end;
		this.checked =checked;
	}
	
	/**
	 * Constructor
	 * 
	 * @param id
	 * @param day
	 * @param begin
	 * @param end
	 * @param classroom
	 * @param checked
	 */
	public Schedule(int id, Date day, String begin, String end, Classroom classroom, boolean checked) {
		super();
		this.id = id;
		this.day = day;
		this.begin = begin;
		this.end = end;
		this.classroom = classroom;
		this.checked = checked;
	}

	/**
	 * Constructor by copy
	 * 
	 * @param schedule
	 */
	private Schedule(Schedule schedule){
		super();
		this.day = schedule.day;
		this.begin = schedule.begin;
		this.end = schedule.end;
		this.classroom = schedule.classroom;
		this.checked = schedule.checked;
	}
	
	/**
	 * Create a schedule in database and add the id to the current schedule
	 * @return 
	 */
	public boolean initSchedule(){
		this.id = (MainActivity.getDb().addSchedule(new Schedule(this)));
		if(this.id >= 1){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Save each modification of the schedule in database
	 * @return 
	 */
	public boolean saveSchedule(){
		if(this.getId() != 0){
			if(MainActivity.getDb().updateSchedule(this) >= 1){
				return true;
			}else{
				
				return false;
			}
		}else{
			Log.d("In saveClass()", "Can't save a schedule that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * delete a schedule in database
	 * @return 
	 */
	public boolean deleteSchedule(){
		if(this.getId() != 0){
			if(MainActivity.getDb().deleteSchedule(this) >= 1){
				return true;
			}else{
				return false;
			}
		}else{
			Log.d("In deleteSchedule()", "Can't delete a schedule that doesn't exist in database");
			return false;
		}
	}
	
	public int getId() {
		return id;
	}
	public Date getDay() {
		return day;
	}
	public void setDay(Date day) {
		this.day = day;
	}
	public String getBegin() {
		return begin;
	}
	public void setBegin(String begin) {
		this.begin = begin;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public Classroom getClassroom() {
		return classroom;
	}
	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
}
