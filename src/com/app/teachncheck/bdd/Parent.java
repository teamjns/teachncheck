package com.app.teachncheck.bdd;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import android.util.Log;

import com.app.teachncheck.MainActivity;

/**
 * 
 * @author Julien
 *
 * Parent object
 *
 */
public class Parent implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String first_name; //Pr�nom
	private String surname;		//Nom de famille
	private String birthday;
	private String address;
	private String phone;
	private String mail;
	
	/**
	 * Constructor
	 * 
	 * @param first_name
	 * @param surname
	 * @param birthday
	 * @param address
	 * @param phone
	 * @param mail
	 */
	public Parent(String first_name, String surname, String birthday, String address,
			String phone, String mail) {
		super();
		this.first_name = first_name;
		this.surname = surname;
		this.birthday = birthday;
		this.address = address;
		this.phone = phone;
		this.mail = mail;
	}
	
	/**
	 * Constructor
	 * 
	 * @param id
	 * @param first_name
	 * @param surname
	 * @param birthday
	 * @param address
	 * @param phone
	 * @param mail
	 */
	public Parent(int id, String first_name, String surname, String birthday,
			String address, String phone, String mail) {
		super();
		this.id = id;
		this.first_name = first_name;
		this.surname = surname;
		this.birthday = birthday;
		this.address = address;
		this.phone = phone;
		this.mail = mail;
	}
	
	/**
	 * Constructor by copy
	 * @param parent
	 */
	private Parent(Parent parent){
		this.first_name = parent.first_name;
		this.surname = parent.surname;
		this.birthday = parent.birthday;
		this.address = parent.address;
		this.phone = parent.phone;
		this.mail = parent.mail;
	}
	
	/**
	 * Create a parent in database and add the id to the current parent
	 */
	public boolean initParent(){
		this.id = (MainActivity.getDb().addParent(new Parent(this)));
		if(this.id >= 1){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Save each modification of the parent in database
	 */
	public boolean saveParent(){
		if(this.getId() != 0){
			if(MainActivity.getDb().updateParent(this)>=1){
				return true;
			}else{
				return false;
			}
		}else{
			Log.d("In saveParent()", "Can't save a parent that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * delete a parent in database
	 */
	public boolean deleteParent(){
		if(this.getId() != 0){
			if(MainActivity.getDb().deleteParent(this)>=1){
				return true;
			}else{
				return false;
			}
		}else{
			Log.d("In deleteParent()", "Can't delete a parent that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * Add a student to the current class
	 * @return 
	 */
	public boolean addStudentToParent(Student student){
		if(this.getId() != 0 && student.getId() != 0){
			if(MainActivity.getDb().addParent_Student(this.getId(), student.getId())>=1){
				return true;
			}else{
				return false;
			}
		}else{
			Log.d("In addStudentToParent()", "Can't add a student to parent. One or more parameter is not recorded in database");
			return false;
		}
	}
	
	/**
	 * Delete a student of the current parent
	 * @return 
	 */
	public boolean deleteStudentOfParent(Student student){
		if(this.getId() != 0 && student.getId() != 0){
			if(MainActivity.getDb().deleteParent_Student(this.id, student.getId())>=1){
				return true;
			}else{
				return false;
			}
		}else{
			Log.d("In deleteStudentOfParent()", "Can't delete a student to parent. One or more parameter is not recorded in database");
			return false;
		}
	}
	
	/**
	 * Get all students of the current parent
	 */
	public List<Student> getAllStudentOfParent(){
		List<Student> students = new ArrayList<Student>();
		if(this.getId() != 0){
			students = MainActivity.getDb().getAllStudent(this);
		}else{
			Log.d("In getAllStudentOfParent()", "Can't get all student of parent that doesn't exist in database");
		}
		return students;
	}
	
	public int getId() {
		return id;
	}
	public String getFirstName() {
		return first_name;
	}
	public void setFirstName(String name) {
		this.first_name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
}
