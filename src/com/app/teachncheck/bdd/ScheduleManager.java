package com.app.teachncheck.bdd;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.app.teachncheck.MainActivity;

/**
 * 
 * @author Julien
 *
 * Manage Schedules
 *
 */
public class ScheduleManager {
	
	private List<Schedule> schedules;
	
	/**
	 * Constructor
	 */
	public ScheduleManager(){
		schedules = new ArrayList<Schedule>();
		initSchedules();
	}
	
	/**
	 * Load all schedules of the database
	 * @return 
	 */
	public boolean initSchedules(){
		schedules = MainActivity.getDb().getAllSchedule();
		if(schedules.size() != 0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Reload all schedules of the database. Same operation as initSchedules
	 * @return 
	 */
	public boolean reloadSchedules(){
		return initSchedules();
	}

	public List<Schedule> getSchedules() {
		return schedules;
	}
}
