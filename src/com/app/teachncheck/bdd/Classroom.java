package com.app.teachncheck.bdd;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import android.util.Log;

import com.app.teachncheck.MainActivity;
import com.app.teachncheck.R.string;

/**
 * 
 * @author Julien
 * 
 * Classroom object
 *
 */
public class Classroom implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String level;
	private String entitled;
	private String year;
	
	/**
	 * Constructor of classroom
	 * 
	 * @param name
	 * @param level
	 * @param entitled
	 * @param year
	 */
	public Classroom(String name, String level, String entitled, String year) {
		super();
		this.name = name;
		this.level = level;
		this.entitled = entitled;
		this.year = year;
	}
	
	/**
	 * Constructor of classroom
	 * 
	 * @param id
	 * @param name
	 * @param level
	 * @param entitled
	 * @param year
	 */
	public Classroom(int id, String name, String level, String entitled,String year) {
		super();
		this.id = id;
		this.name = name;
		this.level = level;
		this.entitled = entitled;
		this.year = year;
	}
	
	/**
	 * Constructor used for importing data
	 */
	public Classroom(){
		super();
	}
	
	/**
	 * Constructor by copy
	 * 
	 * @param clas
	 */
	public Classroom(Classroom clas){
		super();
		this.name = clas.name;
		this.level = clas.level;
		this.entitled = clas.entitled;
		this.year = clas.year;
	}
	
	/**
	 * Create a Classroom in database and add the id to the current class
	 */
	public boolean initClass(){
		this.id = (MainActivity.getDb().addClassroom(new Classroom(this)));
		if(this.id >= 1){
			MainActivity.getDb().close();
			return true;
		}else{
			MainActivity.getDb().close();
			return false;
		}
	}
	
	/**
	 * Save each modification of the Classroom in database
	 */
	public boolean saveClass(){
		if(this.getId() != 0){
			if(MainActivity.getDb().updateClassroom(this)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In saveClass()", "Can't save a classroom that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * delete a classroom in database
	 */
	public boolean deleteClassroom(){
		if(this.getId() != 0){
			if(MainActivity.getDb().deleteClassroom(this)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In deleteClassroom()", "Can't delete a classroom that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * Add a student to the current Classroom
	 */
	public boolean addStudentToClass(Student student){
		if(this.getId() != 0 && student.getId() != 0){
			if(MainActivity.getDb().addClass_Student(this.getId(), student.getId())>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In addStudentToClass()", "Can't add a student to the classroom. One or more parameter is not recorded in database");
			return false;
		}
	}
	
	/**
	 * delete a student of the current Classroom
	 */
	public boolean deleteStudentOfClass(Student student){
		if(this.getId() != 0 && student.getId() != 0){
			if(MainActivity.getDb().deleteClass_Student(this.getId(), student.getId())>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In deleteStudentOfClass()", "Can't delete a student of the classroom. One or more parameter is not recorded in database");
			return false;
		}
	}
	
	/**
	 * Add a schedule to the current Classroom
	 */
	public boolean addScheduleToClass(Schedule schedule){
		if(this.getId() != 0 && schedule.getId() != 0){
			if(MainActivity.getDb().updateSchedule(this.getId(), schedule)>=1){
				schedule.setClassroom(this);
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In addScheduleToClass()", "Can't add a schedule to the classroom. One or more parameter is not recorded in database");
			return false;
		}
	}
	
	/**
	 * delete a schedule of the current Classroom
	 */
	public boolean deleteScheduleOfClass(Schedule schedule){
		if(this.getId() != 0 && schedule.getId() != 0){
			if(MainActivity.getDb().deleteSchedule(schedule)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In deleteScheduleOfClass()", "Can't delete a schedule of the classroom. One or more parameter is not recorded in database");
			return false;
		}
	}
	
	/**
	 * Add a mark to the current Classroom
	 */
	public boolean addMarkToClass(Mark m) {
		if(this.getId() != 0 && m.getId() != 0){
			if(MainActivity.getDb().updateMark(this.getId(), m)>=1){
				m.setClassroom(this);
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In addMarkToClass()", "Can't add a mark to the classroom. One or more parameter is not recorded in database");
			return false;
		}
	}
	
	/**
	 * return all students of the classroom
	 */
	public ArrayList<Student> getAllStudentsOfClassroom(){
		ArrayList<Student> students = new ArrayList<Student>();
		if(this.getId() != 0){
			students = MainActivity.getDb().getAllStudent(this);
			MainActivity.getDb().close();
		}else{
			Log.d("In getAllStudentsOfClassroom()", "Can't get students of the classroom. One or more parameter is not recorded in database");
		}
		return students;
	}
	
	/**
	 * return all students that are not in the classroom
	 */
	public ArrayList<Student> getAllStudentsNotInClassroom(){
		ArrayList<Student> students = new ArrayList<Student>();
		if(this.getId() != 0){
			students = MainActivity.getDb().getAllStudentNotIn(this);
			MainActivity.getDb().close();
		}else{
			Log.d("In getAllStudentsNotInClassroom()", "Can't get students. One or more parameter is not recorded in database");
		}
		return students;
	}
	
	/**
	 * return all mark of the classroom
	 */
	public ArrayList<Mark> getAllMarksOfClassroom(){
		ArrayList<Mark> marks = new ArrayList<Mark>();
		if(this.getId() != 0){
			marks = MainActivity.getDb().getAllMark(this);
			MainActivity.getDb().close();
		}else{
			Log.d("In getAllMarksOfClassroom()", "Can't get marks of the classroom. One or more parameter is not recorded in database");
		}
		return marks;
	}
	
	/**
	 * return schedule of the day of the classroom
	 */
	public ArrayList<Schedule> getAllSchedulesOfClassroom(){
		ArrayList<Schedule> schedules = new ArrayList<Schedule>();
		if(this.getId() != 0){
			schedules = MainActivity.getDb().getAllSchedule(this);
			MainActivity.getDb().close();
		}else{
			Log.d("In getAllSchedulesOfClassroom()", "Can't get schedules of the classroom. One or more parameter is not recorded in database");
		}
		return schedules;
	}
	
	/**
	 * return all student absent of the classroom in a specific schedule
	 */
	public ArrayList<Student> getAllStudentAbsentOfClassroom(Schedule schedule){
		ArrayList<Student> student = new ArrayList<Student>();
		if(this.getId() != 0 && schedule.getId() != 0){
			student = MainActivity.getDb().getAllStudentAbsentBySchedule(this, schedule);
			MainActivity.getDb().close();
		}else{
			Log.d("In getAllStudentAbsentOfClassroom()", "Can't get student absent of the classroom. One or more parameter is not recorded in database");
		}
		return student;
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getEntitled() {
		return entitled;
	}
	public void setEntitled(String entitled) {
		this.entitled = entitled;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	public String toString(){
		if(name.equals(""))
    		return name;
		return name.replaceFirst(".",(name.charAt(0)+"").toUpperCase()) + " ("+year+")";
	}

}
