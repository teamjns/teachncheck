package com.app.teachncheck.bdd;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.app.teachncheck.MainActivity;
import android.util.Log;

/**
 * 
 * @author Julien
 *
 * Student object
 *
 */
public class Student implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String identity;
	private String first_name; //Pr�nom
	private String surname;		//Nom de famille
	private String birthday;
	private String address;
	private String phone;
	private String mail;
	private String picture_path;
	
	/**
	 * Constructor
	 * 
	 * @param id
	 * @param identity
	 * @param name
	 * @param surname
	 * @param birthday
	 * @param address
	 * @param phone
	 * @param mail
	 * @param picture_path
	 */
	public Student(int id, String identity, String name, String surname, String birthday,
			String address, String phone, String mail, String picture_path) {
		super();
		this.id = id;
		this.identity = identity;
		this.first_name = name;
		this.surname = surname;
		this.birthday = birthday;
		this.address = address;
		this.phone = phone;
		this.mail = mail;
		this.picture_path = picture_path;
	}
	
	/**
	 * Constructor
	 * 
	 * @param identity
	 * @param name
	 * @param surname
	 * @param birthday
	 * @param address
	 * @param phone
	 * @param mail
	 * @param picture_path
	 */
	public Student(String identity, String name, String surname, String birthday,
			String address, String phone, String mail, String picture_path) {
		super();
		this.id = 0;
		this.identity = identity;
		this.first_name = name;
		this.surname = surname;
		this.birthday = birthday;
		this.address = address;
		this.phone = phone;
		this.mail = mail;
		this.picture_path = picture_path;
	}
	
	/**
	 * Constructor
	 * 
	 * @param surname
	 * @param first_name
	 */
	public Student(String surname, String first_name) {
		super();
		this.id = 0;
		this.identity = "";
		this.first_name = first_name;
		this.surname = surname;
		this.birthday = "";
		this.address = "";
		this.phone = "";
		this.mail = "";
		this.picture_path = "";
	}
	
	/**
	 * Constructor used for importing data
	 */
	public Student(){
		super();
	}
	
	/**
	 * To create a student without known id in the case of an adding in BDD
	 * Don't delete the second star
	 */
	public Student(Student student) {
		super();
		this.identity = student.identity;
		this.first_name = student.first_name;
		this.surname = student.surname;
		this.birthday = student.birthday;
		this.address = student.address;
		this.phone = student.phone;
		this.mail = student.mail;
		this.picture_path = student.picture_path;
	}
	
	/**
	 * Create a Student in database and add the id to the current student
	 */
	public boolean initStudent(){
		this.id = (MainActivity.getDb().addStudent(new Student(this)));
		if(this.id >= 1){
			MainActivity.getDb().close();
			return true;
		}else{
			MainActivity.getDb().close();
			return false;
		}
	}
	
	/**
	 * Save each modification of the student in database
	 */
	public boolean saveStudent(){
		if(this.getId() != 0){
			if(MainActivity.getDb().updateStudent(this)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In saveStudent()", "Can't save a student that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * delete a student in database
	 */
	public boolean deleteStudent(){
		if(this.getId() != 0){
			if(MainActivity.getDb().deleteStudent(this)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In deleteStudent()", "Can't delete a student that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * Add an student mark to the current student
	 */
	public boolean addMarkToStudent(StudentMark sm, Mark m) {
		if(this.getId() != 0 && sm.getId_student_mark() != 0 && m.getId() != 0){
			if(MainActivity.getDb().updateStudentMark(this.getId(), sm, m)>=1){
				sm.setMark(m);
				sm.setStudent(this);
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In addMarkToStudent()", "Can't add a student mark. One or more parameter is not recorded in database");
			return false;
		}
	} 
	
	/**
	 * Add an absence to the current student
	 */
	public boolean addAbsenceToStudent(Schedule schedule, Absence absence){
		if(this.getId() != 0 && schedule.getId() != 0 && absence.getId() != 0){
			if(MainActivity.getDb().updateAbsence(this.getId(), schedule, absence)>=1){
				absence.setSchedule(schedule);
				absence.setStudent(this);
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In addAbsenceToStudent()", "Can't add an absence. One or more parameter is not recorded in database");
			return false;
		}
	}
	
	/**
	 * Delete an absence of the current student
	 */
	public boolean deleteAbsenceOfStudent(Absence absence){
		if(this.getId() != 0 && absence.getId() != 0){
			if(MainActivity.getDb().deleteAbsence(absence)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In deleteAbsenceOfStudent()", "Can't delete an absence. One or more parameter is not recorded in database");
			return false;
		}
	}
	
	/**
	 * Get all parents of the current student
	 */
	public ArrayList<Parent> getAllParentOfStudent(){
		ArrayList<Parent> parents = new ArrayList<Parent>();
		if(this.getId() != 0){
			parents = MainActivity.getDb().getAllParent(this);
		}else{
			Log.d("In getAllParentOfStudent()", "Can't get parent(s). One or more parameter is not recorded in database");
		}
		MainActivity.getDb().close();
		return parents;
	}
	/*
	/**
	 * Add an mark to the current student
	 
	public boolean addMarkToStudent(Mark mark, Classroom classroom){
		if(this.getId() != 0 && mark.getId() != 0 && classroom.getId() != 0){
			if(MainActivity.getDb().updateMark(this.getId(), mark, classroom)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In addMarkToStudent()", "Can't add a mark. One or more parameter is not recorded in database");
			return false;
		}
	}*/
	
	/**
	 * Delete a mark of the current student
	 */
	public boolean deleteMarkOfStudent(Mark mark){
		if(this.getId() != 0 && mark.getId() != 0){
			if(MainActivity.getDb().deleteMark(mark)>=1){
				MainActivity.getDb().close();
				return true;
			}else{
				MainActivity.getDb().close();
				return false;
			}
		}else{
			MainActivity.getDb().close();
			Log.d("In deleteMarkOfStudent()", "Can't delete a mark. One or more parameter is not recorded in database");
			return false;
		}
	}
	
	/**
	 * Get all marks of the current student
	 */
	public ArrayList<StudentMark> getAllMarkOfStudent(){
		ArrayList<StudentMark> marks = new ArrayList<StudentMark>();
		if(this.getId() != 0){
			marks = MainActivity.getDb().getAllStudentMark(this);
		}else{
			Log.d("In getAllMarkOfStudent()", "Can't get marks. One or more parameter is not recorded in database");
		}
		MainActivity.getDb().close();
		return marks;
	}
	
	public ArrayList<Absence> getAllAbsenceOfStudent(){
		return MainActivity.getDb().getAllAbsence(this);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	public String getName() {
		return first_name;
	}
	public void setName(String name) {
		this.first_name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPicture_path() {
		return picture_path;
	}
	public void setPicture_path(String picture_path) {
		this.picture_path = picture_path;
	}
	
	public String toString(){
		return surname + " " +first_name;
	}
	
	@Override
	public boolean equals(Object o) {
		return this.id == ((Student)o).id;
	}
}
