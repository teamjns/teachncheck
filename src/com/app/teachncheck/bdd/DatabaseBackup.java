package com.app.teachncheck.bdd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 * @author Julien
 * 
 * Backup the database in a file
 *
 */
public class DatabaseBackup {

	private Context context;
	private File fileDb;
	private File fileEx;

	/**
	 * Constructor
	 * 
	 * @param context
	 */
	public DatabaseBackup(Context context) {
		this.context = context;
	}
	
	/**
	 * Execute a backup
	 */
	public void backupDatabase() {
		try {
			fileDb = new File(getDataDir() + "/databases/db");
		} catch (Exception e) {
			e.printStackTrace();
		}
		File exportDir = new File(Environment.getExternalStorageDirectory() + "/TeachNcheckFiles/");
    	if (!exportDir.exists()) {
    		exportDir.mkdirs();
    	}
		fileEx = new File(Environment.getExternalStorageDirectory() + "/TeachNcheckFiles/", "");
		new ManageDbFileTask().execute(fileDb, fileEx);
	}
	
	/**
	 * Execute a restore
	 */
	public void restoreDatabase() {
		File exportDir = new File(Environment.getExternalStorageDirectory() + "/TeachNcheckFiles/");
    	if (!exportDir.exists()) {
    		exportDir.mkdirs();
    	}
		fileDb = new File(Environment.getExternalStorageDirectory()+ "/TeachNcheckFiles/db");
		try {
			fileEx = new File(getDataDir() + "/databases/", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		new ManageDbFileTask().execute(fileDb, fileEx);
	}
	
	/**
	 * Get the dir of the db file
	 * 
	 * @return
	 * @throws Exception
	 */
	private String getDataDir() throws Exception {
	    return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.dataDir;
	}

	/**
	 * AsyncTask to do the backup
	 * 
	 * @author Julien
	 *
	 */
	private class ManageDbFileTask extends AsyncTask<File, Void, Boolean> {
	    //private final ProgressDialog dialog = new ProgressDialog(context);

		
	    protected void onPreExecute() {
	       //this.dialog.setMessage("Computing...");
	       //this.dialog.show();
	    }

	    protected Boolean doInBackground(final File... args) {
	    	File dbFile = args[0];//new File(Environment.getDataDirectory() + "/data/com.mypkg/databases/mydbfile.db");
		
	    	File exportDir = args[1];//new File(Environment.getExternalStorageDirectory(), "");
	    	if (!exportDir.exists()) {
	    		exportDir.mkdirs();
	    	}
	    	File file = new File(exportDir, dbFile.getName());
		
	    	try {
	    		file.createNewFile();
	    		this.copyFile(dbFile, file);
	    		return true;
	    	} catch (IOException e) {
	    		Log.e("error in copying db file", e.getMessage(), e);
	    		return false;
	    	}
		}

	    protected void onPostExecute(final Boolean success) {
//	       if (this.dialog.isShowing()) {
//	          this.dialog.dismiss();
//	       }
	       if (success) {
	          Toast.makeText(context, "Successful!", Toast.LENGTH_SHORT).show();
	       } else {
	          Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
	       }
	    }

	    /**
	     * Copy the file with a source and a destination
	     * 
	     * @param src
	     * @param dst
	     * @throws IOException
	     */
	    void copyFile(File src, File dst) throws IOException {
	       FileChannel inChannel = new FileInputStream(src).getChannel();
	       FileChannel outChannel = new FileOutputStream(dst).getChannel();
	       try {
	          inChannel.transferTo(0, inChannel.size(), outChannel);
	       } finally {
	          if (inChannel != null)
	             inChannel.close();
	          if (outChannel != null)
	             outChannel.close();
	       }
	    }

	 }

}


