package com.app.teachncheck.bdd;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.app.teachncheck.MainActivity;

/**
 * 
 * @author Julien
 *
 * Manage parents
 *
 */
public class ParentManager {
	
	private List<Parent> parents;
	
	/**
	 * Constructor
	 */
	public ParentManager(){
		parents = new ArrayList<Parent>();
		initParents();
	}
	
	/**
	 * Load all classrooms of the database
	 */
	public boolean initParents(){
		parents = MainActivity.getDb().getAllParent();
		if(parents.size() != 0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Reload all classrooms of the database. Same operation as initClassrooms
	 */
	public boolean reloadClassrooms(){
		return initParents();
	}

	public List<Parent> getClassrooms() {
		return parents;
	}
}
