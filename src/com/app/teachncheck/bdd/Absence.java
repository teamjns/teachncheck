package com.app.teachncheck.bdd;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import android.util.Log;

import com.app.teachncheck.MainActivity;

/**
 * 
 * An Absence 
 *
 */
public class Absence implements  Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	private boolean justified;
	private String reason;
	private Student student;
	private Schedule schedule;
	
	/**
	 * Constructor
	 * 
	 * @param justified
	 * @param reason
	 * @param student
	 * @param schedule
	 */
	public Absence(boolean justified, String reason, Student student, Schedule schedule) {
		super();
		this.justified = justified;
		this.reason = reason;
		this.schedule = schedule;
		this.student = student;
	}
	
	/**
	 * Constructor
	 * 
	 * @param justified
	 * @param reason
	 */
	public Absence(boolean justified, String reason) {
		super();
		this.justified = justified;
		this.reason = reason;
	}
	
	/**
	 * Constructor
	 * 
	 * @param id
	 * @param justified
	 * @param reason
	 * @param student
	 * @param schedule
	 */
	public Absence(int id, boolean justified, String reason, Student student, Schedule schedule) {
		super();
		this.id = id;
		this.justified = justified;
		this.reason = reason;
		this.schedule = schedule;
		this.student = student;
	}
	
	/**
	 * Constructor by copy
	 * 
	 * @param absence
	 */
	private Absence(Absence absence){
		super();
		this.justified = absence.justified;
		this.reason = absence.reason;
		this.schedule = absence.schedule;
		this.student = absence.student;
	}
	
	/**
	 * Create a absence in database and add the id to the current absence
	 */
	public boolean initAbsence(){
		this.id = (MainActivity.getDb().addAbsence(new Absence(this)));
		if(this.id >= 1){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Save each modification of the absence in database
	 */
	public boolean saveAbsence(){
		if(this.getId() != 0){
			if(MainActivity.getDb().updateAbsence(this)>=1){
				return true;
			}else{
				return false;
			}
		}else{
			Log.d("In saveAbsence()", "Can't save a Absence that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * delete an absence in database
	 */
	public boolean deleteAbsence(){
		if(this.getId() != 0){
			if(MainActivity.getDb().deleteAbsence(this)>=1){
				return true;
			}else{
				return false;
			}
		}else{
			Log.d("In deleteAbsence()", "Can't delete an absence that doesn't exist in database");
			return false;
		}
	}
	
	/**
	 * get the schedule of an absence in database
	 */
	public Schedule getScheduleOfAbsence(){
		Schedule s = null;
		if(this.getId() != 0){
			s = MainActivity.getDb().getSchedule(this);
			MainActivity.getDb().close();
		}else{
			Log.d("In getSchedule()", "Can't get the Schedule. One or more parameter is not recorded in database");
		}
		return s;
	}
	
	public int getId() {
		return id;
	}
	public boolean getJustified() {
		return justified;
	}
	public void setJustified(boolean justified) {
		this.justified = justified;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent (Student student) {
		this.student = student;
	}
	public Schedule getSchedule() {
		return schedule;
	}
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	
	@Override
	public String toString() {
		Schedule schedule = MainActivity.getDb().getSchedule(this);
		Log.w("tag", schedule+"");
		DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
		return dateFormat.format(schedule.getDay());
	}
	
}
